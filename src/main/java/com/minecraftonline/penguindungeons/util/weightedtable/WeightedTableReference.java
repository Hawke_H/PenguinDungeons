package com.minecraftonline.penguindungeons.util.weightedtable;

import com.minecraftonline.penguindungeons.customentity.PDEntityType;
import com.minecraftonline.penguindungeons.dungeon.WeightedSpawnOption;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import javax.annotation.Nullable;
import java.util.Collection;
import java.util.Map;

/**
 * An interface for indirection of some weighted table.
 * Anticipate <b>all</b> can cause immediate changes to the world.
 * @param <T>
 */
public interface WeightedTableReference<T> {

    static WeightedTableReference<PDEntityType> of(final WeightedSpawnOption weightedSpawnOption) {
        return new WeightedSpawnOptionTableReference(weightedSpawnOption);
    }

    static WeightedTableReference<PDEntityType> ofSpawner(final Location<World> mobSpawner) {
        return new MobSpawnerWeightedTableReference(mobSpawner);
    }

    /**
     * Since this is a Reference to a WeightedTable,
     * at any time, it could become invalid. Users
     * of this class must handle this.
     */
    class ReferenceNoLongerValid extends Exception {
        public ReferenceNoLongerValid(final String msg) {
            super(msg);
        }
    }

    class KeyNotValidException extends Exception {
        public KeyNotValidException(final String reason) {
            super(reason);
        }
    }

    @Nullable
    Double getWeight(T obj) throws ReferenceNoLongerValid;

    /**
     * Puts the specified object with the given weight
     * into the table, replacing it if it already exists.
     * @param obj Object to put into the table
     * @param weight Weight of the object.
     * @throws KeyNotValidException if the given object is not supported
     *                              by this WeightedTableModifier due to
     *                              some implementation detail.
     */
    void put(T obj, double weight) throws ReferenceNoLongerValid, KeyNotValidException;

    boolean has(T obj) throws ReferenceNoLongerValid;

    void remove(T obj) throws ReferenceNoLongerValid;

    Collection<Map.Entry<T, Double>> entries() throws ReferenceNoLongerValid;

    double totalWeight() throws ReferenceNoLongerValid;
}
