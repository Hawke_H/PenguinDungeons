package com.minecraftonline.penguindungeons;

import com.google.inject.Inject;
import com.minecraftonline.penguindungeons.command.commands.PDCommandRoot;
import com.minecraftonline.penguindungeons.config.ConfigManager;
import com.minecraftonline.penguindungeons.config.PDConfigEntityRegistry;
import com.minecraftonline.penguindungeons.container_loot.ContainerListener;
import com.minecraftonline.penguindungeons.customentity.CustomEntityType;
import com.minecraftonline.penguindungeons.customentity.CustomEntityTypes;
import com.minecraftonline.penguindungeons.customentity.LaserGuardian;
import com.minecraftonline.penguindungeons.customentity.PDEntityRegistry;
import com.minecraftonline.penguindungeons.customentity.PDEntityType;
import com.minecraftonline.penguindungeons.customentity.dragon.CustomDragon;
import com.minecraftonline.penguindungeons.customentity.wither.CustomWither;
import com.minecraftonline.penguindungeons.data.PenguinDungeonAITaskTypes;
import com.minecraftonline.penguindungeons.data.PenguinDungeonKeys;
import com.minecraftonline.penguindungeons.dungeon.Dungeon;
import com.minecraftonline.penguindungeons.gui.sign.OpenSignEditorManager;
import com.minecraftonline.penguindungeons.interact.PDInteract;
import com.minecraftonline.penguindungeons.loot.LootTableManager;
import com.minecraftonline.penguindungeons.mobspawner.MobSpawnerListener;
import com.minecraftonline.penguindungeons.spawnable.Spawnable;
import com.minecraftonline.penguindungeons.trigger.TriggerManager;
import com.minecraftonline.penguindungeons.util.ResourceKey;
import com.minecraftonline.penguindungeons.wand.PDWand;
import com.minecraftonline.penguindungeons.wand.PDWandHeldListener;

import net.minecraft.entity.EntityList;
import net.minecraft.entity.boss.EntityDragon;
import net.minecraft.entity.boss.EntityWither;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.monster.EntityGuardian;
import net.minecraft.scoreboard.Team.CollisionRule;
import net.minecraft.util.ResourceLocation;
import org.slf4j.Logger;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.config.ConfigDir;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.ExperienceOrb;
import org.spongepowered.api.entity.living.Living;
import org.spongepowered.api.entity.living.Villager;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.entity.projectile.DamagingProjectile;
import org.spongepowered.api.entity.projectile.source.ProjectileSource;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.cause.EventContextKeys;
import org.spongepowered.api.event.cause.entity.damage.source.EntityDamageSource;
import org.spongepowered.api.event.entity.DestructEntityEvent;
import org.spongepowered.api.event.entity.SpawnEntityEvent;
import org.spongepowered.api.event.filter.cause.First;
import org.spongepowered.api.event.filter.cause.Root;
import org.spongepowered.api.event.filter.type.Exclude;
import org.spongepowered.api.event.game.state.GamePreInitializationEvent;
import org.spongepowered.api.event.game.state.GameStartedServerEvent;
import org.spongepowered.api.event.game.state.GameStoppingServerEvent;
import org.spongepowered.api.event.item.inventory.DropItemEvent;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStackSnapshot;
import org.spongepowered.api.item.inventory.query.QueryOperationTypes;
import org.spongepowered.api.plugin.Plugin;
import org.spongepowered.api.plugin.PluginContainer;
import org.spongepowered.api.scoreboard.CollisionRules;
import org.spongepowered.api.scoreboard.Team;
import org.spongepowered.api.scoreboard.Visibilities;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Plugin(
        id = "penguindungeons",
        name = "PenguinDungeons",
        authors = {
                "tyhdefu",
                "Anna_28"
        },
        description = "A plugin that allows creation and configuration of spawner nodes, to make dungeons, as well as automatic triggering of spawning of these dungeons."
)
public class PenguinDungeons {

    public static final String BASE_PERMISSION = "penguindungeons.";

    private static PenguinDungeons INSTANCE;

    private final Logger logger;
    private final Path configDir;
    private final PluginContainer container;

    @Inject
    public PenguinDungeons(final Logger logger, @ConfigDir(sharedRoot = false) Path configDir,
                           final PluginContainer container) {
        this.container = container;
        INSTANCE = this;
        this.logger = logger;
        this.configDir = configDir;
        this.configManager = new ConfigManager(this.configDir);
        Path lootTables = this.configDir.resolve("LootTables");
        this.lootTableManager = new LootTableManager(lootTables.resolve("defaults"), lootTables);
    }

    public static PenguinDungeons getInstance() {
        return INSTANCE;
    }

    public static Logger getLogger() {
        return getInstance().logger;
    }

    private final PDEntityRegistry ENTITY_REGISTRY = new PDEntityRegistry();
    private final PDConfigEntityRegistry CONFIG_ENTITY_REGISTRY = new PDConfigEntityRegistry();

    // By keeping track of deleted, we don't yeet the whole config file if we have an error while loading it etc.
    private final Map<String, Dungeon> dungeons = new HashMap<>();
    private final TriggerManager triggerManager = new TriggerManager();
    private final ConfigManager configManager;
    private final OpenSignEditorManager openSignEditorManager = new OpenSignEditorManager();
    private final LootTableManager lootTableManager;

    public static Map<String, Dungeon> getDungeons() {
        return getInstance().dungeons;
    }

    public void deleteDungeon(Dungeon dungeon) {
        this.dungeons.remove(dungeon.name());
        this.configManager.notifyDungeonDeleted(dungeon);
        PDWand.dungeonDeleted(dungeon);
    }

    public PluginContainer getContainer() {
        return this.container;
    }

    public PDEntityRegistry getPDEntityRegistry() {
        return this.ENTITY_REGISTRY;
    }

    public PDConfigEntityRegistry getPDConfigEntityRegistry() {
        return this.CONFIG_ENTITY_REGISTRY;
    }

    public OpenSignEditorManager getOpenSignEditorManager() {
        return openSignEditorManager;
    }

    public ResourceLocation getLootTableFor(PDEntityType pdEntityType) {
        return this.lootTableManager.getLootTable(pdEntityType.getId());
    }

    public ResourceLocation getLootTableFor(ResourceKey id) {
        return this.lootTableManager.getLootTable(id);
    }

    public LootTableManager getLootTableManager() {
        return lootTableManager;
    }

    @Listener
    public void onPreInit(GamePreInitializationEvent event) {
        // TODO: I don't think this will work for spongeforge.
        {
            // So, this is very jank, but I guess thats what you get when you try and fake being a both-sided mod,
            // yet, you aren't. Here we register the custom dragon.
            // This creates the mappings:
            // CustomDragon.class -> 63
            // dragonKey -> CustomDragon.class
            // Therefore to stop things hitting the fan (although i didn't ever actually test if they will)
            // We need to Re-replace the original dragon
            // Therefore we add:
            // EnderDragon.class -> 63 (already existed)
            // dragonKey -> EntityDragon.class (this is the one we need to stop things going wrongly)
            int enderDragonId = 63;
            ResourceLocation dragonKey = new ResourceLocation("ender_dragon");
            EntityList.REGISTRY.register(enderDragonId, dragonKey, CustomDragon.class);
            EntityList.REGISTRY.register(enderDragonId, dragonKey, EntityDragon.class);
            EntityList.KNOWN_TYPES.add(dragonKey);
            int guardianId = 68;
            ResourceLocation guardianKey = new ResourceLocation("guardian");
            EntityList.REGISTRY.register(guardianId, guardianKey, LaserGuardian.class);
            EntityList.REGISTRY.register(guardianId, guardianKey, EntityGuardian.class);
            EntityList.KNOWN_TYPES.add(guardianKey);
            int witherId = 64;
            ResourceLocation witherKey = new ResourceLocation("wither");
            EntityList.REGISTRY.register(witherId, witherKey, CustomWither.class);
            EntityList.REGISTRY.register(witherId, witherKey, EntityWither.class);
            EntityList.KNOWN_TYPES.add(witherKey);
        }

        PenguinDungeonKeys.register();
        PenguinDungeonAITaskTypes.register();

        CustomEntityTypes.initRegistry();

        ENTITY_REGISTRY.registerAll(CustomEntityTypes.getAll());
        ENTITY_REGISTRY.registerFallback(CONFIG_ENTITY_REGISTRY);

        Sponge.getCommandManager().register(this, new PDCommandRoot().create(), "pd", "penguindungeons");

        Sponge.getEventManager().registerListeners(this, new PDWandHeldListener());
        Sponge.getEventManager().registerListeners(this, new PDWand());
        Sponge.getEventManager().registerListeners(this, new MobSpawnerListener());
        Sponge.getEventManager().registerListeners(this, new ContainerListener());
        Sponge.getEventManager().registerListeners(this, new PDInteract());

        // Read config //
        this.configManager.makeDirs();
        this.configManager.readEntitiesConfig().forEach(entity -> CONFIG_ENTITY_REGISTRY.register(entity.getId(), entity));
        this.configManager.readDungeonsConfig().forEach(dungeon -> this.dungeons.put(dungeon.name(), dungeon));
        this.configManager.readTriggersConfig().forEach(tuple -> this.triggerManager.register(tuple.getFirst(), tuple.getSecond()));

        // Loot tables //
        this.lootTableManager.copyDefaultLootTables(); // I don't know a better and easier way.
    }

    @Listener
    public void onServerStart(GameStartedServerEvent event) {
        logger.info("Penguin Dungeons is starting!");

        try {
            Sponge.getServer().getServerScoreboard().ifPresent(scoreboard -> {
                // Scoreboard team that adds the "_Flippeh" suffix to users on this team
                // so that the Flippeh bosses can have usernames longer than 16 characters
                Team flippehTeam = Team.builder().allowFriendlyFire(false)
                .name("FlippehFamily")
                .displayName(Text.of("Flippeh Family"))
                .nameTagVisibility(Visibilities.ALWAYS)
                .canSeeFriendlyInvisibles(false)
                .collisionRule(CollisionRules.NEVER)
                .suffix(Text.of("_Flippeh"))
                .build();
                scoreboard.registerTeam(flippehTeam);
                logger.info("Added Flippeh Family scoreboard team");
            });
        } catch(IllegalArgumentException e) {
            logger.info("Unable to add Flippeh Family scoreboard team, it may already be present");
        }
    }

    @Listener
    public void onServerStopping(GameStoppingServerEvent event) {
        // Save config
        this.save();
    }

    @Listener
    @Exclude(SpawnEntityEvent.ChunkLoad.class)
    public void spawnEntityEventByPlayerPDEgg(SpawnEntityEvent event, @Root Player player) {
        Optional<ItemStackSnapshot> optStack = event.getContext().get(EventContextKeys.USED_ITEM);
        if (!optStack.isPresent()) {
            return;
        }
        ItemStackSnapshot itemStackSnapshot = optStack.get();
        if (itemStackSnapshot.getType() != ItemTypes.SPAWN_EGG) {
            return;
        }
        Optional<CustomEntityType> customEntityType = itemStackSnapshot.get(PenguinDungeonKeys.PD_ENTITY_TYPE)
                .flatMap(CustomEntityTypes::getById);
        if (!customEntityType.isPresent()) {
            return;
        }
        event.setCancelled(true);
        if (!player.hasPermission(customEntityType.get().getPermission())) {
            // Uh oh, someone doesn't have permission to have the item they just used. Lets go through their inventory, wiping everything they shouldn't have
            player.getInventory().query(QueryOperationTypes.ITEM_STACK_CUSTOM.of(stack -> stack.get(PenguinDungeonKeys.PD_ENTITY_TYPE)
                    .flatMap(CustomEntityTypes::getById)
                    .filter(type -> !player.hasPermission(type.getPermission()))
                    .isPresent()
            )).poll();
            PenguinDungeons.getLogger().warn("Player " + player.getName() + ", had a PenguinDungeons spawn egg but didn't have permission to have it. All offending items have been removed. (uuid '" + player.getUniqueId() + "')");
            return;
        }
        for (Entity entity : event.getEntities()) {
            Spawnable newEntity = customEntityType.get().createEntity(entity.getWorld(), entity.getLocation().getPosition());
            newEntity.spawn(entity.getWorld());
            PenguinDungeons.getLogger().debug(player.getName() + " Spawned a " + customEntityType.get().getId() + " custom entity");
        }
    }

    @Listener
    @Exclude(SpawnEntityEvent.ChunkLoad.class)
    public void spawnEntityEventByPlayer(SpawnEntityEvent event, @Root Player player) {
        // ignore spawns done by this plugin to prevent infinite loop
        if (event.getCause().contains(Sponge.getPluginManager().getPlugin("penguindungeons").get())) return;
        // handle custom PD entities being spawned by a player using NBT egg or commands
        boolean usedEgg = false;
        Optional<ItemStackSnapshot> optStack = event.getContext().get(EventContextKeys.USED_ITEM);
        if (optStack.isPresent()) {
            // this is probably finding a match in the EntityTag of the spawn egg NBT, which makes this not work
            Optional<CustomEntityType> eggCustomEntityType = optStack.get().get(PenguinDungeonKeys.PD_ENTITY_TYPE)
                    .flatMap(CustomEntityTypes::getById);
            if (eggCustomEntityType.isPresent()) return; // handled by spawnEntityEventByPlayerPDEgg
            if (optStack.get().getType() == ItemTypes.SPAWN_EGG) {
                // NBT egg was used to spawn this entity
                usedEgg = true;
            }
        }
        for (Entity entity : event.getEntities()) {
            Optional<CustomEntityType> customEntityType = entity.get(PenguinDungeonKeys.PD_ENTITY_TYPE)
                    .flatMap(CustomEntityTypes::getById);
            if (customEntityType.isPresent()) {
                // this is a PD entity (spawned by NBT spawn egg or command or other means by player)
                Optional<Boolean> pdSpawn = entity.get(PenguinDungeonKeys.PD_SPAWN);
                if (pdSpawn.isPresent()) {
                    if (!pdSpawn.get().booleanValue())
                    {
                        // this entity needs to be reloaded
                        PenguinDungeons.getLogger().debug("Loading a " + customEntityType.get().getId() + " custom entity");
                        customEntityType.get().onLoad(entity);
                    }
                    // don't try to replace or deny PD mobs that have already been spawned correctly
                    // this includes mobs being removed from the shoulder of a player
                    continue;
                }
                if (!event.isCancelled()) event.setCancelled(true);
                if (usedEgg && !player.hasPermission(customEntityType.get().getPermission())) {
                    // no permission
                    player.getInventory().query(QueryOperationTypes.ITEM_STACK_EXACT.of(optStack.get().createStack())).poll();
                    PenguinDungeons.getLogger().warn("Player " + player.getName() + ", had an NBT spawn egg for a PenguinDungeons entity but didn't have permission to use it. Item used has been removed. (uuid '" + player.getUniqueId() + "')");
                    return;
                }
                if (usedEgg)
                {
                    PenguinDungeons.getLogger().debug(player.getName() + " Spawned a " + customEntityType.get().getId() + " custom entity with an NBT spawn egg");
                }
                else
                {
                    PenguinDungeons.getLogger().debug(player.getName() + " Spawned a " + customEntityType.get().getId() + " custom entity without a custom PD spawn egg");
                }
                Spawnable newEntity = customEntityType.get().createEntity(entity.getWorld(), entity.getLocation().getPosition());
                newEntity.spawn(entity.getWorld());
            }
        }
    }

    @Listener
    @Exclude(SpawnEntityEvent.ChunkLoad.class)
    public void spawnEntityEvent(SpawnEntityEvent event) {
        // events with a player as the root cause are handled by spawnEntityEventByPlayer above
        if (event.getCause().root() instanceof Player) return;
        // handle custom PD entities being spawned by mob spawners or commands
        for (Entity entity : event.getEntities()) {
            Optional<CustomEntityType> customEntityType = entity.get(PenguinDungeonKeys.PD_ENTITY_TYPE)
                    .flatMap(CustomEntityTypes::getById);
            if (customEntityType.isPresent()) {
                Optional<Boolean> pdSpawn = entity.get(PenguinDungeonKeys.PD_SPAWN);
                if (pdSpawn.isPresent())
                {
                    // this PD entity was already spawned in by this plugin code
                    // though not necessarily by this plugin (could be vanilla mixin or shoulder riding respawn)
                    if (!pdSpawn.get().booleanValue())
                    {
                        // this entity needs to be reloaded
                        PenguinDungeons.getLogger().debug("Loading a " + customEntityType.get().getId() + " custom entity");
                        customEntityType.get().onLoad(entity);
                    }
                    // remove the pdSpawn tag so that any copies of this entity nbt don't gain it
                    entity.remove(PenguinDungeonKeys.PD_SPAWN);
                    PenguinDungeons.getLogger().debug("A " + customEntityType.get().getId() + " custom entity was spawned by PenguinDungeons");
                }
                else
                {
                    // this is a PD entity (spawned by mob spawner or command or other means)
                    if (!event.isCancelled()) event.setCancelled(true);
                    PenguinDungeons.getLogger().debug("A " + customEntityType.get().getId() + " custom entity was spawned by NBT");
                    Spawnable newEntity = customEntityType.get().createEntity(entity.getWorld(), entity.getLocation().getPosition());
                    newEntity.spawn(entity.getWorld());
                }
            }
        }
    }

    @Listener
    public void onEntityLoad(SpawnEntityEvent.ChunkLoad event) {
        for (Entity entity : event.getEntities()) {
            if (!(entity instanceof Living)) {
                continue;
            }
            final ResourceKey id = entity.get(PenguinDungeonKeys.PD_ENTITY_TYPE).orElse(null);
            if (id == null) {
                continue;
            }
            CustomEntityTypes.getById(id).ifPresent(type -> type.onLoad(entity));
        }
    }

    @Listener
    public void onDropXP(DestructEntityEvent.Death event) {
        Entity target = event.getTargetEntity();
        Optional<ResourceKey> entityType = target.get(PenguinDungeonKeys.PD_ENTITY_TYPE);
        // make experience shulkers drop more xp
        if (entityType.isPresent() && entityType.get().equals(CustomEntityTypes.EXPERIENCE_SHULKER.getId()))
        {
            Location<World> location = target.getLocation();
            ExperienceOrb xpOrb = (ExperienceOrb) location.createEntity(EntityTypes.EXPERIENCE_ORB);
            xpOrb.offer(Keys.CONTAINED_EXPERIENCE, 37);
            location.spawnEntity(xpOrb);
        }
    }

    @Listener
    public void onVillagerDeath(DestructEntityEvent.Death event) {
        Entity target = event.getTargetEntity();
        if (!(target instanceof Villager)) return;
        Optional<ResourceKey> entityType = target.get(PenguinDungeonKeys.PD_ENTITY_TYPE);
        // make villagers lose their name tag on death
        // so the zombie villagers created from them are unnamed
        if (entityType.isPresent())
        {
            target.remove(Keys.DISPLAY_NAME);
            target.remove(Keys.CUSTOM_NAME_VISIBLE);
        }
    }

    @Listener
    public void onDropLoot(DropItemEvent.Destruct event, @First Living mob, @First EntityDamageSource source) {
        Player player;
        Entity attacker = source.getSource();
        if (attacker instanceof DamagingProjectile)
        {
            ProjectileSource shooter = ((DamagingProjectile) attacker).getShooter();
            if (!(shooter instanceof Player)) return;
            player = (Player) shooter;
        }
        else if (!(attacker instanceof Player)) {
            return;
        }
        else
        {
            player = (Player) source.getSource();
        }

        boolean ownLoot = mob.get(PenguinDungeonKeys.OWNER_LOOT).orElse(false);

        // check if tag value
        if (!ownLoot) return;

        // items dropped from mob
        for (Entity entity : event.getEntities()) {
            entity.offer(Keys.INVULNERABLE, true);
            EntityItem item = (EntityItem) entity;
            item.setOwner(player.getName());
        }
    }

    public boolean save() {
        this.configDir.toFile().mkdirs();
        return this.configManager.saveDungeons(this.dungeons.values())
                && this.configManager.saveEntities(this.CONFIG_ENTITY_REGISTRY.allBase());
    }
}
