package com.minecraftonline.penguindungeons.trigger;

import ninja.leaping.configurate.objectmapping.Setting;
import ninja.leaping.configurate.objectmapping.serialize.ConfigSerializable;

import java.util.ArrayList;
import java.util.List;

@ConfigSerializable
public abstract class AbstractTrigger implements Trigger {

    @Setting("dungeons")
    private final List<String> dungeons = new ArrayList<>();

    @Override
    public List<String> getDungeonsToSpawn() {
        return this.dungeons;
    }
}
