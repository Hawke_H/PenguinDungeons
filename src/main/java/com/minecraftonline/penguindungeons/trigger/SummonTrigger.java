package com.minecraftonline.penguindungeons.trigger;

import com.minecraftonline.penguindungeons.PenguinDungeons;
import ninja.leaping.configurate.objectmapping.Setting;
import ninja.leaping.configurate.objectmapping.serialize.ConfigSerializable;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityType;
import org.spongepowered.api.event.cause.EventContextKeys;
import org.spongepowered.api.event.cause.entity.spawn.SpawnTypes;
import org.spongepowered.api.event.entity.SpawnEntityEvent;

@ConfigSerializable
public class SummonTrigger extends AbstractTrigger {

    @Setting(value = "entity")
    private EntityType entityType;

    @Override
    public void register(Runnable onTrigger) {
        Sponge.getEventManager().registerListener(PenguinDungeons.getInstance(), SpawnEntityEvent.class, event -> {
            if (event instanceof SpawnEntityEvent.ChunkLoad) {
                return;
            }
            if (!event.getContext().get(EventContextKeys.SPAWN_TYPE).map(spawnType -> spawnType == SpawnTypes.PASSIVE).isPresent()) {
                return;
            }
            for (Entity entity : event.getEntities()) {
                if (entity.getType() == this.entityType) {
                    PenguinDungeons.getLogger().info("Summon Trigger triggered!");
                    onTrigger.run();
                }
            }
        });
    }

    @Override
    public TriggerType getType() {
        return TriggerType.SUMMON;
    }

    @Override
    public String toString() {
        return "SummonTrigger{" +
                "entityType=" + entityType +
                '}';
    }
}
