package com.minecraftonline.penguindungeons.equipment;

import org.spongepowered.api.entity.living.Living;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.util.Tuple;

import java.util.Collections;
import java.util.List;

public class EmptyEquipmentLoadout implements EquipmentLoadout {

    @Override
    public void apply(Living entity) {}

    @Override
    public List<Tuple<ItemStack, Double>> getDropChances() {
        return Collections.emptyList();
    }
}
