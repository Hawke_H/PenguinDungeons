package com.minecraftonline.penguindungeons.equipment;

import net.minecraft.entity.EntityLiving;
import net.minecraft.inventory.EntityEquipmentSlot;
import org.spongepowered.api.entity.living.Living;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.util.Tuple;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class SimpleEquipmentLoadout implements EquipmentLoadout {
    private final Map<EntityEquipmentSlot, Tuple<ItemStack, Double>> map;

    public SimpleEquipmentLoadout(Map<EntityEquipmentSlot, Tuple<ItemStack, Double>> map) {
        this.map = map;
    }

    @Override
    public void apply(Living entity) {
        EntityLiving entityLiving = (EntityLiving) entity;
        for (Map.Entry<EntityEquipmentSlot, Tuple<ItemStack, Double>> entry : this.map.entrySet()) {
            entityLiving.setItemStackToSlot(entry.getKey(), (net.minecraft.item.ItemStack) (Object) entry.getValue().getFirst().copy());
            entityLiving.setDropChance(entry.getKey(), entry.getValue().getSecond().floatValue());
        }
    }

    @Override
    public List<Tuple<ItemStack, Double>> getDropChances() {
        return new ArrayList<>(map.values());
    }
}
