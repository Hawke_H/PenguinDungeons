package com.minecraftonline.penguindungeons.gui.inventory;

import com.minecraftonline.penguindungeons.PenguinDungeons;
import com.minecraftonline.penguindungeons.customentity.PDEntityType;
import com.minecraftonline.penguindungeons.util.weightedtable.WeightedTableReference;
import com.minecraftonline.penguindungeons.wand.PDWand;
import org.spongepowered.api.scheduler.Task;
import org.spongepowered.api.text.Text;

import java.util.UUID;

public class PDNodeEditInventory extends PDSpawnEntityEditInventory {
    private final UUID nodeUUID;

    public PDNodeEditInventory(WeightedTableReference<PDEntityType> weightedTableReference, Text title, UUID nodeUUID) {
        super(weightedTableReference, title, SpawnInventoryType.DUNGEON_NODE);
        this.nodeUUID = nodeUUID;
    }

    @Override
    protected boolean supportsDelete() {
        return true;
    }

    @Override
    protected void delete() {
        Task.builder()
                .delayTicks(1)
                .execute(this::closeAllOpenInventories)
                .submit(PenguinDungeons.getInstance());
        PDWand.deleteNode(nodeUUID);
    }
}
