package com.minecraftonline.penguindungeons.gui.inventory;

import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.item.inventory.ClickInventoryEvent;
import org.spongepowered.api.item.inventory.Inventory;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.property.InventoryDimension;
import org.spongepowered.api.item.inventory.property.SlotIndex;
import org.spongepowered.api.item.inventory.property.SlotPos;
import org.spongepowered.api.item.inventory.query.QueryOperationTypes;

import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Supplier;

public class RowConfiguration {

    private final Map<Integer, RowConfigurationButton> config;

    public RowConfiguration(Map<Integer, RowConfigurationButton> config) {
        this.config = config;
    }

    public static Builder builder() {
        return new RowConfigurationBuilder();
    }

    public Inventory.Builder applyListeners(Inventory.Builder invBuilder, InventoryDimension dimension, int row) {
        return invBuilder.listener(ClickInventoryEvent.Primary.class, event -> {
            Optional<Integer> index = event.getSlot().flatMap(slot -> slot.getInventoryProperty(SlotIndex.class)).map(SlotIndex::getValue);
            if (index.isPresent()) {
                int clickedRow = index.get() / dimension.getColumns();
                int clickedColumn = index.get() % dimension.getColumns();
                if (clickedRow != row) {
                    return;
                }
                event.getCursorTransaction().setValid(false);
                RowConfigurationButton button = this.config.get(clickedColumn);
                if (button != null && button.shouldApply()) {
                    event.getCause().first(Player.class).ifPresent(button::onClick);
                }
                else {
                    event.setCancelled(true);
                }
            }
        });
    }

    public void applyItems(Inventory inv, int row) {
        int maxColumn = config.keySet().stream().mapToInt(i -> i).max().orElse(0);
        final InventoryDimension dimension = inv.getInventoryProperty(InventoryDimension.class)
                .orElseThrow(() -> new IllegalStateException("Cannot get inventory dimension!"));
        if (row > dimension.getRows()) {
            throw new IllegalArgumentException("Inventory does not have row " + row + ", it only has " + dimension.getRows() + " rows");
        }
        if (maxColumn >= dimension.getColumns()) {
            throw new IllegalArgumentException("Inventory is too small to fit this row configuration. Cannot add button in column index " + maxColumn + ", inventory only has " + dimension.getColumns() + " columns");
        }
        for (Map.Entry<Integer, RowConfigurationButton> entry : this.config.entrySet()) {
            ItemStack icon = entry.getValue().shouldApply() ? entry.getValue().getIcon() : entry.getValue().getNotAppliedItem();
            if (icon == null) {
                continue;
            }
            inv.query(QueryOperationTypes.INVENTORY_PROPERTY.of(SlotPos.of(entry.getKey(), row))).set(icon);
        }
    }

    public interface Builder {

        Builder withButton(int index, ItemStack icon, Consumer<Player> run);

        Builder withConditionalButton(int index, ItemStack icon, Consumer<Player> run, Supplier<Boolean> shouldApply);

        Builder withConditionalButton(int index, ItemStack icon, Consumer<Player> run, Supplier<Boolean> shouldApply, ItemStack notAppliedItem);

        RowConfiguration build();

    }
}
