package com.minecraftonline.penguindungeons.data.spawning;

import org.spongepowered.api.data.manipulator.immutable.common.AbstractImmutableBooleanData;

import com.minecraftonline.penguindungeons.data.PenguinDungeonKeys;

public class ImmutablePDSpawnData extends AbstractImmutableBooleanData<ImmutablePDSpawnData, PDSpawnData>
{

	public ImmutablePDSpawnData(boolean value)
	{
		super(PenguinDungeonKeys.PD_SPAWN, value, false);
	}
	
    @Override
    public PDSpawnData asMutable()
    {
        return new PDSpawnData(this.getValue());
    }

    @Override
    public int getContentVersion()
    {
        return 1;
    }

}
