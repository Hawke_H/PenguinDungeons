package com.minecraftonline.penguindungeons.data.bossbar;

import java.util.Optional;

import org.spongepowered.api.boss.BossBarColor;
import org.spongepowered.api.boss.BossBarColors;
import org.spongepowered.api.data.DataContainer;
import org.spongepowered.api.data.DataHolder;
import org.spongepowered.api.data.manipulator.mutable.common.AbstractSingleCatalogData;
import org.spongepowered.api.data.merge.MergeFunction;

import com.minecraftonline.penguindungeons.data.PenguinDungeonKeys;

public class PDBossBarColorData extends AbstractSingleCatalogData<BossBarColor, PDBossBarColorData, ImmutablePDBossBarColorData>
{

	public PDBossBarColorData(BossBarColor value)
	{
        super(PenguinDungeonKeys.PD_BOSS_BAR_COLOR, value, BossBarColors.RED);
    }

    @Override
    public Optional<PDBossBarColorData> fill(DataHolder dataHolder, MergeFunction overlap)
    {
        dataHolder.get(PDBossBarColorData.class).ifPresent((data) -> {
            PDBossBarColorData finalData = overlap.merge(this, data);
            setValue(finalData.getValue());
        });
        return Optional.of(this);
    }

    @Override
    public Optional<PDBossBarColorData> from(DataContainer container)
    {
        Optional<BossBarColor> optColor = container.getObject(this.usedKey.getQuery(), BossBarColor.class);
        return optColor.map(PDBossBarColorData::new);
    }

    @Override
    public PDBossBarColorData copy()
    {
        return new PDBossBarColorData(this.getValue());
    }

    @Override
    public ImmutablePDBossBarColorData asImmutable()
    {
        return new ImmutablePDBossBarColorData(this.getValue());
    }

    @Override
    public int getContentVersion()
    {
        return 1;
    }
}
