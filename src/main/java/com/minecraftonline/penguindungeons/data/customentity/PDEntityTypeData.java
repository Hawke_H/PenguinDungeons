package com.minecraftonline.penguindungeons.data.customentity;

import com.minecraftonline.penguindungeons.data.PenguinDungeonKeys;
import com.minecraftonline.penguindungeons.util.ResourceKey;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.data.DataContainer;
import org.spongepowered.api.data.DataHolder;
import org.spongepowered.api.data.manipulator.mutable.common.AbstractSingleData;
import org.spongepowered.api.data.merge.MergeFunction;
import org.spongepowered.api.data.value.mutable.Value;

import java.util.Optional;

public class PDEntityTypeData extends AbstractSingleData<ResourceKey, PDEntityTypeData, ImmutablePDEntityTypeData> {

	public PDEntityTypeData(ResourceKey value) {
		super(PenguinDungeonKeys.PD_ENTITY_TYPE, value);
	}

	public Value<ResourceKey> entityType() {
		return Sponge.getRegistry().getValueFactory()
				.createValue(PenguinDungeonKeys.PD_ENTITY_TYPE, getValue());
	}

	@Override
	protected Value<ResourceKey> getValueGetter() {
		return entityType();
	}

	@Override
	protected DataContainer fillContainer(DataContainer dataContainer) {
		return dataContainer.set(this.usedKey.getQuery(), this.getValue().asString());
	}

	@Override
	public Optional<PDEntityTypeData> fill(DataHolder dataHolder, MergeFunction overlap) {
		dataHolder.get(PDEntityTypeData.class).ifPresent((data) -> {
			PDEntityTypeData finalData = overlap.merge(this, data);
			setValue(finalData.getValue());
		});
		return Optional.of(this);
	}

	@Override
	public Optional<PDEntityTypeData> from(DataContainer container) {
		Optional<String> type = container.getString(PenguinDungeonKeys.PD_ENTITY_TYPE.getQuery());
		return type.flatMap(ResourceKey::tryResolve).map(PDEntityTypeData::new);
	}

	@Override
	public PDEntityTypeData copy() {
		return new PDEntityTypeData(this.getValue());
	}

	@Override
	public ImmutablePDEntityTypeData asImmutable() {
		return new ImmutablePDEntityTypeData(this.getValue());
	}

	@Override
	public int getContentVersion() {
		return 1;
	}
}
