package com.minecraftonline.penguindungeons.data;

import com.minecraftonline.penguindungeons.PenguinDungeons;
import com.minecraftonline.penguindungeons.ai.AttackIfReachable;
import com.minecraftonline.penguindungeons.ai.AttackPlayerIfReachable;
import com.minecraftonline.penguindungeons.ai.AttackRangedBow;
import com.minecraftonline.penguindungeons.ai.DelegatingToMCAI;
import com.minecraftonline.penguindungeons.ai.ShulkerDelegatingToMCAI;
import com.minecraftonline.penguindungeons.customentity.ArchProjectileAttack;
import com.minecraftonline.penguindungeons.customentity.AvoidTarget;
import com.minecraftonline.penguindungeons.customentity.AvoidTargetOnLowHealth;
import com.minecraftonline.penguindungeons.customentity.EntityAIHurtByNonPD;
import com.minecraftonline.penguindungeons.customentity.FollowFromDistance;
import com.minecraftonline.penguindungeons.customentity.JumpToTarget;
import com.minecraftonline.penguindungeons.customentity.PanicWithRider;
import com.minecraftonline.penguindungeons.customentity.StraightProjectileAttack;
import com.minecraftonline.penguindungeons.customentity.SummonMinion;
import com.minecraftonline.penguindungeons.customentity.bee.AngryBombee;
import com.minecraftonline.penguindungeons.customentity.ShieldAttack;
import com.minecraftonline.penguindungeons.customentity.ShootLasers;
import com.minecraftonline.penguindungeons.customentity.golem.ExplodingSnowGolem;
import com.minecraftonline.penguindungeons.customentity.golem.IceSnowGolem;
import com.minecraftonline.penguindungeons.customentity.shulker.standard.ExplodingShulker;
import com.minecraftonline.penguindungeons.customentity.shulker.standard.PaladinShulker;
import com.minecraftonline.penguindungeons.customentity.zombie.FireworkZombie;

import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.ai.EntityAINearestAttackableTarget;
import net.minecraft.entity.player.EntityPlayer;

import org.spongepowered.api.Sponge;
import org.spongepowered.api.entity.ai.task.AITaskType;
import org.spongepowered.api.entity.ai.task.AbstractAITask;
import org.spongepowered.api.entity.living.Agent;
import org.spongepowered.api.entity.living.golem.Shulker;

public class PenguinDungeonAITaskTypes {

    public static AITaskType SHULKER_SNIPE;

    public static AITaskType SHULKER_EXPLODE;

    public static AITaskType SEARCH_FOR_EXPLODABLES;

    public static AITaskType SHULKER_FIERY_ATTACK;

    public static AITaskType SHULKER_BLINDING_ATTACK;

    public static AITaskType SHULKER_EFFECT_ATTACK;

    public static AITaskType CHANGE_COLOUR;

    public static AITaskType SHULKER_INVULNERABLE_WHEN_CLOSED;

    public static AITaskType SHULKER_INVULNERABLE_BULLETS;

    public static AITaskType STRAIGHT_PROJECTILE_RANGED_ATTACK;

    public static AITaskType SNOW_GOLEM_MELEE_ATTACK;

    public static AITaskType SNOW_GOLEM_EXPLODE;

    public static AITaskType SHOOT_LASERS;

    public static AITaskType FOLLOW_DISTANCE;

    public static AITaskType SHIELD_ATTACK;

    public static AITaskType ATTACK_BACK_NON_PD;

    public static AITaskType FIREWORK_ZOMBIE_EXPLODE;

    public static AITaskType COPY_RIDER_TARGET;

    public static AITaskType ARCH_PROJECTILE_RANGED_ATTACK;

    public static AITaskType JUMP_AT_TARGET;

    public static AITaskType BEE_EXPLODE;

    public static AITaskType AVOID_TARGET;

    public static AITaskType AVOID_TARGET_ON_LOW_HEALTH;

    public static AITaskType PANIC_WITH_PLAYER_RIDER;

    public static AITaskType SHOOT_WITH_BOW;

    public static AITaskType ATTACK_IF_REACHABLE;

    public static AITaskType ATTACK_PLAYER_IF_REACHABLE;

    public static AITaskType SUMMON_MINION;

    public static void register() {
        SHULKER_EXPLODE = register( "shulker_explode", "Shulker Explode", ExplodingShulker.StartExploding.class);

        SEARCH_FOR_EXPLODABLES = register("search_for_explodables", "Shulker search for explodables", FindExplodableTarget.class);

        SHULKER_FIERY_ATTACK = registerShulkerDelegating("shulker_fiery_attack", "Shulker Fiery Attack");

        SHULKER_EFFECT_ATTACK = registerShulkerDelegating("shulker_effect_attack", "Shulker Effect Attack");

        CHANGE_COLOUR = registerShulkerDelegating("shulker_change_colour", "Shulker Change Colour");

        SHULKER_INVULNERABLE_WHEN_CLOSED = register("shulker_invulnerable_when_closed", "Shulker Invulnerable When Closed", PaladinShulker.InvulnerableWhenClosed.class);

        SHULKER_INVULNERABLE_BULLETS = registerShulkerDelegating("shulker_invulnerable_bullets", "Shulker Invulnerable Bullets");

        STRAIGHT_PROJECTILE_RANGED_ATTACK = register("straight_shulker_bullet_attack", "Straight Ranged Attack", StraightProjectileAttack.RangedAttack.class);

        SNOW_GOLEM_MELEE_ATTACK = register("snow_golem_melee_attack", "Snow Golem Melee Attack", IceSnowGolem.MeleeAttack.class);

        SNOW_GOLEM_EXPLODE = register( "snow_golem_explode", "Snow Golem Explode", ExplodingSnowGolem.StartExploding.class);

        SHOOT_LASERS = register( "laser_attack", "Laser Attack", ShootLasers.LaserAttack.class);

        FOLLOW_DISTANCE = register( "follow_distance", "Follow from a distance", FollowFromDistance.FollowDistance.class);

        SHIELD_ATTACK = register( "shield_attack", "Follow from a distance", ShieldAttack.AttackWithShield.class);

        ATTACK_BACK_NON_PD = register( "attack_back_non_pd", "Attack other living entities that hurt this and are not from Penguin Dungeons", EntityAIHurtByNonPD.TargetNonPDAttackers.class);

        FIREWORK_ZOMBIE_EXPLODE = register( "firework_zombie_explode", "Firework Zombie Explode", FireworkZombie.StartExploding.class);

        ARCH_PROJECTILE_RANGED_ATTACK = register("arch_projectile_attack", "Arched Ranged Attack", ArchProjectileAttack.RangedArchAttack.class);

        JUMP_AT_TARGET = register("jump_at_target", "Jump At Target", JumpToTarget.JumpAtTarget.class);

        BEE_EXPLODE = register("bee_explode", "Bee Explode", AngryBombee.StartExploding.class);

        AVOID_TARGET = register("avoid_target", "Avoid Target", AvoidTarget.AvoidTargetTask.class);

        AVOID_TARGET_ON_LOW_HEALTH = register("avoid_target_on_low_health", "Avoid Target On Low Health", AvoidTargetOnLowHealth.AvoidTargetOnLowHealthTask.class);

        PANIC_WITH_PLAYER_RIDER = register("panic_with_player_rider", "Panic when ridden by player", PanicWithRider.PanicWithPlayerRiderTask.class);

        SHOOT_WITH_BOW = register("shoot_with_bow", "Shoot if holding a bow", AttackRangedBow.ShootWithBow.class);

        ATTACK_IF_REACHABLE = register("attack_if_reachable", "Melee attack the target if they are reachable, but don't attempt to move", AttackIfReachable.AttackIfReachableTask.class);

        ATTACK_PLAYER_IF_REACHABLE = register("attack_if_reachable", "Melee attack any player if they are reachable, but don't attempt to move", AttackPlayerIfReachable.AttackPlayerIfReachableTask.class);

        SUMMON_MINION = register("summon_minion", "Summons a specified Penguin Dungeons custom entity", SummonMinion.SummonMinionTask.class);
    }

    private static AITaskType register(final String id, final String name, Class<? extends AbstractAITask<? extends Agent>> aiClass) {
        return Sponge.getRegistry().registerAITaskType(PenguinDungeons.getInstance(), id, name, aiClass);
    }

    private static AITaskType registerShulkerDelegating(final String id, final String name) {
        return register(id, name, ShulkerDelegatingToMCAI.class);
    }

    public static class FindExplodableTarget extends DelegatingToMCAI<Shulker> {

        public FindExplodableTarget(EntityCreature creature) {
            super(PenguinDungeonAITaskTypes.SEARCH_FOR_EXPLODABLES, new MCFindExplodableTarget(creature));
        }
    }

    public static class MCFindExplodableTarget extends EntityAINearestAttackableTarget<EntityPlayer> {

        public MCFindExplodableTarget(EntityCreature creature) {
            super(creature, EntityPlayer.class, true);
        }

        @Override
        protected double getTargetDistance() {
            return 5;
        }
    }
}
