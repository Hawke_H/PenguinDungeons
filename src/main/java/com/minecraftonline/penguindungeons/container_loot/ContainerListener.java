package com.minecraftonline.penguindungeons.container_loot;

import java.util.Optional;
import java.util.Random;
import java.util.Set;

import org.spongepowered.api.block.BlockType;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.data.DataQuery;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.HandTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.entity.living.player.gamemode.GameModes;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.block.InteractBlockEvent;
import org.spongepowered.api.event.cause.EventContextKeys;
import org.spongepowered.api.event.filter.cause.First;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.ItemStackSnapshot;
import org.spongepowered.api.util.Direction;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import com.google.common.collect.Sets;
import com.minecraftonline.penguindungeons.PenguinDungeons;
import com.minecraftonline.penguindungeons.data.PenguinDungeonKeys;
import com.minecraftonline.penguindungeons.data.container_loot.ContainerLootableData;
import com.minecraftonline.penguindungeons.util.ResourceKey;
import com.minecraftonline.penguindungeons.wand.PDWand;

import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.InventoryLargeChest;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.ILockableContainer;
import net.minecraft.world.WorldServer;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.world.storage.loot.LootContext;
import net.minecraft.world.storage.loot.LootTable;

public class ContainerListener {

    private static final int MS_PER_SEC = 1000;
    private static final int SEC_PER_HOUR = 3600;

    private static final int LOOTABLE_DELAY = SEC_PER_HOUR * MS_PER_SEC; // 1 hour in milliseconds

    public static final Set<BlockType> containerBlocks = Sets.newHashSet(
            BlockTypes.CHEST, BlockTypes.TRAPPED_CHEST, BlockTypes.DISPENSER, BlockTypes.DROPPER, BlockTypes.HOPPER,
            BlockTypes.BLACK_SHULKER_BOX, BlockTypes.BLUE_SHULKER_BOX, BlockTypes.BROWN_SHULKER_BOX, BlockTypes.CYAN_SHULKER_BOX,
            BlockTypes.GRAY_SHULKER_BOX, BlockTypes.GREEN_SHULKER_BOX, BlockTypes.LIGHT_BLUE_SHULKER_BOX, BlockTypes.LIME_SHULKER_BOX,
            BlockTypes.MAGENTA_SHULKER_BOX, BlockTypes.ORANGE_SHULKER_BOX, BlockTypes.PINK_SHULKER_BOX, BlockTypes.PURPLE_SHULKER_BOX,
            BlockTypes.RED_SHULKER_BOX, BlockTypes.SILVER_SHULKER_BOX, BlockTypes.WHITE_SHULKER_BOX, BlockTypes.YELLOW_SHULKER_BOX,
            BlockTypes.BREWING_STAND, BlockTypes.BEACON, BlockTypes.FURNACE);

    public static final Direction[] HORIZONTAL_DIRECTIONS = new Direction[]{Direction.NORTH, Direction.EAST, Direction.SOUTH, Direction.WEST};

    @Listener
    public void onInteractContainer(InteractBlockEvent.Secondary event, @First Player player) {
        if (player.gameMode() == GameModes.SPECTATOR) return;

        Optional<ItemStackSnapshot> usedItem = event.getContext().get(EventContextKeys.USED_ITEM);

        // ignore shift clicks with items
        if (usedItem.isPresent() && player.get(Keys.IS_SNEAKING).orElse(false)) return;

        if (usedItem.isPresent() && PDWand.isPDWand(usedItem.get())) return; // handled by PDWand listener

        Location<World> block = event.getTargetBlock().getLocation().get();
        if (!containerBlocks.contains(block.getBlockType())) return;

        Optional<String> lock = block.get(Keys.LOCK_TOKEN);

        // check for adjacent chest
        BlockType blockType = block.getBlockType();
        Optional<Location<World>> adjacentChest = Optional.empty();
        if (blockType == BlockTypes.CHEST || blockType == BlockTypes.TRAPPED_CHEST) {
            for (Direction direction : HORIZONTAL_DIRECTIONS) {
                Location<World> adjacentBlock = block.getBlockRelative(direction);
                if (adjacentBlock.getBlockType() == blockType) {
                    Optional<String> adjacentLock = adjacentBlock.get(Keys.LOCK_TOKEN);
                    if (direction != Direction.SOUTH && direction != Direction.EAST) {
                        // this adjacent chest is the main chest, swap them around
                        adjacentChest = Optional.of(block);
                        block = adjacentBlock;
                        // override lock with the lock of this chest if present
                        if (adjacentLock.isPresent()) lock = adjacentLock;
                    } else {
                        adjacentChest = Optional.of(adjacentBlock);
                        // try get lock from this adjacent block if first block does not have it
                        if (!lock.isPresent()) lock = adjacentLock;
                    }
                    break;
                }
            }
        }

        if (lock.isPresent()) {
            // keys only work in main hand
            Optional<ItemStack> key = player.getItemInHand(HandTypes.MAIN_HAND);
            if (!key.isPresent()) return;
            // use a data query to get the item name as a String instead of Text
            // and because asking Sponge for an item's display name also gets book names
            // which are not accepted as key names for locked chests
            Optional<String> keyName = key.get().toContainer().getString(
                DataQuery.of("UnsafeData", "display", "Name"));
            if (!keyName.isPresent() || !keyName.get().equals(lock.get())) return;
        }

        Optional<ResourceKey> lootTableKey = block.get(PenguinDungeonKeys.CONTAINER_LOOT);
        if (lootTableKey.isPresent()) {
            Optional<Long> lootableTime = block.get(PenguinDungeonKeys.CONTAINER_LOOTABLE);
            long currentTime = System.currentTimeMillis();
            if (!lootableTime.isPresent() || lootableTime.get() <= currentTime) {
                ResourceLocation resource;
                if (lootTableKey.get().namespace() == ResourceKey.PENGUIN_DUNGEONS_NAMESPACE) {
                    resource = PenguinDungeons.getInstance().getLootTableFor(lootTableKey.get());
                } else {
                    resource = new ResourceLocation(lootTableKey.get().asString());
                }
                LootTable loottable = ((net.minecraft.world.World) player.getWorld()).getLootTableManager().getLootTableFromLocation(resource);
                Random random = new Random();

                LootContext.Builder lootContextBuilder = new LootContext.Builder((WorldServer)player.getWorld());
                if (player != null) {
                    lootContextBuilder.withLuck(((EntityPlayer) player).getLuck());
                }

                if (block.getTileEntity().isPresent()) {
                    IInventory inventory;
                    boolean clearSlot3 = false;
                    boolean clearSlot4 = false;
                    if (adjacentChest.isPresent() && adjacentChest.get().getTileEntity().isPresent()) {
                        // fill double chest with loot
                        inventory = new InventoryLargeChest("container.chestDouble",
                                (ILockableContainer) block.getTileEntity().get(),
                                (ILockableContainer) adjacentChest.get().getTileEntity().get());
                    } else {
                        // fill single container with loot
                        inventory = (IInventory) block.getTileEntity().get();

                        if (block.getBlockType() == BlockTypes.BREWING_STAND) {
                            // fill in non-potion slots first so reward items don't end up in them
                            // there is probably a Sponge way to do this, but couldn't find a good way to see specific slots
                            if (inventory.getStackInSlot(3).isEmpty()) {
                                inventory.setInventorySlotContents(3, new net.minecraft.item.ItemStack(Items.NETHER_WART));
                                clearSlot3 = true;
                            }
                            if (inventory.getStackInSlot(4).isEmpty()) {
                                inventory.setInventorySlotContents(4, new net.minecraft.item.ItemStack(Items.BLAZE_POWDER));
                                clearSlot4 = true;
                            }
                        }
                    }

                    loottable.fillInventory(inventory, random, lootContextBuilder.build());

                    // remove temporary items that were filling slots
                    if (clearSlot3) inventory.removeStackFromSlot(3);
                    if (clearSlot4) inventory.removeStackFromSlot(4);

                    // set a new lootable time, 1 hour +/- up to half an hour
                    block.offer(new ContainerLootableData((long) (currentTime + (LOOTABLE_DELAY + ((LOOTABLE_DELAY * random.nextFloat()) - (LOOTABLE_DELAY / 2))))));
                    inventory.markDirty();
                }
            }
        }
    }
    
}
