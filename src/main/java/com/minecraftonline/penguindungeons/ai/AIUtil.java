package com.minecraftonline.penguindungeons.ai;

import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityAreaEffectCloud;
import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.monster.EntityShulker;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.projectile.EntityDragonFireball;
import net.minecraft.entity.projectile.EntityFireball;
import net.minecraft.entity.projectile.EntityShulkerBullet;
import net.minecraft.entity.projectile.EntityThrowable;
import net.minecraft.init.SoundEvents;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.EnumDifficulty;
import net.minecraft.world.World;

import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.effect.potion.PotionEffect;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.ai.Goal;
import org.spongepowered.api.entity.ai.GoalTypes;
import org.spongepowered.api.entity.ai.task.AITask;
import org.spongepowered.api.entity.ai.task.AITaskType;
import org.spongepowered.api.entity.living.Agent;
import org.spongepowered.api.entity.living.golem.Shulker;

import com.minecraftonline.penguindungeons.customentity.HasEffects;
import com.minecraftonline.penguindungeons.customentity.HasProjectileData;
import com.minecraftonline.penguindungeons.customentity.wither.DragonFireball;
import com.minecraftonline.penguindungeons.customentity.wither.Egg;
import com.minecraftonline.penguindungeons.customentity.wither.LargeFireball;
import com.minecraftonline.penguindungeons.customentity.wither.SkullProjectile;
import com.minecraftonline.penguindungeons.customentity.wither.SmallFireball;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;

public class AIUtil {

    public static void swapShulkerAttackForCustom(Shulker shulker, ShulkerBulletCreator shulkerBulletCreator, AITaskType aiTaskType, String name) {
        EntityShulker mcShulker = (EntityShulker) shulker;
        EntityShulker.AIAttack delegate = mcShulker.new AIAttack() {
            @Override
            public void updateTask() {
                AtomicInteger atomicInteger = new AtomicInteger(this.attackTime);
                AIUtil.shulkerAttackWithCustomBullet(mcShulker, atomicInteger, shulkerBulletCreator, name);
                this.attackTime = atomicInteger.get();
            }
        };

        Goal<Agent> normalGoals = shulker.getGoal(GoalTypes.NORMAL).get();
        AIUtil.removeShulkerAttack(normalGoals);


        normalGoals.addTask(4, new DelegatingToMCAI<Shulker>(aiTaskType, delegate));
    }

    public static void removeShulkerAttack(Goal<Agent> goal) {
        goal.getTasks().stream().filter(aiTask -> aiTask instanceof EntityShulker.AIAttack)
                .findAny().ifPresent(task -> goal.removeTask((AITask<? extends Agent>) task));
    }

    public interface ShulkerBulletCreator {
        EntityShulkerBullet create(World world, EntityLivingBase owner, EntityLivingBase target, EnumFacing.Axis axis);

        static ShulkerBulletCreator forCustom(Consumer<EntityLivingBase> onHit) {
            return (world, owner, target, axis) -> new ShulkerBulletCollideEffect(world, owner, target, axis, onHit);
        }

        static ShulkerBulletCreator forCustom(Consumer<EntityLivingBase> onHit, SoundEvent sound) {
            return (world, owner, target, axis) -> new ShulkerBulletCollideEffect(world, owner, target, axis, onHit, sound);
        }

        static ShulkerBulletCreator forCustomEffect(PotionEffect potionEffect) {
            return forCustom(forHit(potionEffect));
        }

        static ShulkerBulletCreator forClearEffects() {
            return forCustom(clearEffects());
        }

        static ShulkerBulletCreator forInvulnerable() {
            return (world, owner, target, axis) -> new ShulkerBulletInvulnerable(world, owner, target, axis);
        }

        static Consumer<EntityLivingBase> forHit(PotionEffect potionEffect) {
            return forHit(Arrays.asList(potionEffect));
        }

        static Consumer<EntityLivingBase> forHit2(List<net.minecraft.potion.PotionEffect> potionEffects) {
            return entity -> {
                Entity spongeEntity = (Entity)entity;
                List<PotionEffect> effects = spongeEntity.get(Keys.POTION_EFFECTS).orElse(new ArrayList<>());
                potionEffects.forEach((potionEffect) -> {
                    effects.add((PotionEffect) potionEffect);
                });
                spongeEntity.offer(Keys.POTION_EFFECTS, effects);
            };
        }

        static Consumer<EntityLivingBase> forHit(List<PotionEffect> potionEffects) {
            return entity -> {
                Entity spongeEntity = (Entity)entity;
                List<PotionEffect> effects = spongeEntity.get(Keys.POTION_EFFECTS).orElse(new ArrayList<>());
                potionEffects.forEach((potionEffect) -> {
                    effects.add(potionEffect);
                });
                spongeEntity.offer(Keys.POTION_EFFECTS, effects);
            };
        }

        static Consumer<EntityLivingBase> clearEffects() {
            return entity -> {
                Entity spongeEntity = (Entity)entity;
                spongeEntity.remove(Keys.POTION_EFFECTS);
            };
        }
    }

    public interface DirectShulkerBulletCreator {
        EntityShulkerBullet create(World world, EntityLivingBase shooter, EntityLivingBase target, double accelX, double accelY, double accelZ);

        static DirectShulkerBulletCreator forDirectBullet(Consumer<EntityLivingBase> onHit)
        {
            return (world, shooter, target, accelX, accelY, accelZ) -> new ShulkerBulletDirect(world, shooter, target, onHit, accelX, accelY, accelZ);
        }

        static DirectShulkerBulletCreator forDirectBullet(Consumer<EntityLivingBase> onHit, SoundEvent sound)
        {
            return (world, shooter, target, accelX, accelY, accelZ) -> new ShulkerBulletDirect(world, shooter, target, onHit, accelX, accelY, accelZ, sound);
        }
    }

    public interface ProjectileForImpact {
        // returns true if impact is valid and projectile should be set to dead
        boolean forImpact(RayTraceResult result, AnyProjectile projectile);

        static ProjectileForImpact forMultipleImpacts(ProjectileForImpact... others) {
            return (result, projectile) -> {
                boolean hit = false;
                for (ProjectileForImpact base : others)
                {
                    boolean isHit = base.forImpact(result, projectile);
                    if (!hit && isHit) hit = isHit;
                }
                return hit;
            };
        }

        static ProjectileForImpact forImpactWithSound(ProjectileForImpact base, SoundEvent sound) {
            return (result, projectile) -> {
                boolean hit = base.forImpact(result, projectile);
                if (hit) projectile.getMinecraftEntity().playSound(sound, 1.0F, 0);
                return hit;
            };
        }

        static ProjectileForImpact forImpactDamage(float damage) {
            return forImpactDamage(damage, false);
        }

        static ProjectileForImpact forImpactDamage(float damage, boolean fireball) {
            return (result, projectile) -> {
                if (result.entityHit == null || !hasHitShooter(result.entityHit, projectile.getShooter())) {
                    if (result.entityHit != null) {
                        if (projectile.getShooter() != null) {
                            if (fireball && projectile.getMinecraftEntity() instanceof EntityFireball) {
                                result.entityHit.attackEntityFrom(DamageSource.causeFireballDamage((EntityFireball) projectile.getMinecraftEntity(), projectile.getShooter()), damage);
                            } else if (projectile.getMinecraftEntity() instanceof EntityThrowable) {
                                result.entityHit.attackEntityFrom(DamageSource.causeThrownDamage(projectile.getMinecraftEntity(), projectile.getShooter()), damage);
                            } else {
                                result.entityHit.attackEntityFrom(DamageSource.causeIndirectDamage(projectile.getMinecraftEntity(), projectile.getShooter()), damage);
                            }
                        } else {
                            result.entityHit.attackEntityFrom(DamageSource.MAGIC, damage);
                        }
                    }
                    return true;
                }
                return false;
            };
        }

        static ProjectileForImpact forImpactAreaOfEffect(float radius, int duration, HasProjectileData data) {
            return (result, projectile) -> {
                if (result.entityHit == null || !hasHitShooter(result.entityHit, projectile.getShooter())) {
                    List<EntityLivingBase> list = projectile.getMinecraftEntity().world.<EntityLivingBase>getEntitiesWithinAABB(EntityLivingBase.class, projectile.getMinecraftEntity().getEntityBoundingBox().grow(4.0D, 2.0D, 4.0D));
                    EntityAreaEffectCloud entityareaeffectcloud = new EntityAreaEffectCloud(
                            projectile.getMinecraftEntity().world, projectile.getMinecraftEntity().posX, projectile.getMinecraftEntity().posY, projectile.getMinecraftEntity().posZ);
                    entityareaeffectcloud.setOwner(projectile.getShooter());
                    entityareaeffectcloud.setParticle(data.getParticle());
                    entityareaeffectcloud.setParticleParam1(data.getParticleParam1());
                    entityareaeffectcloud.setParticleParam2(data.getParticleParam2());
                    entityareaeffectcloud.setRadius(radius);
                    entityareaeffectcloud.setDuration(duration);
                    entityareaeffectcloud.setRadiusPerTick((7.0F - entityareaeffectcloud.getRadius()) / (float)entityareaeffectcloud.getDuration());
                    data.getEffects().forEach((effect) -> {
                        entityareaeffectcloud.addEffect(effect);
                    });
                    if (!list.isEmpty()) {
                        for(EntityLivingBase entitylivingbase : list) {
                            double d0 = projectile.getMinecraftEntity().getDistanceSq(entitylivingbase);
                            if (d0 < 16.0D) {
                                entityareaeffectcloud.setPosition(entitylivingbase.posX, entitylivingbase.posY, entitylivingbase.posZ);
                                break;
                            }
                        }
                    }

                    if (projectile.getMinecraftEntity() instanceof EntityDragonFireball) {
                        projectile.getMinecraftEntity().world.playEvent(2006, new BlockPos(projectile.getMinecraftEntity().posX, projectile.getMinecraftEntity().posY, projectile.getMinecraftEntity().posZ), 0);
                    }
                    projectile.getMinecraftEntity().world.spawnEntity(entityareaeffectcloud);
                    return true;
                }
                return false;
            };
        }

        static ProjectileForImpact forImpactExplosion(float strength, boolean causesFire, boolean damageTerrain) {
            return (result, projectile) -> {
                if (result.entityHit == null || !hasHitShooter(result.entityHit, projectile.getShooter())) {
                    projectile.getMinecraftEntity().world.newExplosion(projectile.getMinecraftEntity(),
                            projectile.getMinecraftEntity().posX, projectile.getMinecraftEntity().posY, projectile.getMinecraftEntity().posZ,
                            strength, causesFire, damageTerrain);
                    return true;
                }
                return false;
            };
        }

        static ProjectileForImpact forImpactSetEffects(HasEffects data) {
            return forImpactSetEffects(data, true);
        }

        static ProjectileForImpact forImpactSetEffects(HasEffects data, boolean withExplosion) {
            return (result, projectile) -> {
                if (result.entityHit == null || !hasHitShooter(result.entityHit, projectile.getShooter())) {
                    if (result.entityHit != null && result.entityHit instanceof EntityLivingBase) {
                        data.getEffects().forEach((effect) -> {
                            ((EntityLivingBase)result.entityHit).addPotionEffect(effect);
                        });
                    }
                    if (withExplosion) {
                        projectile.getMinecraftEntity().world.newExplosion(projectile.getMinecraftEntity(),
                                projectile.getMinecraftEntity().posX, projectile.getMinecraftEntity().posY, projectile.getMinecraftEntity().posZ,
                                1.0F, false, false);
                    }
                    return true;
                }
                return false;
            };
        }

        static ProjectileForImpact forImpactSetOnFire(int duration, float damage) {
            return (result, projectile) -> {
                if (result.entityHit == null || !hasHitShooter(result.entityHit, projectile.getShooter())) {
                    if (result.entityHit != null && !result.entityHit.isImmuneToFire()) {
                        boolean flag;
                        if (projectile.getEntity() instanceof EntityFireball) {
                            flag = result.entityHit.attackEntityFrom(DamageSource.causeFireballDamage((EntityFireball) projectile.getMinecraftEntity(), projectile.getShooter()), damage);
                        } else {
                            flag = result.entityHit.attackEntityFrom(DamageSource.causeIndirectDamage(projectile.getMinecraftEntity(), projectile.getShooter()), damage);
                        }
                        if (flag) {
                            result.entityHit.setFire(duration);
                        }
                    }
                    return true;
                }
                return false;
            };
        }

        static ProjectileForImpact forImpactTeleportRandomly() {
            return (result, projectile) -> {
                if (result.entityHit == null || !hasHitShooter(result.entityHit, projectile.getShooter())) {
                    if (result.entityHit != null && !result.entityHit.getIsInvulnerable()) {
                        if (result.entityHit instanceof EntityPlayer || result.entityHit instanceof EntityLiving) {
                            randomlyTeleportNearby((EntityLivingBase) result.entityHit);
                        }
                    }
                    return true;
                }
                return false;
            };
        }
    }

    public interface ProjectileCreator {
        AnyProjectile create(World world, EntityLivingBase shooter, double accelX, double accelY, double accelZ);

        static ProjectileCreator forSkull(ProjectileForImpact forImpact)
        {
            return (world, shooter, accelX, accelY, accelZ) -> new AnyProjectile((Entity)new SkullProjectile(world, shooter, accelX, accelY, accelZ, forImpact));
        }

        static ProjectileCreator forSkulls(ProjectileForImpact forImpact, ProjectileForImpact forBoostedImpact)
        {
            return (world, shooter, accelX, accelY, accelZ) -> new AnyProjectile((Entity)new SkullProjectile(world, shooter, accelX, accelY, accelZ, forImpact, forBoostedImpact));
        }

        static ProjectileCreator forSkulls(ProjectileForImpact forImpact, ProjectileForImpact forBoostedImpact, float boostedChance)
        {
            return (world, shooter, accelX, accelY, accelZ) -> new AnyProjectile((Entity)new SkullProjectile(world, shooter, accelX, accelY, accelZ, forImpact, forBoostedImpact, boostedChance));
        }

        static ProjectileCreator forLargeFireball(ProjectileForImpact forImpact)
        {
            return (world, shooter, accelX, accelY, accelZ) -> new AnyProjectile((Entity)new LargeFireball(world, shooter, accelX, accelY, accelZ, forImpact));
        }

        static ProjectileCreator forSmallFireball(ProjectileForImpact forImpact)
        {
            return (world, shooter, accelX, accelY, accelZ) -> new AnyProjectile((Entity)new SmallFireball(world, shooter, accelX, accelY, accelZ, forImpact));
        }

        static ProjectileCreator forDragonFireball(ProjectileForImpact forImpact)
        {
            return (world, shooter, accelX, accelY, accelZ) -> new AnyProjectile((Entity)new DragonFireball(world, shooter, accelX, accelY, accelZ, forImpact));
        }

        static ProjectileCreator forEggs(ProjectileForImpact forImpact)
        {
            return (world, shooter, accelX, accelY, accelZ) -> new AnyProjectile((Entity)new Egg(world, shooter, accelX, accelY, accelZ, forImpact));
        }
    }

    public static void shulkerAttackWithCustomBullet(EntityShulker mcShulker, AtomicInteger attackTime, ShulkerBulletCreator shulkerBulletCreator, String name) {
        if (mcShulker.world.getDifficulty() != EnumDifficulty.PEACEFUL) {
            int attackTimeCur = attackTime.decrementAndGet();
            EntityLivingBase entitylivingbase = mcShulker.getAttackTarget();
            mcShulker.getLookHelper().setLookPositionWithEntity(entitylivingbase, 180.0F, 180.0F);
            double d0 = mcShulker.getDistanceSq(entitylivingbase);
            if (d0 < 400.0D) {
                if (attackTimeCur <= 0) {
                    attackTime.set(20 + mcShulker.world.rand.nextInt(10) * 20 / 2);
                    EntityShulkerBullet entityshulkerbullet = shulkerBulletCreator.create(mcShulker.world, mcShulker, entitylivingbase, mcShulker.getAttachmentFacing().getAxis());
                    entityshulkerbullet.setCustomNameTag(name);
                    mcShulker.world.spawnEntity(entityshulkerbullet);
                    mcShulker.playSound(SoundEvents.ENTITY_SHULKER_SHOOT, 2.0F, (mcShulker.world.rand.nextFloat() - mcShulker.world.rand.nextFloat()) * 0.2F + 1.0F);
                }
            } else {
                mcShulker.setAttackTarget(null);
            }
        }
    }

    public static void randomlyTeleportNearby(EntityLivingBase entitylivingbase) {
        randomlyTeleportNearby(entitylivingbase, 16);
    }

    public static void randomlyTeleportNearby(EntityLivingBase entitylivingbase, int tries) {
        double d0 = entitylivingbase.posX;
        double d1 = entitylivingbase.posY;
        double d2 = entitylivingbase.posZ;

        for (int i = 0; i < tries; ++i)
        {
            double d3 = entitylivingbase.posX + (entitylivingbase.getRNG().nextDouble() - 0.5D) * 16.0D;
            double d4 = MathHelper.clamp(entitylivingbase.posY + (double)(entitylivingbase.getRNG().nextInt(16) - 8), 0.0D, (double)(entitylivingbase.world.getActualHeight() - 1));
            double d5 = entitylivingbase.posZ + (entitylivingbase.getRNG().nextDouble() - 0.5D) * 16.0D;

            if (entitylivingbase.isRiding())
            {
                entitylivingbase.dismountRidingEntity();
            }

            if (attemptTeleport(entitylivingbase, d3, d4, d5))
            {
                entitylivingbase.world.playSound((EntityPlayer)null, d0, d1, d2, SoundEvents.ITEM_CHORUS_FRUIT_TELEPORT, entitylivingbase.getSoundCategory(), 1.0F, 1.0F);
                entitylivingbase.playSound(SoundEvents.ITEM_CHORUS_FRUIT_TELEPORT, 1.0F, 1.0F);
                break;
            }
        }
    }

    // this is pretty much identical to EntityLivingBase.attemptTeleport
    // except it isn't telling Sponge that a teleport has happened
    // Sponge or another plugin is doing something causing these teleport attempts to be successful
    // when they should not actually be
    public static boolean attemptTeleport(EntityLivingBase entitylivingbase, double x, double y, double z) {
        double d0 = entitylivingbase.posX;
        double d1 = entitylivingbase.posY;
        double d2 = entitylivingbase.posZ;
        entitylivingbase.posX = x;
        entitylivingbase.posY = y;
        entitylivingbase.posZ = z;
        boolean flag = false;
        BlockPos blockpos = new BlockPos(entitylivingbase);
        World world = entitylivingbase.world;
        Random random = entitylivingbase.getRNG();
        if (world.isBlockLoaded(blockpos)) {
            boolean flag1 = false;

            while(!flag1 && blockpos.getY() > 0) {
                BlockPos blockpos1 = blockpos.down();
                IBlockState iblockstate = world.getBlockState(blockpos1);
                if (iblockstate.getMaterial().blocksMovement()) {
                    flag1 = true;
                } else {
                    --entitylivingbase.posY;
                    blockpos = blockpos1;
                }
            }

            if (flag1) {
                entitylivingbase.setPositionAndUpdate(entitylivingbase.posX, entitylivingbase.posY, entitylivingbase.posZ);
                if (world.getCollisionBoxes(entitylivingbase, entitylivingbase.getEntityBoundingBox()).isEmpty() && !world.containsAnyLiquid(entitylivingbase.getEntityBoundingBox())) {
                    flag = true;
                }
            }
        }

        if (!flag) {
            entitylivingbase.setPositionAndUpdate(d0, d1, d2);
            return false;
        } else {
            for(int j = 0; j < 128; ++j) {
                double d6 = (double)j / 127.0D;
              float f = (random.nextFloat() - 0.5F) * 0.2F;
              float f1 = (random.nextFloat() - 0.5F) * 0.2F;
              float f2 = (random.nextFloat() - 0.5F) * 0.2F;
              double d3 = d0 + (entitylivingbase.posX - d0) * d6 + (random.nextDouble() - 0.5D) * (double)entitylivingbase.width * 2.0D;
              double d4 = d1 + (entitylivingbase.posY - d1) * d6 + random.nextDouble() * (double)entitylivingbase.height;
              double d5 = d2 + (entitylivingbase.posZ - d2) * d6 + (random.nextDouble() - 0.5D) * (double)entitylivingbase.width * 2.0D;
              world.spawnParticle(EnumParticleTypes.PORTAL, d3, d4, d5, (double)f, (double)f1, (double)f2);
            }

            if (entitylivingbase instanceof EntityCreature) {
                ((EntityCreature)entitylivingbase).getNavigator().clearPath();
            }

            return true;
        }
    }

    public static boolean hasHitShooter(net.minecraft.entity.Entity entityHit, EntityLivingBase shooter) {
        if (entityHit == null) return false;
        if (shooter == null) return false;
        if (entityHit.isEntityEqual(shooter)) return true;
        if (entityHit.isRidingSameEntity(shooter)) return true;
        return (entityHit.isRidingOrBeingRiddenBy(shooter));
    }
}


