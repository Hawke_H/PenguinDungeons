package com.minecraftonline.penguindungeons.ai;

import com.minecraftonline.penguindungeons.mixin.EntityShulkerBulletAccessor;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.projectile.EntityShulkerBullet;
import net.minecraft.entity.projectile.ProjectileHelper;
import net.minecraft.init.SoundEvents;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;

import java.util.function.Consumer;

public class ShulkerBulletDirect extends EntityShulkerBullet {

    private final Consumer<EntityLivingBase> onHit;
    public EntityLivingBase shootingEntity;
    private int ticksInAir;
    public double accelerationX;
    public double accelerationY;
    public double accelerationZ;
    protected EnumParticleTypes particle;
    protected int particleParam1;
    protected int particleParam2;
    private final SoundEvent sound;

    public ShulkerBulletDirect(World worldIn) {
        this(worldIn, SoundEvents.ENTITY_SHULKER_BULLET_HIT);
    }

    public ShulkerBulletDirect(World worldIn, SoundEvent soundIn) {
        super(worldIn);
        onHit = x -> {};
        sound = soundIn;
    }

    public ShulkerBulletDirect(World worldIn, EntityLivingBase shooter, Entity targetIn, Consumer<EntityLivingBase> onHit,
            double accelX, double accelY, double accelZ) {
        this(worldIn, shooter, targetIn, onHit, accelX, accelY, accelZ, SoundEvents.ENTITY_SHULKER_BULLET_HIT);
    }

    public ShulkerBulletDirect(World worldIn, EntityLivingBase shooter, Entity targetIn, Consumer<EntityLivingBase> onHit,
            double accelX, double accelY, double accelZ, SoundEvent soundIn) {
        super(worldIn, shooter, targetIn, EnumFacing.Axis.Y);
        this.shootingEntity = shooter;
        this.setSize(0.3125F, 0.3125F);
        this.setLocationAndAngles(shooter.posX, shooter.posY + (shooter.height / 2.0F), shooter.posZ, shooter.rotationYaw, shooter.rotationPitch);
        this.setPosition(this.posX, this.posY, this.posZ);
        this.motionX = 0.0D;
        this.motionY = 0.0D;
        this.motionZ = 0.0D;
        accelX = accelX + this.rand.nextGaussian() * 0.4D;
        accelY = accelY + this.rand.nextGaussian() * 0.4D;
        accelZ = accelZ + this.rand.nextGaussian() * 0.4D;
        double d0 = (double)MathHelper.sqrt(accelX * accelX + accelY * accelY + accelZ * accelZ);
        this.accelerationX = accelX / d0 * 0.1D;
        this.accelerationY = accelY / d0 * 0.1D;
        this.accelerationZ = accelZ / d0 * 0.1D;
        this.onHit = onHit;
        this.sound = soundIn;
    }

    protected float getMotionFactor() {
       return 0.95F;
    }

    @Override
    public void onUpdate() {
        if (this.world.isRemote || (this.shootingEntity == null || !this.shootingEntity.isDead) && this.world.isBlockLoaded(new BlockPos(this))) {
           // don't use standard shulker onUpdate
//           ((Entity)this).onUpdate();

           ++this.ticksInAir;
           RayTraceResult raytraceresult = ProjectileHelper.forwardsRaycast(this, true, this.ticksInAir >= 25, this.shootingEntity);
           if (raytraceresult != null) {
              this.bulletHit(raytraceresult);
           }

           this.posX += this.motionX;
           this.posY += this.motionY;
           this.posZ += this.motionZ;
           ProjectileHelper.rotateTowardsMovement(this, 0.2F);
           float f = this.getMotionFactor();
           if (this.isInWater()) {
              for(int i = 0; i < 4; ++i) {
            	 // unused variable from Minecraft fireball code?
                 @SuppressWarnings("unused")
				 float f1 = 0.25F;
                 ((WorldServer)this.world).spawnParticle(EnumParticleTypes.WATER_BUBBLE, this.posX - this.motionX * 0.25D, this.posY - this.motionY * 0.25D, this.posZ - this.motionZ * 0.25D, this.motionX, this.motionY, this.motionZ);
              }

              f = 0.8F;
           }

           this.motionX += this.accelerationX;
           this.motionY += this.accelerationY;
           this.motionZ += this.accelerationZ;
           this.motionX *= (double)f;
           this.motionY *= (double)f;
           this.motionZ *= (double)f;
           this.setPosition(this.posX, this.posY, this.posZ);
        } else {
           this.setDead();
        }
     }

    @Override
    protected void bulletHit(RayTraceResult result) {
        EntityLivingBase owner = ((EntityShulkerBulletAccessor) this).getOwner();
        if (result.entityHit == null) {
            ((WorldServer)this.world).spawnParticle(EnumParticleTypes.EXPLOSION_LARGE, this.posX, this.posY, this.posZ, 2, 0.2D, 0.2D, 0.2D, 0.0D);
            this.playSound(sound, 1.0F, 1.0F);
        } else {
            boolean flag = result.entityHit.attackEntityFrom(DamageSource.causeIndirectDamage(this, owner).setProjectile(), 4.0F);
            if (flag) {
                this.applyEnchantments(owner, result.entityHit);
                if (result.entityHit instanceof EntityLivingBase) {
                    onHit.accept((EntityLivingBase) result.entityHit);
                }
            }
        }
        this.setDead();
    }
}
