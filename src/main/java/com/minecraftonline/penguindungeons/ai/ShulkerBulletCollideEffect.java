package com.minecraftonline.penguindungeons.ai;

import com.minecraftonline.penguindungeons.mixin.EntityShulkerBulletAccessor;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.projectile.EntityShulkerBullet;
import net.minecraft.init.SoundEvents;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;

import java.util.function.Consumer;

public class ShulkerBulletCollideEffect extends EntityShulkerBullet {

    private final Consumer<EntityLivingBase> onHit;
    private final SoundEvent sound;

    public ShulkerBulletCollideEffect(World worldIn) {
        this(worldIn, SoundEvents.ENTITY_SHULKER_BULLET_HIT);
    }

    public ShulkerBulletCollideEffect(World worldIn, SoundEvent soundIn) {
        super(worldIn);
        onHit = x -> {};
        sound = soundIn;
    }

    public ShulkerBulletCollideEffect(World worldIn, EntityLivingBase ownerIn, Entity targetIn, EnumFacing.Axis p_i46772_4_, Consumer<EntityLivingBase> onHit) {
        this(worldIn, ownerIn, targetIn, p_i46772_4_, onHit, SoundEvents.ENTITY_SHULKER_BULLET_HIT);
    }

    public ShulkerBulletCollideEffect(World worldIn, EntityLivingBase ownerIn, Entity targetIn, EnumFacing.Axis p_i46772_4_, Consumer<EntityLivingBase> onHit, SoundEvent soundIn) {
        super(worldIn, ownerIn, targetIn, p_i46772_4_);
        this.onHit = onHit;
        this.sound = soundIn;
    }

    @Override
    protected void bulletHit(RayTraceResult result) {
        EntityLivingBase owner = ((EntityShulkerBulletAccessor) this).getOwner();
        if (result.entityHit == null) {
            ((WorldServer)this.world).spawnParticle(EnumParticleTypes.EXPLOSION_LARGE, this.posX, this.posY, this.posZ, 2, 0.2D, 0.2D, 0.2D, 0.0D);
            this.playSound(sound, 1.0F, 1.0F);
        } else {
            boolean flag = result.entityHit.attackEntityFrom(DamageSource.causeIndirectDamage(this, owner).setProjectile(), 4.0F);
            if (flag) {
                this.applyEnchantments(owner, result.entityHit);
                if (result.entityHit instanceof EntityLivingBase) {
                    onHit.accept((EntityLivingBase) result.entityHit);
                }
            }
        }
        this.setDead();
    }
}
