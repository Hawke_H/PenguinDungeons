package com.minecraftonline.penguindungeons.customentity.shulker.standard;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.penguindungeons.spawnable.Spawnable;
import com.minecraftonline.penguindungeons.util.ResourceKey;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.DyeColor;
import org.spongepowered.api.data.type.DyeColors;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.World;

import java.util.Arrays;
import java.util.List;

public class HealthyShulker extends StandardShulkerType {

    public static final double ORIGINAL_SHULKER_HEALTH = 30;
    public static final double NEW_HEALTH = 30 * 2;

    public HealthyShulker(ResourceKey key) {
        super(key);
    }

    @Override
    public DyeColor getColor() {
        return DyeColors.RED;
    }

    @Override
    public Text getDisplayName() {
        return Text.of(TextColors.RED, "Healthy ", TextColors.WHITE, "Shulker");
    }

    @Override
    public Spawnable createEntity(World world, Vector3d blockPos) {
        Entity shulker = super.makeShulker(world, blockPos);
        shulker.offer(Keys.MAX_HEALTH, NEW_HEALTH);
        shulker.offer(Keys.HEALTH, NEW_HEALTH);
        return Spawnable.of(shulker);
    }

    @Override
    public List<Text> getEntityDescription() {
        return Arrays.asList(Text.of(TextColors.WHITE, "A shulker with ", TextColors.GREEN, NEW_HEALTH),
                Text.of(TextColors.WHITE, "instead of ", TextColors.GREEN, ORIGINAL_SHULKER_HEALTH, TextColors.WHITE, " health"));
    }
}
