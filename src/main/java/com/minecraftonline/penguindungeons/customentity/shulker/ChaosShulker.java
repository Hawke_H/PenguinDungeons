package com.minecraftonline.penguindungeons.customentity.shulker;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.penguindungeons.spawnable.Spawnable;
import com.minecraftonline.penguindungeons.util.ResourceKey;
import net.minecraft.entity.item.EntityEnderCrystal;
import org.spongepowered.api.data.type.DyeColor;
import org.spongepowered.api.data.type.DyeColors;
import org.spongepowered.api.entity.EnderCrystal;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityType;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.World;

import java.util.Arrays;
import java.util.List;

public class ChaosShulker extends ShulkerType {

    public ChaosShulker(ResourceKey key) {
        super(key);
    }

    @Override
    public EntityType getType() {
        return EntityTypes.SHULKER;
    }

    @Override
    public List<Text> getEntityDescription() {
        return Arrays.asList(
                Text.of(TextColors.WHITE, "A chaotic shulker"),
                Text.of(TextColors.WHITE, "that teleports around"),
                Text.of(TextColors.WHITE, "Leaving fire in its path."),
                Text.of(TextColors.WHITE, "(But only in the end)"));
    }

    @Override
    public DyeColor getColor() {
        return DyeColors.PURPLE;
    }

    @Override
    public Text getDisplayName() {
        return Text.of(TextColors.LIGHT_PURPLE, "Chaos", TextColors.WHITE, " Shulker");
    }

    @Override
    public Spawnable createEntity(World world, Vector3d blockPos) {
        Entity shulker = super.makeShulker(world, blockPos);

        EnderCrystal endCrystal = (EnderCrystal) world.createEntity(EntityTypes.ENDER_CRYSTAL, blockPos);
        ((EntityEnderCrystal) endCrystal).setShowBottom(false);

        return world1 -> {
            world1.spawnEntity(shulker);
            world1.spawnEntity(endCrystal);
            shulker.addPassenger(endCrystal);
            onLoad(shulker);
            return shulker;
        };
    }
}
