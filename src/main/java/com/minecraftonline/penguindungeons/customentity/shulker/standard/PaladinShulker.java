package com.minecraftonline.penguindungeons.customentity.shulker.standard;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.penguindungeons.ai.AIUtil;
import com.minecraftonline.penguindungeons.data.PenguinDungeonAITaskTypes;
import com.minecraftonline.penguindungeons.spawnable.Spawnable;
import com.minecraftonline.penguindungeons.util.ResourceKey;

import net.minecraft.entity.monster.EntityShulker;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.DyeColor;
import org.spongepowered.api.data.type.DyeColors;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.ai.Goal;
import org.spongepowered.api.entity.ai.GoalTypes;
import org.spongepowered.api.entity.ai.task.AITask;
import org.spongepowered.api.entity.ai.task.AbstractAITask;
import org.spongepowered.api.entity.living.Agent;
import org.spongepowered.api.entity.living.golem.Shulker;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.World;

import java.util.Arrays;
import java.util.List;

public class PaladinShulker extends StandardShulkerType {

    public PaladinShulker(ResourceKey key) {
        super(key);
    }

    @Override
    public DyeColor getColor() {
        return DyeColors.MAGENTA;
    }

    @Override
    public Text getDisplayName() {
        return Text.of(TextColors.DARK_PURPLE, "Paladin ", TextColors.WHITE, "Shulker");
    }

    @Override
    public Spawnable createEntity(World world, Vector3d blockPos) {
        Entity shulker = super.makeShulker(world, blockPos);
        onLoad(shulker);
        return Spawnable.of(shulker);
    }

    @Override
    public void onLoad(Entity entity) {
        super.onLoad(entity);
        if (!(entity instanceof Shulker)) {
            throw new IllegalArgumentException("Expected a shulker to be given to Shulker to load, but got: " + entity);
        }
        Shulker shulker = (Shulker) entity;
        shulker.offer(Keys.INVULNERABLE, true);

        AIUtil.swapShulkerAttackForCustom(shulker, AIUtil.ShulkerBulletCreator.forInvulnerable(), PenguinDungeonAITaskTypes.SHULKER_INVULNERABLE_BULLETS, "Paladin Bullet");

        Goal<Agent> goal = shulker.getGoal(GoalTypes.NORMAL).get();

        goal.addTask(3, new InvulnerableWhenClosed());
    }

    public static class InvulnerableWhenClosed extends AbstractAITask<Shulker> {

        public InvulnerableWhenClosed() {
            super(PenguinDungeonAITaskTypes.SHULKER_INVULNERABLE_WHEN_CLOSED);
        }

        @Override
        public void start() {
            getOwner().get().offer(Keys.INVULNERABLE, false);
        }

        @Override
        public boolean shouldUpdate() {
            if (!getOwner().isPresent()) {
                return false;
            }
            return ((EntityShulker) getOwner().get()).getPeekTick() > 0;
        }

        @Override
        public void update() {
            // do nothing here
        }

        @Override
        public boolean continueUpdating() {
            return shouldUpdate();
        }

        @Override
        public void reset() {
            getOwner().get().offer(Keys.INVULNERABLE, true);
        }

        @Override
        public boolean canRunConcurrentWith(AITask<Shulker> other) {
            return true;
        }

        @Override
        public boolean canBeInterrupted() {
            return false;
        }
    }

    @Override
    public List<Text> getEntityDescription() {
        return Arrays.asList(Text.of(TextColors.WHITE, "A shulker that is invulnerable"),
                Text.of(TextColors.WHITE, "when closed"));
    }
}
