package com.minecraftonline.penguindungeons.customentity.shulker.standard;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.penguindungeons.data.PenguinDungeonAITaskTypes;
import com.minecraftonline.penguindungeons.data.PenguinDungeonAITaskTypes.FindExplodableTarget;
import com.minecraftonline.penguindungeons.spawnable.Spawnable;
import com.minecraftonline.penguindungeons.util.ResourceKey;
import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.monster.EntityShulker;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.DyeColor;
import org.spongepowered.api.data.type.DyeColors;
import org.spongepowered.api.effect.sound.SoundTypes;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.ai.Goal;
import org.spongepowered.api.entity.ai.GoalTypes;
import org.spongepowered.api.entity.ai.task.AITask;
import org.spongepowered.api.entity.ai.task.AbstractAITask;
import org.spongepowered.api.entity.living.Agent;
import org.spongepowered.api.entity.living.golem.Shulker;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.Arrays;
import java.util.List;

public class ExplodingShulker extends StandardShulkerType {

    public ExplodingShulker(ResourceKey key) {
        super(key);
    }

    private static final DyeColor COLOUR = DyeColors.LIME;
    private static final DyeColor COLOUR_EXPLODING = DyeColors.WHITE;

    @Override
    public List<Text> getEntityDescription() {
        return Arrays.asList(Text.of(TextColors.WHITE, "A shulker that ", TextColors.GREEN, "explodes"),
                Text.of(TextColors.WHITE, "if you get too close"));
    }

    @Override
    public DyeColor getColor() {
        return COLOUR;
    }

    @Override
    public Text getDisplayName() {
        return Text.of(TextColors.GREEN, "Exploding", TextColors.WHITE, " Shulker");
    }

    @Override
    public Spawnable createEntity(World world, Vector3d blockPos) {
        Entity shulker = super.makeShulker(world, blockPos);
        onLoad(shulker);
        return Spawnable.of(shulker);
    }

    @Override
    public void onLoad(Entity entity) {
        super.onLoad(entity);
        if (!(entity instanceof Shulker)) {
            throw new IllegalArgumentException("Expected a shulker to be given to Shulker to load, but got: " + entity);
        }
        Shulker shulker = (Shulker) entity;
        Goal<Agent> targetGoals = shulker.getGoal(GoalTypes.TARGET).get();
        targetGoals.clear();

        Goal<Agent> normalGoals = shulker.getGoal(GoalTypes.NORMAL).get();
        // Remove regular attack.
        normalGoals.getTasks().stream().filter(aiTask -> aiTask instanceof EntityShulker.AIAttack || aiTask instanceof EntityShulker.AIPeek)
                .forEach(task -> normalGoals.removeTask((AITask<? extends Agent>) task));

        normalGoals.addTask(0, new StartExploding());
        targetGoals.addTask(1, new FindExplodableTarget((EntityCreature) shulker));
    }

    public static class StartExploding extends AbstractAITask<Shulker> {

        public static final int STARTING_FUSE_LENGTH = 30;
        private static final int SWAP_COLOR_INTERVAL = 5;
        private int fuseLength;

        public StartExploding() {
            super(PenguinDungeonAITaskTypes.SHULKER_EXPLODE);
        }

        @Override
        public void start() {
            fuseLength = STARTING_FUSE_LENGTH;
            ((EntityShulker) getOwner().get()).updateArmorModifier(30);
        }

        @Override
        public boolean shouldUpdate() {
            if (!getOwner().isPresent() || !getOwner().get().getTarget().isPresent()) {
                return false;
            }
            Shulker shulker = getOwner().get();
            if (shulker.isRemoved()) {
                return false;
            }
            if (!shulker.getTarget().isPresent())
            {
                return false;
            }
            Entity target = shulker.getTarget().get();
            if (target.isRemoved()) {
                return false;
            }
            if (!shulker.getLocation().getExtent().equals(target.getLocation().getExtent())) {
                return false;
            }
            return !(shulker.getLocation().getPosition().distanceSquared(target.getLocation().getPosition()) > 7 * 7);
        }

        @Override
        public void update() {
            Shulker shulker = getOwner().get();
            if (fuseLength == STARTING_FUSE_LENGTH) {
                // HISSS
                shulker.getLocation().getExtent().playSound(SoundTypes.ENTITY_CREEPER_PRIMED, shulker.getLocation().getPosition(), 1.0D);
            }
            if (fuseLength % SWAP_COLOR_INTERVAL == 0) {
                shulker.offer(Keys.DYE_COLOR, ExplodingShulker.COLOUR_EXPLODING);
            }
            else if (fuseLength % SWAP_COLOR_INTERVAL == SWAP_COLOR_INTERVAL / 2) {
                shulker.offer(Keys.DYE_COLOR, ExplodingShulker.COLOUR);
            }
            fuseLength--;
            if (fuseLength <= 0) {
                // its gonna blow chief.
                Location<World> location = shulker.getLocation();
                ((EntityLivingBase) shulker).world.createExplosion((EntityLivingBase) shulker, location.getX(), location.getY(), location.getZ(), 3.5F, false);

                shulker.remove();
            }
        }

        @Override
        public boolean continueUpdating() {
            return shouldUpdate();
        }

        @Override
        public void reset() {
            this.fuseLength = STARTING_FUSE_LENGTH;
            ((EntityShulker) getOwner().get()).updateArmorModifier(0);
            getOwner().get().offer(Keys.DYE_COLOR, ExplodingShulker.COLOUR);
        }

        @Override
        public boolean canRunConcurrentWith(AITask<Shulker> other) {
            return false;
        }

        @Override
        public boolean canBeInterrupted() {
            return false;
        }
    }
}
