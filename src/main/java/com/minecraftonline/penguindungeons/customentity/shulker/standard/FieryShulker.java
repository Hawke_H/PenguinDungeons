package com.minecraftonline.penguindungeons.customentity.shulker.standard;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.penguindungeons.ai.AIUtil;
import com.minecraftonline.penguindungeons.ai.ShulkerBulletCollideEffect;
import com.minecraftonline.penguindungeons.data.PenguinDungeonAITaskTypes;
import com.minecraftonline.penguindungeons.spawnable.Spawnable;
import com.minecraftonline.penguindungeons.util.ResourceKey;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.EnumFacing;
import net.minecraft.world.World;
import org.spongepowered.api.data.type.DyeColor;
import org.spongepowered.api.data.type.DyeColors;
import org.spongepowered.api.entity.living.golem.Shulker;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import java.util.Collections;
import java.util.List;

public class FieryShulker extends StandardShulkerType {

    public FieryShulker(ResourceKey key) {
        super(key);
    }

    @Override
    public List<Text> getEntityDescription() {
        return Collections.singletonList(Text.of(TextColors.WHITE, "A shulker that lights targets on ", TextColors.GOLD, "fire"));
    }

    @Override
    public DyeColor getColor() {
        return DyeColors.ORANGE;
    }

    @Override
    public Text getDisplayName() {
        return Text.of(TextColors.GOLD, "Fiery", TextColors.WHITE, " Shulker");
    }

    @Override
    public Spawnable createEntity(org.spongepowered.api.world.World world, Vector3d blockPos) {
        org.spongepowered.api.entity.Entity shulker = super.makeShulker(world, blockPos);
        onLoad(shulker);
        return Spawnable.of(shulker);
    }

    @Override
    public void onLoad(org.spongepowered.api.entity.Entity entity) {
        super.onLoad(entity);
        if (!(entity instanceof Shulker)) {
            return;
        }
        Shulker shulker = (Shulker) entity;
        AIUtil.swapShulkerAttackForCustom(shulker, FieryShulkerBullet::new, PenguinDungeonAITaskTypes.SHULKER_FIERY_ATTACK, "Fiery Bullet");
    }

    public static class FieryShulkerBullet extends ShulkerBulletCollideEffect {

        public FieryShulkerBullet(World worldIn) {
            super(worldIn);
        }

        public FieryShulkerBullet(World worldIn, EntityLivingBase ownerIn, Entity targetIn, EnumFacing.Axis p_i46772_4_) {
            super(worldIn, ownerIn, targetIn, p_i46772_4_, e -> e.setFire(5));
        }

        @Override
        public boolean isBurning() {
            return true;
        }
    }
}
