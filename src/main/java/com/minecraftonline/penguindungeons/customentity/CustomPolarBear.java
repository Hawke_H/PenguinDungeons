package com.minecraftonline.penguindungeons.customentity;

import java.util.Collections;
import java.util.List;

import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityType;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.ai.Goal;
import org.spongepowered.api.entity.ai.GoalTypes;
import org.spongepowered.api.entity.ai.task.AITask;
import org.spongepowered.api.entity.living.Agent;
import org.spongepowered.api.entity.living.animal.PolarBear;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.World;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.penguindungeons.data.customentity.PDEntityTypeData;
import com.minecraftonline.penguindungeons.data.loot.OwnerLootData;
import com.minecraftonline.penguindungeons.data.spawning.PDSpawnData;
import com.minecraftonline.penguindungeons.spawnable.Spawnable;
import com.minecraftonline.penguindungeons.util.ResourceKey;

import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.ai.EntityAINearestAttackableTarget;
import net.minecraft.entity.player.EntityPlayer;

public class CustomPolarBear extends AbstractCustomEntity {

    public CustomPolarBear(ResourceKey key) {
        super(key);
    }

    @Override
    public EntityType getType() {
        return EntityTypes.POLAR_BEAR;
    }

    @Override
    public ItemStack getSpawnEgg() {
        return ItemStack.builder().itemType(ItemTypes.SPAWN_EGG)
                .add(Keys.SPAWNABLE_ENTITY_TYPE, getType())
                .add(Keys.DISPLAY_NAME, Text.of("Polar Bear"))
                .add(Keys.ITEM_LORE, getDescription())
                .itemData(new PDEntityTypeData(getId()))
                .build();
    }

    @Override
    public Spawnable createEntity(World world, Vector3d pos) {
        return Spawnable.of(makePolarBear(world, pos), this::onLoad);
    }

    protected PolarBear makePolarBear(World world, Vector3d blockPos) {
        PolarBear polarBear = (PolarBear) world.createEntity(getType(), blockPos);
        polarBear.offer(new PDEntityTypeData(getId()));
        polarBear.offer(new OwnerLootData(ownerLoot()));
        polarBear.offer(new PDSpawnData(true));
        applyEquipment(polarBear);
        applyLootTable(polarBear);
        return polarBear;
    }

    @Override
    public void onLoad(Entity entity)
    {
        if (!(entity instanceof PolarBear)) {
            throw new IllegalArgumentException("Expected a PolarBear to be given to CustomPolarBear to load, but got: " + entity);
        }
        PolarBear polarBear = (PolarBear) entity;
        Goal<Agent> targetGoals = polarBear.getGoal(GoalTypes.TARGET).get();
        targetGoals.clear();
        targetGoals.addTask(2, (AITask<? extends Agent>) new EntityAINearestAttackableTarget((EntityCreature) polarBear, EntityPlayer.class, true));
        polarBear.offer(Keys.ANGRY, true);
    }

    public boolean ownerLoot()
    {
        return true;
    }

    @Override
    public List<Text> getEntityDescription() {
        return Collections.singletonList(Text.of(TextColors.WHITE, "A polar bear that attacks players by default"));
    }

}
