package com.minecraftonline.penguindungeons.customentity.zombie;

import java.util.Collections;
import java.util.List;

import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.EntityType;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.living.monster.ZombiePigman;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.World;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.penguindungeons.data.customentity.PDEntityTypeData;
import com.minecraftonline.penguindungeons.data.loot.OwnerLootData;
import com.minecraftonline.penguindungeons.data.spawning.PDSpawnData;
import com.minecraftonline.penguindungeons.spawnable.Spawnable;
import com.minecraftonline.penguindungeons.util.ResourceKey;

import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.util.math.BlockPos;

public class AngryZombiePigman extends ZombieType<ZombiePigman> {

    public AngryZombiePigman(ResourceKey key) {
        super(key);
    }

    public Text getDisplayName() {
        return Text.of(TextColors.RESET, "Zombie Pigman");
    }

    @Override
    public EntityType getType() {
        return EntityTypes.PIG_ZOMBIE;
    }

    @Override
    public Spawnable createEntity(World world, Vector3d pos) {
        return Spawnable.of(makeZombie(world, pos), this::onLoad);
    }

    @Override
    public ZombiePigman makeZombie(World world, Vector3d blockPos) {
        ZombiePigman zombie_pigman = (ZombiePigman) world.createEntity(getType(), blockPos);

        EntityLiving living = (EntityLiving) zombie_pigman;
        living.onInitialSpawn(((net.minecraft.world.World) world).getDifficultyForLocation(new BlockPos(living)), (IEntityLivingData)null);

        zombie_pigman.offer(new PDEntityTypeData(getId()));
        zombie_pigman.offer(new OwnerLootData(ownerLoot()));
        zombie_pigman.offer(new PDSpawnData(true));
        zombie_pigman.offer(Keys.ANGRY, true);

        // entity should despawn
        zombie_pigman.offer(Keys.PERSISTS, false);

        applyLootTable(zombie_pigman);

        // note equipment and custom name are not set

        return zombie_pigman;
    }

    @Override
    public List<Text> getEntityDescription() {
        return Collections.singletonList(Text.of(TextColors.WHITE, "An aggressive zombie pigman that does not drop items"));
    }

    @Override
    public boolean ownerLoot()
    {
        return false;
    }
}
