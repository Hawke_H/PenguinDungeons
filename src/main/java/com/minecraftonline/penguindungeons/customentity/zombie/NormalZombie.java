package com.minecraftonline.penguindungeons.customentity.zombie;

import java.util.Collections;
import java.util.List;

import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.living.monster.Zombie;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.World;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.penguindungeons.data.customentity.PDEntityTypeData;
import com.minecraftonline.penguindungeons.data.loot.OwnerLootData;
import com.minecraftonline.penguindungeons.data.spawning.PDSpawnData;
import com.minecraftonline.penguindungeons.util.ResourceKey;

import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.util.math.BlockPos;

public class NormalZombie extends ZombieType<Zombie> {

    public NormalZombie(ResourceKey key) {
        super(key);
    }

    public Text getDisplayName() {
        return Text.of(TextColors.RESET, "Zombie");
    }

    @Override
    public Zombie makeZombie(World world, Vector3d blockPos) {
        Zombie zombie = (Zombie) world.createEntity(getType(), blockPos);

        EntityLiving living = (EntityLiving) zombie;
        living.onInitialSpawn(((net.minecraft.world.World) world).getDifficultyForLocation(new BlockPos(living)), (IEntityLivingData)null);

        zombie.offer(new PDEntityTypeData(getId()));
        zombie.offer(new OwnerLootData(ownerLoot()));
        zombie.offer(new PDSpawnData(true));

        // entity should despawn
        zombie.offer(Keys.PERSISTS, false);

        applyLootTable(zombie);

        // note equipment and custom name are not set

        return zombie;
    }

    @Override
    public List<Text> getEntityDescription() {
        return Collections.singletonList(Text.of(TextColors.WHITE, "A normal zombie"));
    }

    @Override
    public boolean ownerLoot()
    {
        return false;
    }
}
