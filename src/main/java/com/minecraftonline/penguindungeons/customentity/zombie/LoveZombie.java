package com.minecraftonline.penguindungeons.customentity.zombie;

import java.util.Collections;
import java.util.List;

import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.living.monster.Zombie;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.World;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.penguindungeons.util.ResourceKey;

public class LoveZombie extends ZombieType<Zombie> {

    private static final double MOB_HEALTH = 25;

    public LoveZombie(ResourceKey key) {
        super(key);
    }

    @Override
    public Text getDisplayName() {
        return Text.of(TextColors.LIGHT_PURPLE, "Enthralled");
    }

    @Override
    public List<Text> getEntityDescription() {
        return Collections.singletonList(Text.of(TextColors.WHITE, "Spawns a zombie that loves to love"));
    }

    @Override
    public Zombie makeZombie(World world, Vector3d blockPos) {
        Zombie zombie = super.makeZombie(world, blockPos);
        zombie.offer(Keys.MAX_HEALTH, MOB_HEALTH);
        zombie.offer(Keys.HEALTH, MOB_HEALTH);
        return zombie;
    }

    @Override
    public boolean targetsVillagers() {
        return true;
    }

}
