package com.minecraftonline.penguindungeons.customentity;

import com.minecraftonline.penguindungeons.PenguinDungeons;
import com.minecraftonline.penguindungeons.equipment.EquipmentLoadout;
import com.minecraftonline.penguindungeons.util.ResourceKey;
import net.minecraft.entity.EntityLiving;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.storage.loot.LootTableList;

import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.living.Living;
import org.spongepowered.api.item.enchantment.Enchantment;
import org.spongepowered.api.item.enchantment.EnchantmentTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.util.Tuple;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public abstract class AbstractCustomEntity implements CustomEntityType {
    private static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("#.##");
    private final ResourceKey key;
    private EquipmentLoadout cachedLoadout = null;
    private List<Text> cachedDescription = null;

    public AbstractCustomEntity(ResourceKey key) {
        this.key = key;
    }

    @Override
    public final ResourceKey getId() {
        return this.key;
    }

    public EquipmentLoadout.Builder makeEquipmentLoadout() {
        return EquipmentLoadout.builder();
    }

    public List<Text> getDescription() {
        if (this.cachedDescription == null) {
            this.cachedDescription = makeDescription();
        }
        return this.cachedDescription;
    }

    private List<Text> makeDescription() {
        this.cachedDescription = new ArrayList<>();
        this.cachedDescription.addAll(getEntityDescription());
        List<Tuple<ItemStack, Double>> drops = getEquipmentLoadout().getDropChances();
        if (!drops.isEmpty()) {
            List<Text> dropsDescription = new ArrayList<>();
            dropsDescription.add(Text.of(TextColors.GOLD, "Drops:"));
            boolean hasDrops = false;
            for (Tuple<ItemStack, Double> drop : drops) {
                if (drop.getSecond() == 0) {
                    continue;
                }
                Optional<List<Enchantment>> enchants = drop.getFirst().get(Keys.ITEM_ENCHANTMENTS);
                if (enchants.isPresent() && enchants.get().contains(Enchantment.of(EnchantmentTypes.VANISHING_CURSE, 1))) {
                    // items with curse of vanishing won't drop
                    continue;
                }
                hasDrops = true;
                dropsDescription.add(Text.of(
                        Text.of(TextColors.BLUE, drop.getFirst().getTranslation()),
                        Text.of(TextColors.GRAY, " x" + drop.getFirst().getQuantity()),
                        Text.of(TextColors.GRAY, " (" + DECIMAL_FORMAT.format(drop.getSecond() * 100.0) + "%)")));
            }
            if (hasDrops) {
                this.cachedDescription.addAll(dropsDescription);
            }
        }
        return this.cachedDescription;
    }

    public abstract List<Text> getEntityDescription();

    private EquipmentLoadout getEquipmentLoadout() {
        if (this.cachedLoadout == null) {
            this.cachedLoadout = makeEquipmentLoadout().build();
        }
        return this.cachedLoadout;
    }

    /**
     * Applies the equipment to the given entity.
     * This must be called by sub-classes if they want the
     * equipment loadout / drops applied.
     * @param entity Living entity to apply loadout to.
     */
    protected void applyEquipment(Living entity) {
        this.getEquipmentLoadout().apply(entity);
    }

    protected void applyLootTable(Living entity) {
        EntityLiving living = (EntityLiving) entity;
        NBTTagCompound nbt = new NBTTagCompound();
        living.writeToNBT(nbt);
        nbt.setString("DeathLootTable", PenguinDungeons.getInstance().getLootTableFor(this).toString());
        living.readFromNBT(nbt);
    }

    protected void applyEmptyLootTable(Living entity) {
        EntityLiving living = (EntityLiving) entity;
        NBTTagCompound nbt = new NBTTagCompound();
        living.writeToNBT(nbt);
        nbt.setString("DeathLootTable", LootTableList.EMPTY.toString());
        living.readFromNBT(nbt);
    }
}
