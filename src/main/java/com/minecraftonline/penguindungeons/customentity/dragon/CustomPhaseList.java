package com.minecraftonline.penguindungeons.customentity.dragon;

import java.lang.reflect.Constructor;
import java.util.Arrays;
import net.minecraft.entity.boss.dragon.phase.IPhase;

public class CustomPhaseList<T extends IPhase> {
    private static CustomPhaseList<?>[] phases = new CustomPhaseList[0];
    public static final CustomPhaseList<CustomPhaseHoldingPattern> HOLDING_PATTERN = create(CustomPhaseHoldingPattern.class, "HoldingPattern");
    public static final CustomPhaseList<CustomPhaseStrafePlayer> STRAFE_PLAYER = create(CustomPhaseStrafePlayer.class, "StrafePlayer");
    public static final CustomPhaseList<CustomPhaseLandingApproach> LANDING_APPROACH = create(CustomPhaseLandingApproach.class, "LandingApproach");
    public static final CustomPhaseList<CustomPhaseLanding> LANDING = create(CustomPhaseLanding.class, "Landing");
    public static final CustomPhaseList<CustomPhaseTakeoff> TAKEOFF = create(CustomPhaseTakeoff.class, "Takeoff");
    public static final CustomPhaseList<CustomPhaseSittingFlaming> SITTING_FLAMING = create(CustomPhaseSittingFlaming.class, "SittingFlaming");
    public static final CustomPhaseList<CustomPhaseSittingScanning> SITTING_SCANNING = create(CustomPhaseSittingScanning.class, "SittingScanning");
    public static final CustomPhaseList<CustomPhaseSittingAttacking> SITTING_ATTACKING = create(CustomPhaseSittingAttacking.class, "SittingAttacking");
    public static final CustomPhaseList<CustomPhaseChargingPlayer> CHARGING_PLAYER = create(CustomPhaseChargingPlayer.class, "ChargingPlayer");
    public static final CustomPhaseList<CustomPhaseDying> DYING = create(CustomPhaseDying.class, "Dying");
    public static final CustomPhaseList<CustomPhaseHover> HOVER = create(CustomPhaseHover.class, "Hover");
    private final Class<? extends IPhase> clazz;
    private final int id;
    private final String name;

    public CustomPhaseList(int idIn, Class<? extends IPhase> clazzIn, String nameIn) {
        this.id = idIn;
        this.clazz = clazzIn;
        this.name = nameIn;
    }

    public IPhase createPhase(CustomDragon dragon) {
        try {
            Constructor<? extends IPhase> constructor = this.getConstructor();
            return constructor.newInstance(dragon);
        } catch (Exception exception) {
            throw new Error(exception);
        }
    }

    protected Constructor<? extends IPhase> getConstructor() throws NoSuchMethodException {
        return this.clazz.getConstructor(CustomDragon.class);
    }

    public int getId() {
        return this.id;
    }

    public String toString() {
        return this.name + " (#" + this.id + ")";
    }

    /**
     * Gets a phase by its ID. If the phase is out of bounds (negative or beyond the end of the phase array), returns
     * {@link #HOLDING_PATTERN}.
     */
    public static CustomPhaseList<?> getById(int idIn) {
        return idIn >= 0 && idIn < phases.length ? phases[idIn] : HOLDING_PATTERN;
    }

    public static int getTotalPhases() {
        return phases.length;
    }

    private static <T extends IPhase> CustomPhaseList<T> create(Class<T> phaseIn, String nameIn) {
        CustomPhaseList<T> CustomPhaseList = new CustomPhaseList<T>(phases.length, phaseIn, nameIn);
        phases = (CustomPhaseList[])Arrays.copyOf(phases, phases.length + 1);
        phases[CustomPhaseList.getId()] = CustomPhaseList;
        return CustomPhaseList;
    }
}
