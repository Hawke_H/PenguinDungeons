package com.minecraftonline.penguindungeons.customentity.dragon;

import org.spongepowered.api.entity.living.complex.EnderDragon;
import org.spongepowered.api.world.World;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.penguindungeons.customentity.CustomEntityType;

import net.minecraft.world.end.DragonFightManager;

public interface DragonType extends CustomEntityType {

    EnderDragon makeDragon(World world, Vector3d blockPos, DragonFightManager fightManager);
    
}
