package com.minecraftonline.penguindungeons.customentity.bee;

import java.util.Collections;
import java.util.List;

import com.flowpowered.math.vector.Vector3d;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.living.monster.Husk;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import com.minecraftonline.penguindungeons.util.ResourceKey;
import org.spongepowered.api.world.World;

public class MinionBee extends AngryBee {

    private static final double NEW_HEALTH = 11;

    public MinionBee(ResourceKey key) {
        super(key);
    }

    @Override
    public Text getEntityName() {
        return Text.of(TextColors.RESET, "Minion Bee");
    }

    @Override
    public List<Text> getEntityDescription() {
        return Collections.singletonList(Text.of(TextColors.WHITE, "A Young and weak bee summoned by the Queen bee"));
    }

    @Override
    protected Husk makeBee(World world, Vector3d blockPos) {
        Husk bee = super.makeBee(world, blockPos);

        //MinionBees have less health than a normal bee
        bee.offer(Keys.MAX_HEALTH, NEW_HEALTH);
        bee.offer(Keys.HEALTH, NEW_HEALTH);

        return bee;

    }
}
