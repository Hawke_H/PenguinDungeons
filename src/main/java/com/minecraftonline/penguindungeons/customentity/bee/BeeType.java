package com.minecraftonline.penguindungeons.customentity.bee;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.spongepowered.api.data.DataContainer;
import org.spongepowered.api.data.DataQuery;
import org.spongepowered.api.data.DataView;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.SkullTypes;
import org.spongepowered.api.effect.potion.PotionEffect;
import org.spongepowered.api.effect.potion.PotionEffectTypes;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityType;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.ai.Goal;
import org.spongepowered.api.entity.ai.GoalTypes;
import org.spongepowered.api.entity.ai.task.AITask;
import org.spongepowered.api.entity.living.Agent;
import org.spongepowered.api.entity.living.Creature;
import org.spongepowered.api.entity.living.monster.Zombie;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.enchantment.Enchantment;
import org.spongepowered.api.item.enchantment.EnchantmentTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.World;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.penguindungeons.customentity.AbstractCustomEntity;
import com.minecraftonline.penguindungeons.customentity.EntityAIHurtByNonPD;
import com.minecraftonline.penguindungeons.data.customentity.PDEntityTypeData;
import com.minecraftonline.penguindungeons.data.loot.OwnerLootData;
import com.minecraftonline.penguindungeons.data.spawning.PDSpawnData;
import com.minecraftonline.penguindungeons.equipment.EquipmentLoadout;
import com.minecraftonline.penguindungeons.spawnable.Spawnable;
import com.minecraftonline.penguindungeons.util.ResourceKey;

import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.ai.EntityAIHurtByTarget;
import net.minecraft.entity.ai.EntityAIMoveThroughVillage;
import net.minecraft.entity.ai.EntityAINearestAttackableTarget;
import net.minecraft.entity.monster.EntityZombie;
import net.minecraft.entity.player.EntityPlayer;

public abstract class BeeType<T extends Creature> extends AbstractCustomEntity {

    protected static final double ITEM_DROP_CHANCE = 0.0;

    private static final DataQuery SKULL_INFO_PATH = DataQuery.of("UnsafeData", "SkullOwner");
    private static final DataQuery TEXTURES_PATH = DataQuery.of("Properties", "textures");

    public BeeType(ResourceKey key) {
        super(key);
    }

    public abstract Text getDisplayName(); // "Angry Bee"

    public abstract Text getEntityName(); // "Bee"

    @Override
    public EntityType getType() {
        return EntityTypes.HUSK;
    }

    protected boolean isSmall() {
        return true;
    }

    protected abstract String getTexture();

    protected abstract String getUUID();

    public ItemStack getHead() {
        // prevent this head from being dropped, as one is already in the loot table
        List<Enchantment> enchants = new ArrayList<Enchantment>();
        enchants.add(Enchantment.of(EnchantmentTypes.VANISHING_CURSE, 1));

        ItemStack skull = ItemStack.builder()
                .itemType(ItemTypes.SKULL)
                .add(Keys.DISPLAY_NAME, getDisplayName())
                .add(Keys.ITEM_ENCHANTMENTS, enchants)
                .add(Keys.SKULL_TYPE, SkullTypes.PLAYER)
                //.add(Keys.SKIN_UNIQUE_ID, UUID.fromString(getUUID()))
                .build();

        // add skull texture in nbt
        DataView itemView = skull.toContainer();

        DataView skullInfo = itemView.getView(SKULL_INFO_PATH).orElse(DataContainer.createNew());
        List<DataView> textures = skullInfo.getViewList(TEXTURES_PATH).orElse(new ArrayList<DataView>());

        skullInfo.set(DataQuery.of("Id"), getUUID());
        skullInfo.set(DataQuery.of("Name"), getDisplayName().toPlain());

        DataView texture = DataContainer.createNew();
        texture.set(DataQuery.of("Value"), getTexture());
        textures.add(texture);
        skullInfo.set(TEXTURES_PATH, textures);

        itemView.set(SKULL_INFO_PATH, skullInfo);

        skull.setRawData(itemView);
        return skull;
    }

    @Override
    public ItemStack getSpawnEgg() {
        return ItemStack.builder().itemType(ItemTypes.SPAWN_EGG)
                .add(Keys.SPAWNABLE_ENTITY_TYPE, getType())
                .add(Keys.DISPLAY_NAME, getDisplayName())
                .add(Keys.ITEM_LORE, getDescription())
                .itemData(new PDEntityTypeData(getId()))
                .build();
    }

    @Override
    public Spawnable createEntity(World world, Vector3d pos) {
        return Spawnable.of(makeBee(world, pos), this::onLoad);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onLoad(Entity entity)
    {
        if (!(entity instanceof Creature)) {
            throw new IllegalArgumentException("Expected a Creature to be given to BeeType to load, but got: " + entity);
        }
        Creature bee = (Creature) entity;

        if (bee instanceof Zombie)
        {
            Zombie zombie = (Zombie) bee;
            // remove task for moving through village
            Goal<Agent> normalGoals = zombie.getGoal(GoalTypes.NORMAL).get();
            normalGoals.getTasks().stream().filter(aiTask -> aiTask instanceof EntityAIMoveThroughVillage)
            .forEach(task -> normalGoals.removeTask((AITask<? extends Agent>) task));

            // remove task for attacking villager/irongolem/player
            Goal<Agent> targetGoals = zombie.getGoal(GoalTypes.TARGET).get();
            targetGoals.getTasks().stream().filter(aiTask -> aiTask instanceof EntityAINearestAttackableTarget
                                                          || aiTask instanceof EntityAIHurtByTarget)
            .forEach(task -> targetGoals.removeTask((AITask<? extends Agent>) task));

            // add back attacking players
            targetGoals.addTask(1, new EntityAIHurtByNonPD.TargetNonPDAttackers((Creature) entity, true, true));
            targetGoals.addTask(2, (AITask<? extends Agent>) new EntityAINearestAttackableTarget<EntityPlayer>((EntityCreature) entity, EntityPlayer.class, true));
        }
    }

    protected T makeBee(World world, Vector3d blockPos) {
        @SuppressWarnings("unchecked")
        T bee = (T) world.createEntity(getType(), blockPos);
        bee.offer(Keys.DISPLAY_NAME, getEntityName());
        bee.offer(new PDEntityTypeData(getId()));
        bee.offer(new OwnerLootData(ownerLoot()));
        bee.offer(new PDSpawnData(true));
        // bee mobs are invisible, their skull is what shows
        bee.offer(Keys.POTION_EFFECTS, Collections.singletonList(PotionEffect.of(PotionEffectTypes.INVISIBILITY, 0, Integer.MAX_VALUE)));

        // entity should despawn
        bee.offer(Keys.PERSISTS, false);

        // entity should not make normal mob sounds
        bee.offer(Keys.IS_SILENT, true);

        if (bee instanceof EntityZombie)
        {
            EntityZombie mcZombie = (EntityZombie) bee;
            mcZombie.setChild(isSmall());
        }

        applyEquipment(bee);
        applyLootTable(bee);
        return bee;
    }

    public boolean ownerLoot()
    {
        return true;
    }

    @Override
    public EquipmentLoadout.Builder makeEquipmentLoadout() {
        return super.makeEquipmentLoadout()
                .head(getHead(), ITEM_DROP_CHANCE);
    }

}
