package com.minecraftonline.penguindungeons.customentity;

import java.util.Optional;

import org.spongepowered.api.entity.living.Creature;

import com.minecraftonline.penguindungeons.ai.DelegatingToMCAI;
import com.minecraftonline.penguindungeons.data.PenguinDungeonAITaskTypes;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.ai.EntityAITarget;

class CopyRiderTarget extends EntityAITarget {

    private final EntityLiving entityHost;

    public CopyRiderTarget(EntityCreature creature) {
       super(creature, false);
       this.entityHost = creature;
    }

    /**
     * Returns whether the EntityAIBase should begin execution.
     */
    public boolean shouldExecute() {
        Optional<EntityCreature> optionlController = this.getController();
        if (!optionlController.isPresent()) return false;
        EntityCreature controller = optionlController.get();
        return controller != null && controller.getAttackTarget() != null && this.isSuitableTarget(controller.getAttackTarget(), false);
    }

    private Optional<EntityCreature> getController()
    {
        Entity entity = this.entityHost.getControllingPassenger();
        if (!(entity instanceof EntityCreature)) return Optional.empty();
        return Optional.of((EntityCreature) entity);
    }

    /**
     * Execute a one shot task or start executing a continuous task
     */
    public void startExecuting() {
        this.entityHost.setAttackTarget(this.getController().get().getAttackTarget());
        super.startExecuting();
    }

    public static class CopyRiderTask extends DelegatingToMCAI<Creature> {
        public CopyRiderTask(Creature creature) {
            super(PenguinDungeonAITaskTypes.COPY_RIDER_TARGET, new CopyRiderTarget((EntityCreature) creature));
        }
    }
}
