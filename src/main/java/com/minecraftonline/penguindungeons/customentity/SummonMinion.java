package com.minecraftonline.penguindungeons.customentity;

import java.util.Optional;
import java.util.OptionalInt;

import com.flowpowered.math.vector.Vector3d;
import net.minecraft.entity.Entity;
import net.minecraft.entity.monster.EntityGhast;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.MathHelper;
import org.spongepowered.api.entity.ai.task.AITask;
import org.spongepowered.api.entity.living.Agent;
import org.spongepowered.api.entity.living.Living;

import com.minecraftonline.penguindungeons.ai.DelegatingToMCAI;
import com.minecraftonline.penguindungeons.data.PenguinDungeonAITaskTypes;

import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.ai.EntityAIBase;
import org.spongepowered.api.world.World;

public class SummonMinion<T extends EntityLiving> extends EntityAIBase {

    private final EntityLiving entityHost;
    private final SoundEvent summonSound;
    private final AbstractCustomEntity summonedEntity;
    /**
     * A decrementing tick that summons a minion when its value reaches 0.
     * It will then be returned to maxCooldownTime after completing.
     */
    private int summonCooldown;
    private final int maxCooldown;
    private final int minCooldown;

    private Optional<Float> healthValue = Optional.empty();

    /**
     * Returns whether the EntityAIBase should begin execution.
     */
    public boolean shouldExecute() {
        EntityLivingBase entitylivingbase = this.entityHost.getAttackTarget();
        return entitylivingbase != null && entitylivingbase.isEntityAlive();
    }

    /**
     * Returns whether an in-progress EntityAIBase should continue executing
     */
    public boolean shouldContinueExecuting() {
        return this.shouldExecute();
    }

    public SummonMinion<T> setTriggerHealth(float triggerHealth){
        this.healthValue = Optional.of(triggerHealth);
        return this;
    }

    public SummonMinion(Living host, AbstractCustomEntity minionEntity, SoundEvent sound, int minCooldownTime, int maxCooldownTime) {
        this.entityHost = (EntityLiving)host;
        this.summonedEntity = minionEntity;
        this.summonSound = sound;
        this.summonCooldown = -1;
        this.minCooldown = minCooldownTime;
        this.maxCooldown = maxCooldownTime - this.minCooldown;
        if (this.entityHost instanceof EntityGhast) {
            // continue ghast looking/moving tasks
            this.setMutexBits(4);
        } else {
            this.setMutexBits(3);
        }
    }

    //summons a minion entity
    private void SummonEntity(AbstractCustomEntity summonedEntity, EntityLiving entityHost, SoundEvent summonSound){
        ((Entity)summonedEntity.createEntity((World) entityHost.world, new Vector3d(entityHost.posX,
                        entityHost.posY, entityHost.posZ))
                .spawn((World) entityHost.world)).playSound(summonSound, 2.5F, 1.0F);
    }

    //creates a random cooldown based on inputted maximum and minimum values
    private int CooldownCreator(int maxCooldown, int minCooldown){
        return MathHelper.ceil((float)(maxCooldown) * Math.random()) + minCooldown;
    }

    public void updateTask() {

        if (this.summonCooldown == 0 && this.healthValue.isPresent() && this.entityHost.getHealth() <= this.healthValue.get()) {

            SummonEntity(this.summonedEntity, this.entityHost, this.summonSound);

            this.summonCooldown = CooldownCreator(this.maxCooldown, this.minCooldown);

        } else if (this.summonCooldown == 0 && !this.healthValue.isPresent()){

            SummonEntity(this.summonedEntity, this.entityHost, this.summonSound);

            this.summonCooldown = CooldownCreator(this.maxCooldown, this.minCooldown);

        } else if (this.summonCooldown <= 0){

            this.summonCooldown = CooldownCreator(this.maxCooldown, this.minCooldown);
        }
        this.summonCooldown = this.summonCooldown - 1;
    }

    public static class SummonMinionTask extends DelegatingToMCAI<Agent> {

        public SummonMinionTask(Living host, AbstractCustomEntity minionEntity, SoundEvent sound, int minCooldownTime, int maxCooldownTime){
            super(PenguinDungeonAITaskTypes.SUMMON_MINION,
                    new SummonMinion<>(host, minionEntity, sound, minCooldownTime, maxCooldownTime));
        }

        public SummonMinionTask(Living host, AbstractCustomEntity minionEntity, SoundEvent sound, int minCooldownTime, int maxCooldownTime, float triggerHealth){
            super(PenguinDungeonAITaskTypes.SUMMON_MINION,
                    new SummonMinion<>(host, minionEntity, sound, minCooldownTime, maxCooldownTime).setTriggerHealth(triggerHealth));
        }

        @Override
        public boolean canRunConcurrentWith(AITask<Agent> other) {
            return true;
        }
    }
}
