package com.minecraftonline.penguindungeons.customentity.other;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.DyeColor;
import org.spongepowered.api.data.type.DyeColors;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityType;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.ai.Goal;
import org.spongepowered.api.entity.ai.GoalTypes;
import org.spongepowered.api.entity.ai.task.AITask;
import org.spongepowered.api.entity.living.Agent;
import org.spongepowered.api.entity.living.Creature;
import org.spongepowered.api.entity.living.animal.Sheep;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColor;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.World;

import com.flowpowered.math.vector.Vector3d;
import com.google.common.collect.Lists;
import com.minecraftonline.penguindungeons.customentity.AbstractCustomEntity;
import com.minecraftonline.penguindungeons.customentity.AvoidTarget;
import com.minecraftonline.penguindungeons.customentity.EntityAIHurtByNonPD;
import com.minecraftonline.penguindungeons.data.customentity.PDEntityTypeData;
import com.minecraftonline.penguindungeons.data.loot.OwnerLootData;
import com.minecraftonline.penguindungeons.data.spawning.PDSpawnData;
import com.minecraftonline.penguindungeons.equipment.EquipmentLoadout;
import com.minecraftonline.penguindungeons.spawnable.Spawnable;
import com.minecraftonline.penguindungeons.util.ResourceKey;

import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAIAttackMelee;
import net.minecraft.entity.ai.EntityAINearestAttackableTarget;
import net.minecraft.entity.ai.EntityAISwimming;
import net.minecraft.entity.passive.EntitySheep;
import net.minecraft.entity.player.EntityPlayer;

public class PrideSheep extends AbstractCustomEntity {

    public static final double NEW_HEALTH = 22.0;
    private static final double BLOCK_DROP_CHANCE = 0.05;
    private static final String SHEEP_NAME = "Pride Sheep";
    private static final String BLOCK_NAME = "Pride Block";
    private static final int YEAR = Calendar.getInstance().get(Calendar.YEAR);

    private static final List<DyeColor> DYECOLOURS = Lists.newArrayList(DyeColors.RED, DyeColors.ORANGE,
            DyeColors.YELLOW, DyeColors.LIME, DyeColors.BLUE, DyeColors.PURPLE, DyeColors.PINK);
    private static final List<TextColor> TEXTCOLOURS = Lists.newArrayList(TextColors.RED, TextColors.GOLD,
            TextColors.YELLOW, TextColors.GREEN, TextColors.BLUE, TextColors.DARK_PURPLE, TextColors.LIGHT_PURPLE);
    private static final List<String> NAMECOLOURS = Lists.newArrayList("Red", "Orange",
            "Yellow", "Lime", "Blue", "Purple", "Pink");

    public PrideSheep(ResourceKey key) {
        super(key);
    }

    @Override
    public EntityType getType() {
        return EntityTypes.SHEEP;
    }

    public Text getDisplayName() {
        return Text.of(TextColors.WHITE, SHEEP_NAME);
    }

    @Override
    public ItemStack getSpawnEgg() {
        return ItemStack.builder().itemType(ItemTypes.SPAWN_EGG)
                .add(Keys.SPAWNABLE_ENTITY_TYPE, getType())
                .add(Keys.DISPLAY_NAME, getDisplayName())
                .add(Keys.ITEM_LORE, getDescription())
                .itemData(new PDEntityTypeData(getId()))
                .build();
    }

    @Override
    public Spawnable createEntity(World world, Vector3d pos) {
        return Spawnable.of(makeSheep(world, pos), this::onLoad);
    }

    protected Sheep makeSheep(World world, Vector3d blockPos) {
        // 1% chance to be a rarer version
        boolean rare = new Random().nextFloat() < 0.01F;

        Sheep sheep = (Sheep) world.createEntity(getType(), blockPos);
        sheep.offer(new PDEntityTypeData(getId()));
        sheep.offer(new OwnerLootData(ownerLoot()));
        sheep.offer(new PDSpawnData(true));
        sheep.offer(Keys.PERSISTS, false);

        EntitySheep mcSheep = (EntitySheep) sheep;
        mcSheep.getAttributeMap().registerAttribute(SharedMonsterAttributes.ATTACK_DAMAGE).setBaseValue(4.0D);

        int index;
        if (rare)
        {
            // rarer pink sheep
            index = 6;
            sheep.offer(Keys.MAX_HEALTH, 1.0D);
            sheep.offer(Keys.HEALTH, 1.0D);
        }
        else
        {
            // random colour sheep
            index = new Random().nextInt(6);
            sheep.offer(Keys.MAX_HEALTH, NEW_HEALTH);
            sheep.offer(Keys.HEALTH, NEW_HEALTH);
        }
        sheep.offer(Keys.DISPLAY_NAME, Text.of(TEXTCOLOURS.get(index), SHEEP_NAME));
        sheep.offer(Keys.DYE_COLOR, DYECOLOURS.get(index));
        getEquipmentLoadout(index).apply(sheep);
        applyLootTable(sheep);
        return sheep;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onLoad(Entity entity)
    {
        if (!(entity instanceof Sheep)) {
            throw new IllegalArgumentException("Expected a Sheep to be given to PrideSheep to load, but got: " + entity);
        }
        Sheep sheep = (Sheep) entity;
        EntitySheep entitySheep = (EntitySheep) sheep;

        // remove regular tasks
        Goal<Agent> normalGoals = sheep.getGoal(GoalTypes.NORMAL).get();
        normalGoals.clear();
        normalGoals.addTask(1, (AITask<? extends Agent>) new EntityAISwimming(entitySheep));
        Optional<DyeColor> color = sheep.get(Keys.DYE_COLOR);
        if (color.isPresent() && color.get() == DyeColors.PINK)
        {
            normalGoals.addTask(2, new AvoidTarget.AvoidTargetTask(sheep, 10.0F, 1.0D, 1.2D));
        }
        else
        {
            normalGoals.addTask(2, (AITask<? extends Agent>) new EntityAIAttackMelee(entitySheep, 1.0D, false));
        }

        Goal<Agent> targetGoals = sheep.getGoal(GoalTypes.TARGET).get();
        targetGoals.clear();
        targetGoals.addTask(1, new EntityAIHurtByNonPD.TargetNonPDAttackers((Creature) sheep, true, true));
        targetGoals.addTask(2, (AITask<? extends Agent>) new EntityAINearestAttackableTarget<EntityPlayer>(entitySheep, EntityPlayer.class, true));
    }

    protected ItemStack makePrideBlock(int index)
    {
        return ItemStack.builder()
            .itemType(ItemTypes.WOOL)
            .add(Keys.DYE_COLOR, DYECOLOURS.get(index))
            .add(Keys.DISPLAY_NAME, Text.of(TEXTCOLOURS.get(index), BLOCK_NAME))
            .add(Keys.ITEM_LORE,  Arrays.asList(
                Text.of(TextColors.GRAY, "Pride Month ", YEAR),
                Text.of(TextColors.GRAY, NAMECOLOURS.get(index), " ", BLOCK_NAME, " (", index + 1, " of 6)")))
            .build();
    }

    public EquipmentLoadout.Builder makeEquipmentLoadout(int index) {
        return super.makeEquipmentLoadout()
                .chest(makePrideBlock(index), BLOCK_DROP_CHANCE);
    }

    private EquipmentLoadout getEquipmentLoadout(int index) {
        // don't cache
        return makeEquipmentLoadout(index).build();
    }

    public boolean ownerLoot()
    {
        return true;
    }

    @Override
    public List<Text> getEntityDescription() {
        return Collections.singletonList(Text.of(TextColors.WHITE, "A pride sheep"));
    }

}
