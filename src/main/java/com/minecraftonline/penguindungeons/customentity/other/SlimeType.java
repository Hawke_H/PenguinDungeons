package com.minecraftonline.penguindungeons.customentity.other;

import com.minecraftonline.penguindungeons.customentity.AbstractCustomEntity;
import com.minecraftonline.penguindungeons.data.customentity.PDEntityTypeData;
import com.minecraftonline.penguindungeons.data.loot.OwnerLootData;
import com.minecraftonline.penguindungeons.data.spawning.PDSpawnData;
import com.minecraftonline.penguindungeons.spawnable.Spawnable;
import com.minecraftonline.penguindungeons.util.ResourceKey;
import net.minecraft.entity.ai.EntityAIFindEntityNearestPlayer;
import net.minecraft.entity.monster.EntitySlime;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityType;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.ai.Goal;
import org.spongepowered.api.entity.ai.GoalTypes;
import org.spongepowered.api.entity.ai.task.AITask;
import org.spongepowered.api.entity.living.Agent;
import org.spongepowered.api.entity.living.monster.Slime;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.World;

import com.flowpowered.math.vector.Vector3d;

import java.util.Random;

public abstract class SlimeType<T extends Slime> extends AbstractCustomEntity {

    public SlimeType(ResourceKey key) {
        super(key);
    }

    @Override
    public EntityType getType() {
        return EntityTypes.SLIME;
    }

    public abstract Text getDisplayName();

    @Override
    public ItemStack getSpawnEgg() {
        return ItemStack.builder().itemType(ItemTypes.SPAWN_EGG)
                .add(Keys.SPAWNABLE_ENTITY_TYPE, getType())
                .add(Keys.DISPLAY_NAME, getDisplayName())
                .add(Keys.ITEM_LORE, getDescription())
                .itemData(new PDEntityTypeData(getId()))
                .build();
    }

    @Override
    public Spawnable createEntity(World world, Vector3d pos) {
        return Spawnable.of(makeSlime(world, pos, new Random().nextInt(4)), this::onLoad);
    }

    public T makeSlime(World world, Vector3d blockPos, int size) {
        @SuppressWarnings("unchecked")
        T slime = (T) world.createEntity(getType(), blockPos);
        slime.offer(new PDEntityTypeData(getId()));
        slime.offer(new OwnerLootData(ownerLoot()));
        slime.offer(new PDSpawnData(true));
        slime.offer(Keys.DISPLAY_NAME, getDisplayName());
        slime.offer(Keys.PERSISTS, false);
        slime.offer(Keys.SLIME_SIZE, size);

        applyEquipment(slime);

        double health = (size * size) + 2;
        slime.offer(Keys.MAX_HEALTH, health);
        slime.offer(Keys.HEALTH, health);

        if (size <= 0) {
            // only small slimes drop loot
            applyLootTable(slime);
        } else {
            applyEmptyLootTable(slime);
        }

        return slime;
    }

    @Override
    public void onLoad(Entity entity)
    {
        if (!(entity instanceof Slime)) {
            throw new IllegalArgumentException("Expected a Slime to be given to Slime to load, but got: " + entity);
        }
        Slime slime = (Slime) entity;
        EntitySlime entitySlime = (EntitySlime) slime;

        Goal<Agent> targetGoals = slime.getGoal(GoalTypes.TARGET).get();
        targetGoals.clear();
        // slimes have no path finding or sight checking, so just find the nearest player
        targetGoals.addTask(1, (AITask<? extends Agent>) new EntityAIFindEntityNearestPlayer(entitySlime));

        // health has to be reset here because vanilla loading from nbt sets max health
        int size = slime.get(Keys.SLIME_SIZE).orElse(0);
        double health = (size * size) + 2;
        slime.offer(Keys.MAX_HEALTH, health);
        slime.offer(Keys.HEALTH, health);
    }

    public boolean ownerLoot()
    {
        return true;
    }
}
