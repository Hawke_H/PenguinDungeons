package com.minecraftonline.penguindungeons.customentity.other;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.spongepowered.api.boss.BossBarColors;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityType;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.ai.Goal;
import org.spongepowered.api.entity.ai.GoalTypes;
import org.spongepowered.api.entity.ai.task.AITask;
import org.spongepowered.api.entity.living.Agent;
import org.spongepowered.api.entity.living.monster.Ghast;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.World;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.penguindungeons.ai.AIUtil;
import com.minecraftonline.penguindungeons.customentity.AbstractCustomEntity;
import com.minecraftonline.penguindungeons.customentity.CustomEntityTypes;
import com.minecraftonline.penguindungeons.customentity.StraightProjectileAttack;
import com.minecraftonline.penguindungeons.customentity.SummonMinion;
import com.minecraftonline.penguindungeons.data.bossbar.PDBossBarColorData;
import com.minecraftonline.penguindungeons.data.bossbar.PDBossBarData;
import com.minecraftonline.penguindungeons.data.customentity.PDEntityTypeData;
import com.minecraftonline.penguindungeons.data.loot.OwnerLootData;
import com.minecraftonline.penguindungeons.data.spawning.PDSpawnData;
import com.minecraftonline.penguindungeons.spawnable.Spawnable;
import com.minecraftonline.penguindungeons.util.ResourceKey;

import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.monster.EntityGhast;
import net.minecraft.init.SoundEvents;

public class QueenBee extends AbstractCustomEntity{

    private static final double NEW_HEALTH = 302;

    public QueenBee(ResourceKey key) {
        super(key);
    }
    
    @Override
    public EntityType getType() {
        return EntityTypes.GHAST;
    }

    public Text getDisplayName() {
        return Text.of(TextColors.YELLOW, "Queen Bee");
    }

    @Override
    public ItemStack getSpawnEgg() {
        return ItemStack.builder().itemType(ItemTypes.SPAWN_EGG)
                .add(Keys.SPAWNABLE_ENTITY_TYPE, getType())
                .add(Keys.DISPLAY_NAME, getDisplayName())
                .add(Keys.ITEM_LORE, getDescription())
                .itemData(new PDEntityTypeData(getId()))
                .build();
    }

    @Override
    public List<Text> getEntityDescription() {
        return Collections.singletonList(Text.of(TextColors.WHITE, "A giant bee that uses minion bees to fight for it"));
    }
    
    @Override
    public Spawnable createEntity(World world, Vector3d pos) {
        return Spawnable.of(makeQueenBee(world, pos, true), this::onLoad);
    }

    public Ghast makeQueenBee(World world, Vector3d blockPos, boolean showbossbar){
        Ghast ghast = (Ghast) world.createEntity(getType(), blockPos);
        ghast.offer(new PDEntityTypeData(getId()));
        ghast.offer(new OwnerLootData(ownerLoot()));
        ghast.offer(new PDSpawnData(true));
        ghast.offer(Keys.DISPLAY_NAME, getDisplayName());
        ghast.offer(new PDBossBarData(showbossbar));
        ghast.offer(new PDBossBarColorData(BossBarColors.YELLOW));

        // boss health
        ghast.offer(Keys.MAX_HEALTH, NEW_HEALTH);
        ghast.offer(Keys.HEALTH, NEW_HEALTH);

        // equivalent to golden armor
        ((EntityGhast) ghast).getEntityAttribute(SharedMonsterAttributes.ARMOR).applyModifier(new AttributeModifier("Royal Armor", 11, 0));

        // Queen Bee shouldn't despawn
        ghast.offer(Keys.PERSISTS, true);

        ghast.offer(Keys.IS_SILENT, true);

        applyEquipment(ghast);
        applyLootTable(ghast);
        return ghast;
    }

    @SuppressWarnings("unchecked")
    public void onLoad(Entity entity) {
        if (!(entity instanceof Ghast)) {
            throw new IllegalArgumentException("Expected a Ghast to be given to QueenBee to load, but got: " + entity);
        }
        
        Ghast ghast = (Ghast) entity;
        EntityGhast mcGhast = (EntityGhast) ghast;

        //ghasts appear to not be able to implement targetGoals, likely due to them not being an EntityCreature
        Goal<Agent> normalGoals = ghast.getGoal(GoalTypes.NORMAL).get();
        normalGoals.clear();

        // can't easily remove normal fireball task without also clearing the flying and looking tasks
        // because they are all private classes inside EntityGhast, so they are repeated in GhastAITasks.
        normalGoals.addTask(5, (AITask<? extends Agent>) new GhastAITasks.AIRandomFly(mcGhast));
        normalGoals.addTask(7, (AITask<? extends Agent>) new GhastAITasks.AILookAround(mcGhast));

        // so just add this as a higher priority
        normalGoals.addTask(7, new StraightProjectileAttack.RangedAttack(ghast,
                Optional.of(Text.of(TextColors.YELLOW, "Bee")), Optional.empty(),
                AIUtil.ProjectileCreator.forSkulls(
                        AIUtil.ProjectileForImpact.forImpactWithSound(
                                AIUtil.ProjectileForImpact.forImpactDamage(8.0F), SoundEvents.ENTITY_PLAYER_SMALL_FALL),

                        AIUtil.ProjectileForImpact.forMultipleImpacts(
                                AIUtil.ProjectileForImpact.forImpactDamage(10.0F),
                                AIUtil.ProjectileForImpact.forImpactWithSound(
                                        AIUtil.ProjectileForImpact.forImpactExplosion(4.0F, false, false),
                                        SoundEvents.ENTITY_GENERIC_EXPLODE)), 0.20F), 3D, 60, 160, 80F));

        //not the bees!
        normalGoals.addTask(7, new SummonMinion.SummonMinionTask(ghast, CustomEntityTypes.MINION_BEE, SoundEvents.ENTITY_SLIME_SQUISH, 50, 100));
        normalGoals.addTask(7, new SummonMinion.SummonMinionTask(ghast, CustomEntityTypes.MINION_HONEY_SLIME, SoundEvents.ENTITY_SLIME_SQUISH, 70, 160, 91f));
    }
    
    public boolean ownerLoot() {
        return true;
    }
}