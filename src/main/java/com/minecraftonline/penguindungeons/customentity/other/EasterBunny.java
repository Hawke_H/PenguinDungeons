package com.minecraftonline.penguindungeons.customentity.other;

import java.util.Collections;
import java.util.List;
import java.util.Random;

import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.RabbitTypes;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityType;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.ai.Goal;
import org.spongepowered.api.entity.ai.GoalTypes;
import org.spongepowered.api.entity.ai.task.AITask;
import org.spongepowered.api.entity.living.Agent;
import org.spongepowered.api.entity.living.Creature;
import org.spongepowered.api.entity.living.animal.Rabbit;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.World;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.penguindungeons.customentity.AbstractCustomEntity;
import com.minecraftonline.penguindungeons.customentity.EntityAIHurtByNonPD;
import com.minecraftonline.penguindungeons.data.customentity.PDEntityTypeData;
import com.minecraftonline.penguindungeons.data.loot.OwnerLootData;
import com.minecraftonline.penguindungeons.data.spawning.PDSpawnData;
import com.minecraftonline.penguindungeons.spawnable.Spawnable;
import com.minecraftonline.penguindungeons.util.ResourceKey;

import net.minecraft.entity.ai.EntityAIAttackMelee;
import net.minecraft.entity.ai.EntityAILeapAtTarget;
import net.minecraft.entity.ai.EntityAINearestAttackableTarget;
import net.minecraft.entity.ai.EntityAISwimming;
import net.minecraft.entity.passive.EntityRabbit;
import net.minecraft.entity.player.EntityPlayer;

public class EasterBunny extends AbstractCustomEntity {

    public static final double NEW_HEALTH = 22.0;

    public EasterBunny(ResourceKey key) {
        super(key);
    }

    @Override
    public EntityType getType() {
        return EntityTypes.RABBIT;
    }

    public Text getDisplayName() {
        return Text.of(TextColors.WHITE, "Easter Bunny");
    }

    @Override
    public ItemStack getSpawnEgg() {
        return ItemStack.builder().itemType(ItemTypes.SPAWN_EGG)
                .add(Keys.SPAWNABLE_ENTITY_TYPE, getType())
                .add(Keys.DISPLAY_NAME, getDisplayName())
                .add(Keys.ITEM_LORE, getDescription())
                .itemData(new PDEntityTypeData(getId()))
                .build();
    }

    @Override
    public Spawnable createEntity(World world, Vector3d pos) {
        return Spawnable.of(makeRabbit(world, pos), this::onLoad);
    }

    protected Rabbit makeRabbit(World world, Vector3d blockPos) {
        // 10% chance to be a rarer version that can summon boss
        boolean rare = new Random().nextFloat() < 0.1F;

        Rabbit rabbit = (Rabbit) world.createEntity(getType(), blockPos);
        rabbit.offer(new PDEntityTypeData(getId()));
        rabbit.offer(new OwnerLootData(ownerLoot()));
        rabbit.offer(new PDSpawnData(true));
        rabbit.offer(Keys.DISPLAY_NAME, getDisplayName());
        rabbit.offer(Keys.PERSISTS, false);
        if (rare)
        {
            // rarer, regular health
            rabbit.offer(Keys.MAX_HEALTH, 3.0D);
            rabbit.offer(Keys.HEALTH, 3.0D);
            rabbit.offer(Keys.RABBIT_TYPE, RabbitTypes.BROWN);
        }
        else
        {
            rabbit.offer(Keys.MAX_HEALTH, NEW_HEALTH);
            rabbit.offer(Keys.HEALTH, NEW_HEALTH);
            rabbit.offer(Keys.RABBIT_TYPE, RabbitTypes.KILLER);
        }
        applyEquipment(rabbit);
        applyLootTable(rabbit);
        return rabbit;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onLoad(Entity entity)
    {
        if (!(entity instanceof Rabbit)) {
            throw new IllegalArgumentException("Expected a Rabbit to be given to EasterBunny to load, but got: " + entity);
        }
        Rabbit rabbit = (Rabbit) entity;
        EntityRabbit entityRabbit = (EntityRabbit) rabbit;

        // remove regular tasks
        Goal<Agent> normalGoals = rabbit.getGoal(GoalTypes.NORMAL).get();
        normalGoals.clear();
        normalGoals.addTask(1, (AITask<? extends Agent>) new EntityAISwimming(entityRabbit));
        normalGoals.addTask(4, (AITask<? extends Agent>) new EntityAILeapAtTarget(entityRabbit, 0.3F));
        normalGoals.addTask(5, (AITask<? extends Agent>) new EntityAIAttackMelee(entityRabbit, 1.0D, false));

        Goal<Agent> targetGoals = rabbit.getGoal(GoalTypes.TARGET).get();
        targetGoals.clear();
        targetGoals.addTask(1, new EntityAIHurtByNonPD.TargetNonPDAttackers((Creature) rabbit, true, true));
        targetGoals.addTask(2, (AITask<? extends Agent>) new EntityAINearestAttackableTarget<EntityPlayer>(entityRabbit, EntityPlayer.class, true));
    }

    public boolean ownerLoot()
    {
        return false;
    }

    @Override
    public List<Text> getEntityDescription() {
        return Collections.singletonList(Text.of(TextColors.WHITE, "An easter bunny"));
    }

}
