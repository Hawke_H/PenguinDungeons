package com.minecraftonline.penguindungeons.customentity.other;

import java.util.Collections;
import java.util.List;

import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityType;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.ai.Goal;
import org.spongepowered.api.entity.ai.GoalTypes;
import org.spongepowered.api.entity.ai.task.AITask;
import org.spongepowered.api.entity.living.Agent;
import org.spongepowered.api.entity.living.Creature;
import org.spongepowered.api.entity.living.monster.Silverfish;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.World;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.penguindungeons.customentity.AbstractCustomEntity;
import com.minecraftonline.penguindungeons.customentity.EntityAIHurtByNonPD;
import com.minecraftonline.penguindungeons.data.customentity.PDEntityTypeData;
import com.minecraftonline.penguindungeons.data.loot.OwnerLootData;
import com.minecraftonline.penguindungeons.data.spawning.PDSpawnData;
import com.minecraftonline.penguindungeons.spawnable.Spawnable;
import com.minecraftonline.penguindungeons.util.ResourceKey;

import net.minecraft.entity.ai.EntityAIAttackMelee;
import net.minecraft.entity.ai.EntityAILeapAtTarget;
import net.minecraft.entity.ai.EntityAINearestAttackableTarget;
import net.minecraft.entity.ai.EntityAISwimming;
import net.minecraft.entity.ai.EntityAIWanderAvoidWater;
import net.minecraft.entity.monster.EntitySilverfish;
import net.minecraft.entity.player.EntityPlayer;

public class LoveSilverfish extends AbstractCustomEntity {

    public LoveSilverfish(ResourceKey key) {
        super(key);
    }

    @Override
    public EntityType getType() {
        return EntityTypes.SILVERFISH;
    }

    public Text getDisplayName() {
        return Text.of(TextColors.LIGHT_PURPLE, "Love Bug");
    }

    @Override
    public ItemStack getSpawnEgg() {
        return ItemStack.builder().itemType(ItemTypes.SPAWN_EGG)
                .add(Keys.SPAWNABLE_ENTITY_TYPE, getType())
                .add(Keys.DISPLAY_NAME, getDisplayName())
                .add(Keys.ITEM_LORE, getDescription())
                .itemData(new PDEntityTypeData(getId()))
                .build();
    }

    @Override
    public Spawnable createEntity(World world, Vector3d pos) {
        return Spawnable.of(makeSilverfish(world, pos), this::onLoad);
    }

    protected Silverfish makeSilverfish(World world, Vector3d blockPos) {
        Silverfish silverfish = (Silverfish) world.createEntity(getType(), blockPos);
        silverfish.offer(new PDEntityTypeData(getId()));
        silverfish.offer(new OwnerLootData(ownerLoot()));
        silverfish.offer(new PDSpawnData(true));
        silverfish.offer(Keys.DISPLAY_NAME, getDisplayName());
        silverfish.offer(Keys.PERSISTS, false);
        applyEquipment(silverfish);
        applyLootTable(silverfish);
        return silverfish;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onLoad(Entity entity)
    {
        if (!(entity instanceof Silverfish)) {
            throw new IllegalArgumentException("Expected a Silverfish to be given to LoveSilverfish to load, but got: " + entity);
        }
        Silverfish silverfish = (Silverfish) entity;
        EntitySilverfish entitySilverfish = (EntitySilverfish) silverfish;

        // remove tasks for hiding in blocks etc
        Goal<Agent> normalGoals = silverfish.getGoal(GoalTypes.NORMAL).get();
        normalGoals.clear();
        normalGoals.addTask(1, (AITask<? extends Agent>) new EntityAISwimming(entitySilverfish));
        normalGoals.addTask(4, (AITask<? extends Agent>) new EntityAILeapAtTarget(entitySilverfish, 0.3F));
        normalGoals.addTask(5, (AITask<? extends Agent>) new EntityAIAttackMelee(entitySilverfish, 1.0D, false));
        normalGoals.addTask(6, (AITask<? extends Agent>) new EntityAIWanderAvoidWater(entitySilverfish, 1.0D));

        Goal<Agent> targetGoals = silverfish.getGoal(GoalTypes.TARGET).get();
        targetGoals.clear();
        targetGoals.addTask(1, new EntityAIHurtByNonPD.TargetNonPDAttackers((Creature) silverfish, true, true));
        targetGoals.addTask(2, (AITask<? extends Agent>) new EntityAINearestAttackableTarget<EntityPlayer>(entitySilverfish, EntityPlayer.class, true));
    }

    public boolean ownerLoot()
    {
        return true;
    }

    @Override
    public List<Text> getEntityDescription() {
        return Collections.singletonList(Text.of(TextColors.WHITE, "A silverfish love bug"));
    }

}
