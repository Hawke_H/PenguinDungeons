package com.minecraftonline.penguindungeons.customentity.golem;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.spongepowered.api.effect.potion.PotionEffect;
import org.spongepowered.api.effect.potion.PotionEffectTypes;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.ai.Goal;
import org.spongepowered.api.entity.ai.GoalTypes;
import org.spongepowered.api.entity.ai.task.AITask;
import org.spongepowered.api.entity.living.Agent;
import org.spongepowered.api.entity.living.Creature;
import org.spongepowered.api.entity.living.golem.SnowGolem;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import com.minecraftonline.penguindungeons.ai.AIUtil;
import com.minecraftonline.penguindungeons.ai.AIUtil.DirectShulkerBulletCreator;
import com.minecraftonline.penguindungeons.customentity.EntityAIHurtByNonPD;
import com.minecraftonline.penguindungeons.customentity.StraightProjectileAttack;
import com.minecraftonline.penguindungeons.util.ResourceKey;

import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.ai.EntityAIAttackRanged;
import net.minecraft.entity.ai.EntityAIHurtByTarget;
import net.minecraft.entity.ai.EntityAINearestAttackableTarget;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.SoundEvents;

public class SnowflakeGolem extends SnowGolemType {

    public SnowflakeGolem(ResourceKey key) {
        super(key);
    }

    public Text getDisplayName() {
        return Text.of(TextColors.WHITE, "Snowflake Golem");
    }

    public boolean wearPumpkin() {
        return false;
    }

    @Override
    public List<Text> getEntityDescription() {
        return Collections.singletonList(Text.of(TextColors.WHITE, "A snow golem that shoots straight shulker bullets"));
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onLoad(Entity entity) {
        if (!(entity instanceof SnowGolem)) {
            throw new IllegalArgumentException("Expected a SnowGolem to be given to SnowflakeGolem to load, but got: " + entity);
        }
        SnowGolem snowGolem = (SnowGolem) entity;

        Goal<Agent> normalGoals = snowGolem.getGoal(GoalTypes.NORMAL).get();
        // Remove regular attack.
        normalGoals.getTasks().stream().filter(aiTask -> aiTask instanceof EntityAIAttackRanged)
                .forEach(task -> normalGoals.removeTask((AITask<? extends Agent>) task));

        normalGoals.addTask(1, new StraightProjectileAttack.RangedAttack(snowGolem,
                Optional.of(Text.of("Snowflake")), Optional.empty(),
                DirectShulkerBulletCreator.forDirectBullet(
                        AIUtil.ShulkerBulletCreator.forHit(
                                PotionEffect.of(PotionEffectTypes.SLOWNESS, 1, TICKS_PER_SECOND*10)),
                                SoundEvents.BLOCK_GLASS_BREAK)));

        // attack players and things that attack PD mobs
        Goal<Agent> targetGoals = snowGolem.getGoal(GoalTypes.TARGET).get();
        targetGoals.clear();
        targetGoals.addTask(1, new EntityAIHurtByNonPD.TargetNonPDAttackers((Creature) entity, true, true));
        targetGoals.addTask(2, (AITask<? extends Agent>) new EntityAINearestAttackableTarget<EntityPlayer>((EntityCreature) entity, EntityPlayer.class, true));
    }
}
