package com.minecraftonline.penguindungeons.customentity.golem;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.spongepowered.api.boss.BossBarColors;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityType;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.ai.Goal;
import org.spongepowered.api.entity.ai.GoalTypes;
import org.spongepowered.api.entity.ai.task.AITask;
import org.spongepowered.api.entity.living.Agent;
import org.spongepowered.api.entity.living.Creature;
import org.spongepowered.api.entity.living.golem.IronGolem;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.World;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.penguindungeons.ai.AIUtil;
import com.minecraftonline.penguindungeons.customentity.AbstractCustomEntity;
import com.minecraftonline.penguindungeons.customentity.ArchProjectileAttack;
import com.minecraftonline.penguindungeons.customentity.EntityAIHurtByNonPD;
import com.minecraftonline.penguindungeons.customentity.JumpToTarget;
import com.minecraftonline.penguindungeons.data.bossbar.PDBossBarColorData;
import com.minecraftonline.penguindungeons.data.bossbar.PDBossBarData;
import com.minecraftonline.penguindungeons.data.customentity.PDEntityTypeData;
import com.minecraftonline.penguindungeons.data.loot.OwnerLootData;
import com.minecraftonline.penguindungeons.data.spawning.PDSpawnData;
import com.minecraftonline.penguindungeons.spawnable.Spawnable;
import com.minecraftonline.penguindungeons.util.ResourceKey;

import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAILookAtVillager;
import net.minecraft.entity.ai.EntityAIMoveThroughVillage;
import net.minecraft.entity.ai.EntityAINearestAttackableTarget;
import net.minecraft.entity.monster.EntityIronGolem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.SoundEvents;

public class EggGolem extends AbstractCustomEntity {

    public static final double NEW_HEALTH = 302.0;
    public static final double NEW_SPEED = 0.35;

    public EggGolem(ResourceKey key) {
        super(key);
    }

    @Override
    public EntityType getType() {
        return EntityTypes.IRON_GOLEM;
    }

    public Text getDisplayName() {
        return Text.of(TextColors.GOLD, "Power Paws");
    }

    @Override
    public List<Text> getEntityDescription() {
        return Collections.singletonList(Text.of(TextColors.WHITE, "An iron golem that throws eggs"));
    }

    @Override
    public ItemStack getSpawnEgg() {
        return ItemStack.builder().itemType(ItemTypes.SPAWN_EGG)
                .add(Keys.SPAWNABLE_ENTITY_TYPE, getType())
                .add(Keys.DISPLAY_NAME, getDisplayName())
                .add(Keys.ITEM_LORE, getDescription())
                .itemData(new PDEntityTypeData(getId()))
                .build();
    }

    @Override
    public Spawnable createEntity(World world, Vector3d pos) {
        return Spawnable.of(makeEggGolem(world, pos, true), this::onLoad);
    }

    public IronGolem makeEggGolem(World world, Vector3d blockPos, boolean showBossBar) {
        IronGolem ironGolem = (IronGolem) world.createEntity(getType(), blockPos);
        ironGolem.offer(Keys.DISPLAY_NAME, getDisplayName());
        ironGolem.offer(new PDEntityTypeData(getId()));
        ironGolem.offer(new OwnerLootData(ownerLoot()));
        ironGolem.offer(new PDSpawnData(true));
        ironGolem.offer(new PDBossBarData(showBossBar));
        ironGolem.offer(new PDBossBarColorData(BossBarColors.YELLOW));
        ironGolem.offer(Keys.MAX_HEALTH, NEW_HEALTH);
        ironGolem.offer(Keys.HEALTH, NEW_HEALTH);
        ((EntityIronGolem) ironGolem).getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(NEW_SPEED);
        applyEquipment(ironGolem);
        applyLootTable(ironGolem);
        return ironGolem;
    }

    @SuppressWarnings("unchecked")
    public void onLoad(Entity entity) {
        if (!(entity instanceof IronGolem)) {
            throw new IllegalArgumentException("Expected a IronGolem to be given to EggGolem to load, but got: " + entity);
        }
        IronGolem ironGolem = (IronGolem) entity;

        Goal<Agent> normalGoals = ironGolem.getGoal(GoalTypes.NORMAL).get();

        // remove task for moving through village and looking at villagers
        normalGoals.getTasks().stream().filter(aiTask -> aiTask instanceof EntityAIMoveThroughVillage || aiTask instanceof EntityAILookAtVillager)
            .forEach(task -> normalGoals.removeTask((AITask<? extends Agent>) task));

        // attack players
        Goal<Agent> targetGoals = ironGolem.getGoal(GoalTypes.TARGET).get();
        targetGoals.clear();

        // add new attacks
        targetGoals.addTask(1, new EntityAIHurtByNonPD.TargetNonPDAttackers((Creature) entity, true, true));

        targetGoals.addTask(2, (AITask<? extends Agent>) new EntityAINearestAttackableTarget<EntityPlayer>((EntityCreature) entity, EntityPlayer.class, true));

        // jump!
        normalGoals.addTask(2, new JumpToTarget.JumpAtTarget((Creature) entity, 1.0f, 10.0D, 25.0D, 0.1f, SoundEvents.ENTITY_RABBIT_JUMP));

        normalGoals.addTask(3, new ArchProjectileAttack.RangedArchAttack(ironGolem, Optional.of("Easter Egg"),
                AIUtil.ProjectileCreator.forEggs(
                    AIUtil.ProjectileForImpact.forMultipleImpacts(
                        AIUtil.ProjectileForImpact.forImpactWithSound(
                                AIUtil.ProjectileForImpact.forImpactDamage(5.0F),
                                SoundEvents.BLOCK_CLOTH_FALL
                        )
                    )
                ), 1.3D, 8, 20, 40F));
    }

    public boolean ownerLoot()
    {
        return true;
    }
}
