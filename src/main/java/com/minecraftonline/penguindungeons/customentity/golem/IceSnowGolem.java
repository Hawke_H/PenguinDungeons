package com.minecraftonline.penguindungeons.customentity.golem;

import java.util.Collections;
import java.util.List;
import java.util.Random;

import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.ai.Goal;
import org.spongepowered.api.entity.ai.GoalTypes;
import org.spongepowered.api.entity.ai.task.AITask;
import org.spongepowered.api.entity.living.Agent;
import org.spongepowered.api.entity.living.Creature;
import org.spongepowered.api.entity.living.golem.SnowGolem;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.World;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.penguindungeons.ai.DelegatingToMCAI;
import com.minecraftonline.penguindungeons.customentity.EntityAIHurtByNonPD;
import com.minecraftonline.penguindungeons.data.PenguinDungeonAITaskTypes;
import com.minecraftonline.penguindungeons.util.ResourceKey;

import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAIAttackMelee;
import net.minecraft.entity.ai.EntityAIAttackRanged;
import net.minecraft.entity.ai.EntityAINearestAttackableTarget;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.monster.EntitySnowman;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.ItemAxe;
import net.minecraft.item.ItemStack;
import net.minecraft.util.DamageSource;
import net.minecraft.util.math.MathHelper;

public class IceSnowGolem extends SnowGolemType {
    public IceSnowGolem(ResourceKey key) {
        super(key);
    }

    public Text getDisplayName() {
        return Text.of(TextColors.DARK_AQUA, "Ice Golem");
    }

    public boolean wearPumpkin() {
        return true;
    }

    @Override
    public List<Text> getEntityDescription() {
        return Collections.singletonList(Text.of(TextColors.WHITE, "A snow golem that moves fast and attacks with melee"));
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onLoad(Entity entity) {
        if (!(entity instanceof SnowGolem)) {
            throw new IllegalArgumentException("Expected a SnowGolem to be given to IceSnowGolem to load, but got: " + entity);
        }
        SnowGolem snowGolem = (SnowGolem) entity;

        Goal<Agent> targetGoals = snowGolem.getGoal(GoalTypes.TARGET).get();
        targetGoals.clear();

        Goal<Agent> normalGoals = snowGolem.getGoal(GoalTypes.NORMAL).get();
        // Remove regular attack.
        normalGoals.getTasks().stream().filter(aiTask -> aiTask instanceof EntityAIAttackRanged)
                .forEach(task -> normalGoals.removeTask((AITask<? extends Agent>) task));

        normalGoals.addTask(1, new MeleeAttack(snowGolem));

        targetGoals.addTask(1, new EntityAIHurtByNonPD.TargetNonPDAttackers((Creature) entity, true, true));
        targetGoals.addTask(2, (AITask<? extends Agent>) new EntityAINearestAttackableTarget<EntityPlayer>((EntityCreature) snowGolem, EntityPlayer.class, true));
    }

    @Override
    protected SnowGolem makeSnowGolem(World world, Vector3d blockPos) {
        SnowGolem snowGolem = super.makeSnowGolem(world, blockPos);

        EntitySnowman mcSnowGolem = (EntitySnowman) snowGolem;
        mcSnowGolem.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).applyModifier(new AttributeModifier("Ice golem", 0.5, 1));
//        mcSnowGolem.getEntityAttribute(SharedMonsterAttributes.ATTACK_DAMAGE).setBaseValue(3.0D);

        return snowGolem;
    }

    public static class MeleeAttack extends DelegatingToMCAI<SnowGolem> {

        public MeleeAttack(SnowGolem snowGolem) {
            super(PenguinDungeonAITaskTypes.SNOW_GOLEM_MELEE_ATTACK, new MCMeleeAttack((EntityCreature) snowGolem));
        }
    }

    public static class MCMeleeAttack extends EntityAIAttackMelee {
        protected EntityCreature creature;
        protected Random rand;
        
        public MCMeleeAttack(EntityCreature snowGolem) {
           super(snowGolem, 1.25D, true);
           this.rand = new Random();
        }

        protected void checkAndPerformAttack(EntityLivingBase enemy, double distToEnemySqr) {
           double d0 = this.getAttackReachSqr(enemy);
           if (distToEnemySqr <= d0 && this.attackTick <= 0) {
              this.attackTick = 20;
              // actually attack
//              float f = (float)this.attacker.getEntityAttribute(SharedMonsterAttributes.ATTACK_DAMAGE).getAttributeValue();
              float f = (float)3.0D;
              int i = 0;
              if (enemy instanceof EntityLivingBase) {
                 f += EnchantmentHelper.getModifierForCreature(this.attacker.getHeldItemMainhand(), ((EntityLivingBase)enemy).getCreatureAttribute());
                 i += EnchantmentHelper.getKnockbackModifier(this.attacker);
              }
              boolean flag = enemy.attackEntityFrom(DamageSource.causeMobDamage(this.attacker), f);
              if (flag) {
                 if (i > 0 && enemy instanceof EntityLivingBase) {
                    ((EntityLivingBase)enemy).knockBack(this.attacker, (float)i * 0.5F, (double)MathHelper.sin(this.attacker.rotationYaw * 0.017453292F), (double)(-MathHelper.cos(this.attacker.rotationYaw * 0.017453292F)));
                    this.attacker.motionX *= 0.6D;
                    this.attacker.motionZ *= 0.6D;
                 }

                 int j = EnchantmentHelper.getFireAspectModifier(this.attacker);
                 if (j > 0) {
                    enemy.setFire(j * 4);
                 }

                 if ((Entity) enemy instanceof EntityPlayer) {
                    EntityPlayer entityplayer = (EntityPlayer)(Entity)enemy;
                    ItemStack itemstack = this.attacker.getHeldItemMainhand();
                    ItemStack itemstack1 = entityplayer.isHandActive() ? entityplayer.getActiveItemStack() : ItemStack.EMPTY;
                    if (!itemstack.isEmpty() && !itemstack1.isEmpty() && itemstack.getItem() instanceof ItemAxe && itemstack1.getItem() == Items.SHIELD) {
                       float f1 = 0.25F + (float)EnchantmentHelper.getEfficiencyModifier(this.attacker) * 0.05F;
                       if (this.rand.nextFloat() < f1) {
                          entityplayer.getCooldownTracker().setCooldown(Items.SHIELD, 100);
                          this.attacker.world.setEntityState(entityplayer, (byte)30);
                       }
                    }
                 }

                 // doesn't work because not visible
                 //this.attacker.applyEnchantments(this.attacker, enemy);
              }
           } else if (distToEnemySqr <= d0 * 2.0D) {
              if (this.attackTick <= 0) {
                 this.attackTick = 20;
              }
           }

        }

        /**
         * Reset the task's internal state. Called when this task is interrupted by another one
         */
        public void resetTask() {
           super.resetTask();
        }

        protected double getAttackReachSqr(EntityLivingBase attackTarget) {
           return (double)(4.0F + attackTarget.width);
        }
     }
}