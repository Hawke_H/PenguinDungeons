package com.minecraftonline.penguindungeons.customentity.golem;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.spongepowered.api.boss.BossBarColors;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.effect.potion.PotionEffect;
import org.spongepowered.api.effect.potion.PotionEffectTypes;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityType;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.ai.Goal;
import org.spongepowered.api.entity.ai.GoalTypes;
import org.spongepowered.api.entity.ai.task.AITask;
import org.spongepowered.api.entity.living.Agent;
import org.spongepowered.api.entity.living.Creature;
import org.spongepowered.api.entity.living.golem.IronGolem;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.World;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.penguindungeons.ai.AIUtil;
import com.minecraftonline.penguindungeons.customentity.AbstractCustomEntity;
import com.minecraftonline.penguindungeons.customentity.EntityAIHurtByNonPD;
import com.minecraftonline.penguindungeons.customentity.HasEffects;
import com.minecraftonline.penguindungeons.customentity.ProjectileData;
import com.minecraftonline.penguindungeons.customentity.ShootLasers.LaserAttack;
import com.minecraftonline.penguindungeons.customentity.StraightProjectileAttack;
import com.minecraftonline.penguindungeons.data.bossbar.PDBossBarColorData;
import com.minecraftonline.penguindungeons.data.bossbar.PDBossBarData;
import com.minecraftonline.penguindungeons.data.customentity.PDEntityTypeData;
import com.minecraftonline.penguindungeons.data.loot.OwnerLootData;
import com.minecraftonline.penguindungeons.data.spawning.PDSpawnData;
import com.minecraftonline.penguindungeons.spawnable.Spawnable;
import com.minecraftonline.penguindungeons.util.ResourceKey;

import net.minecraft.block.Block;
import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.ai.EntityAILookAtVillager;
import net.minecraft.entity.ai.EntityAIMoveThroughVillage;
import net.minecraft.entity.ai.EntityAINearestAttackableTarget;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.MobEffects;
import net.minecraft.init.SoundEvents;
import net.minecraft.util.EnumParticleTypes;

public class FrostGolem extends AbstractCustomEntity implements HasEffects {

    public static final double NEW_HEALTH = 300.0;
    public static final double NEW_SPEED = 0.75;

    public FrostGolem(ResourceKey key) {
        super(key);
    }

    @Override
    public EntityType getType() {
        return EntityTypes.IRON_GOLEM;
    }

    public Text getDisplayName() {
        return Text.of(TextColors.DARK_AQUA, "Frost Golem");
    }

    @Override
    public List<Text> getEntityDescription() {
        return Collections.singletonList(Text.of(TextColors.WHITE, "An iron golem that shoots a laser and ice cube"));
    }

    @Override
    public ItemStack getSpawnEgg() {
        return ItemStack.builder().itemType(ItemTypes.SPAWN_EGG)
                .add(Keys.SPAWNABLE_ENTITY_TYPE, getType())
                .add(Keys.DISPLAY_NAME, getDisplayName())
                .add(Keys.ITEM_LORE, getDescription())
                .itemData(new PDEntityTypeData(getId()))
                .build();
    }

    @Override
    public Spawnable createEntity(World world, Vector3d pos) {
        return Spawnable.of(makeFrostGolem(world, pos, true, false), this::onLoad);
    }

    public IronGolem makeFrostGolem(World world, Vector3d blockPos, boolean showBossBar, boolean startInPhase2) {
        IronGolem ironGolem = (IronGolem) world.createEntity(getType(), blockPos);
        ironGolem.offer(Keys.DISPLAY_NAME, getDisplayName());
        ironGolem.offer(new PDEntityTypeData(getId()));
        ironGolem.offer(new OwnerLootData(ownerLoot()));
        ironGolem.offer(new PDSpawnData(true));
        ironGolem.offer(new PDBossBarData(showBossBar));
        ironGolem.offer(new PDBossBarColorData(BossBarColors.BLUE));
        if (startInPhase2) {
            // increase max health so the starting half is less than half
            ironGolem.offer(Keys.MAX_HEALTH, NEW_HEALTH*3);
        } else {
            ironGolem.offer(Keys.MAX_HEALTH, NEW_HEALTH);
        }
        ironGolem.offer(Keys.HEALTH, NEW_HEALTH);
        ironGolem.offer(Keys.WALKING_SPEED, NEW_SPEED);
        applyEquipment(ironGolem);
        applyLootTable(ironGolem);
        return ironGolem;
    }

    @SuppressWarnings("unchecked")
    public void onLoad(Entity entity) {
        if (!(entity instanceof IronGolem)) {
            throw new IllegalArgumentException("Expected a IronGolem to be given to FrostGolem to load, but got: " + entity);
        }
        IronGolem ironGolem = (IronGolem) entity;

        Goal<Agent> normalGoals = ironGolem.getGoal(GoalTypes.NORMAL).get();

        // remove task for moving through village and looking at villagers
        normalGoals.getTasks().stream().filter(aiTask -> aiTask instanceof EntityAIMoveThroughVillage || aiTask instanceof EntityAILookAtVillager)
        .forEach(task -> normalGoals.removeTask((AITask<? extends Agent>) task));

        normalGoals.addTask(2, new LaserAttack(ironGolem, this, "\u00a73Laser Turret"));

        // attack players
        Goal<Agent> targetGoals = ironGolem.getGoal(GoalTypes.TARGET).get();
        targetGoals.clear();

        // add new attacks
        targetGoals.addTask(1, new EntityAIHurtByNonPD.TargetNonPDAttackers((Creature) entity, true, true));

        targetGoals.addTask(2, (AITask<? extends Agent>) new EntityAINearestAttackableTarget<EntityPlayer>((EntityCreature) entity, EntityPlayer.class, true));

        normalGoals.addTask(2, new StraightProjectileAttack.RangedAttack(ironGolem,
                Optional.of(Text.of(TextColors.RESET, "Present")), Optional.empty(),
                AIUtil.ProjectileCreator.forSkulls(
                    AIUtil.ProjectileForImpact.forMultipleImpacts(
                        AIUtil.ProjectileForImpact.forImpactDamage(8.0F),
                        AIUtil.ProjectileForImpact.forImpactWithSound(
                            AIUtil.ProjectileForImpact.forImpactExplosion(1.0F, false, false),
                                SoundEvents.BLOCK_CLOTH_BREAK
                            )
                    ),
                    AIUtil.ProjectileForImpact.forMultipleImpacts(
                        AIUtil.ProjectileForImpact.forImpactDamage(4.0F),
                        AIUtil.ProjectileForImpact.forImpactWithSound(
                            AIUtil.ProjectileForImpact.forImpactAreaOfEffect(1.5F, TICKS_PER_SECOND*5,
                                new ProjectileData((net.minecraft.potion.PotionEffect)
                                    PotionEffect.of(PotionEffectTypes.SLOWNESS, 2, TICKS_PER_SECOND*2),
                                    EnumParticleTypes.BLOCK_CRACK, Block.getIdFromBlock(Blocks.ICE), 0)),
                                SoundEvents.BLOCK_GLASS_BREAK
                            )
                    ),
                    0.33F
                ), 1.0D, 20, 80, 20F));
    }

    public boolean ownerLoot()
    {
        return true;
    }

    @Override
    public List<net.minecraft.potion.PotionEffect> getEffects() {
        return Arrays.asList(
                new net.minecraft.potion.PotionEffect(MobEffects.INSTANT_DAMAGE),
                new net.minecraft.potion.PotionEffect(MobEffects.SLOWNESS, TICKS_PER_SECOND*5, 1));
    }
}
