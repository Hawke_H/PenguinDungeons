package com.minecraftonline.penguindungeons.customentity;

import java.util.HashSet;
import java.util.Optional;

import javax.annotation.Nullable;

import org.spongepowered.api.entity.living.Creature;
import org.spongepowered.api.entity.living.Living;

import com.google.common.collect.Sets;
import com.minecraftonline.penguindungeons.ai.DelegatingToMCAI;
import com.minecraftonline.penguindungeons.data.PenguinDungeonAITaskTypes;
import com.minecraftonline.penguindungeons.data.PenguinDungeonKeys;
import com.minecraftonline.penguindungeons.util.ResourceKey;

import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.ai.EntityAIHurtByTarget;
import net.minecraft.util.math.AxisAlignedBB;

public class EntityAIHurtByNonPD extends EntityAIHurtByTarget {
   private final boolean anyPDType;

   // these mobs can be targeted by other dungeon mobs
   // and will not be asked by other dungeon mobs to target something
   private static HashSet<ResourceKey> nonDungeonMobs = Sets.newHashSet(
           CustomEntityTypes.STEVE_POLICE.getId(), CustomEntityTypes.STANDING_HUMAN.getId(),
           CustomEntityTypes.WALKING_HUMAN.getId(), CustomEntityTypes.AGGRESSIVE_HUMAN.getId(),
           CustomEntityTypes.AGGRESSIVE_HUMAN.getId(), CustomEntityTypes.PROTECTIVE_HUMAN.getId(),
           CustomEntityTypes.GUARDING_HUMAN.getId(), CustomEntityTypes.PIGCHINKO_PLAYER.getId(),
           CustomEntityTypes.TRADING_CUPID.getId(), CustomEntityTypes.KEA.getId());

   public EntityAIHurtByNonPD(EntityCreature creatureIn, boolean entityCallsForHelpIn, Class<?>... excludedReinforcementTypes) {
      this(creatureIn, entityCallsForHelpIn, false, excludedReinforcementTypes);
   }

   public EntityAIHurtByNonPD(EntityCreature creatureIn, boolean entityCallsForHelpIn, boolean anyPDType, Class<?>... excludedReinforcementTypes) {
      super(creatureIn, entityCallsForHelpIn, excludedReinforcementTypes);
      this.anyPDType = anyPDType;
   }

   @Override
   protected void alertOthers() {
       double d0 = this.getTargetDistance();

       if (anyPDType)
       {
          // alert other PenguinDungeon mobs even if they are not the same entity type
          for(EntityLiving entityliving : this.taskOwner.world.getEntitiesWithinAABB(EntityLiving.class, (new AxisAlignedBB(this.taskOwner.posX, this.taskOwner.posY, this.taskOwner.posZ, this.taskOwner.posX + 1.0D, this.taskOwner.posY + 1.0D, this.taskOwner.posZ + 1.0D)).grow(d0, 10.0D, d0))) {
             if (this.taskOwner != entityliving && (entityliving.getAttackTarget() == null || !entityliving.getAttackTarget().isEntityAlive()) && !entityliving.isOnSameTeam(this.taskOwner.getRevengeTarget())) {

                Living living = (Living) entityliving;
                // skip if not a penguin dungeon entity, or non-dungeon mobs
                Optional<ResourceKey> pdType = living.get(PenguinDungeonKeys.PD_ENTITY_TYPE);
                if (!pdType.isPresent() || nonDungeonMobs.contains(pdType.get())) continue;

                entityliving.setAttackTarget(this.taskOwner.getRevengeTarget());
              }
           }
       }

       super.alertOthers();
   }

   @Override
   protected boolean isSuitableTarget(@Nullable EntityLivingBase target, boolean includeInvincibles)
   {
       // other PenguinDungeon mobs are not suitable targets
       Optional<ResourceKey> pdType = ((Living)target).get(PenguinDungeonKeys.PD_ENTITY_TYPE);
       if (pdType.isPresent() && !nonDungeonMobs.contains(pdType.get())) return false;
       return super.isSuitableTarget(target, includeInvincibles);
   }

   public static class TargetNonPDAttackers extends DelegatingToMCAI<Creature> {

       public TargetNonPDAttackers(Creature creature, boolean entityCallsForHelpIn, Class<?>... excludedReinforcementTypes) {
           this(creature, entityCallsForHelpIn, false, excludedReinforcementTypes);
       }

       public TargetNonPDAttackers(Creature creature, boolean entityCallsForHelpIn, boolean anyPDType, Class<?>... excludedReinforcementTypes) {
           super(PenguinDungeonAITaskTypes.ATTACK_BACK_NON_PD,
                   new EntityAIHurtByNonPD((EntityCreature) creature, entityCallsForHelpIn, anyPDType, excludedReinforcementTypes));
       }
   }
}
