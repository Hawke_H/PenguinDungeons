package com.minecraftonline.penguindungeons.customentity.human;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityType;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.ai.Goal;
import org.spongepowered.api.entity.ai.GoalTypes;
import org.spongepowered.api.entity.ai.task.builtin.LookIdleAITask;
import org.spongepowered.api.entity.ai.task.builtin.WatchClosestAITask;
import org.spongepowered.api.entity.living.Agent;
import org.spongepowered.api.entity.living.Creature;
import org.spongepowered.api.entity.living.Human;
import org.spongepowered.api.entity.living.animal.Pig;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.World;
import org.spongepowered.common.entity.living.human.EntityHuman;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.penguindungeons.customentity.CustomEntityTypes;
import com.minecraftonline.penguindungeons.spawnable.Spawnable;
import com.minecraftonline.penguindungeons.util.ResourceKey;

public class PigchinkoPlayer extends HumanType {

    private static final String PIGCHINKO_SKIN_TEXTURE   = "ewogICJ0aW1lc3RhbXAiIDogMTYwOTQ0NTUzODU0MCwKICAicHJvZmlsZUlkIiA6ICIxMGVhMzQwMzRiYjg0ZWMwYmY5MThmMTY2MTBlNDhkOCIsCiAgInByb2ZpbGVOYW1lIiA6ICJQaWdjaGlua28iLAogICJzaWduYXR1cmVSZXF1aXJlZCIgOiB0cnVlLAogICJ0ZXh0dXJlcyIgOiB7CiAgICAiU0tJTiIgOiB7CiAgICAgICJ1cmwiIDogImh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvYTBmMTNmZDljNTdmY2UwNzllMTcyOGE5YmE5YTYyYjg3ZDcxNWFiNmJkODk0ZWNlMDg3M2ZmY2NiNDk0NTY2MyIKICAgIH0KICB9Cn0=";
    private static final String PIGCHINKO_SKIN_SIGNATURE = "DRV3PBVSxUjhAIKeXQ7XATtcR9dzdYGR7EYfgEk81vVF3ru7cGbN+c/eDKpE6isnMpVd8uugGMOYImSBsVoSEonb184CNdUycM1xsD/+QkMgM2wuTuVyUVN5lJJBAOn6ScGYBb938Ry2o175p4wCcszzFewaUI0rcFkEnpXwSipizjG+IThtj5GDXOh0ZZQiHTfWscejdYAqgpczuOJVmLZyh9RzqTc1/Cbnyw3z9W2tjTJMkl0IjRagdcZyT4GTy8uX2mFTQ7jQWuzJYSoZ3QOTCqAAeTIn0CZ6CKpJb1a73K01gEvhQnn9JRajeE/tK7bJCZsle6tkCv+odGrjzbntsZDm1xXufGCFlCmi14r5wPiOPa3yi5Lwzga8s7yJI8QqbHWfF8uie8q9r6uZNchATjaBUvgmDfZmDeC61n0pdR8lq0Clo3IFrL22aT9wMetjkl3rh2LpP1cL8r4qsxAjqiSX5W9uxnsjsX4HR0uy+iMXWG99rQXtncpOupDsS7Euq0KBt8VMi02kL4Krqul8sxK/hIZ2/+5bdKiFOfSWFVjKhQxKZBgO7kHNICRCJ8dD9/qydpYvst9YelUZOJwHDrT60j4DaJ7eriJ3vcc6hDEUN9p2R4M96w9ownwlbXdYUTViRp822OAGN5G2rXjBC7VYTOP2w3pU2WCc96o=";

    //private static final String PIGCHINKO_UUID = "8e838ef3-9910-5730-8b64-42395eecf334"; // pigchinko_player

    public PigchinkoPlayer(ResourceKey key) {
        super(key);
    }

    @Override
    public EntityType getType() {
        return EntityTypes.PIG;
    }

    @Override
    public void onLoad(Entity entity)
    {
        if (!(entity instanceof Human)) {
            throw new IllegalArgumentException("Expected a human to be given to PigchinkoPlayer to load, but got: " + entity);
        }
        Human human = (Human) entity;
        addAITasks(human);
    }

    @Override
    public Spawnable createEntity(World world, Vector3d blockPos) {
        Human human = super.makeHuman(world, blockPos);
        applySkinFromTexture(human, PIGCHINKO_SKIN_TEXTURE, PIGCHINKO_SKIN_SIGNATURE);

        ((EntityHuman) human).addTag("Pigchinko");

        Pig pig = CustomEntityTypes.PIGCHINKO_PIG.makePig(world, blockPos);

        return world1 -> {
            world1.spawnEntity(human);
            onLoad(human);
            world1.spawnEntity(pig);
            CustomEntityTypes.PIGCHINKO_PIG.onLoad(pig);
            pig.addPassenger(human);
            return pig;
        };
    }

    private void addAITasks(Human human) {
        Agent agent = (Agent) human;

        WatchClosestAITask watchTask = WatchClosestAITask.builder()
                .watch(Player.class)
                .maxDistance(8.0F)
                .build((Creature) agent);

        LookIdleAITask idleTask = LookIdleAITask.builder()
                .build((Creature) agent);

        Optional<Goal<Agent>> normalGoals = agent.getGoal(GoalTypes.NORMAL);
        if (normalGoals.isPresent()) {
            normalGoals.get().addTask(8, watchTask)
                             .addTask(8, idleTask);
        }
    }

    @Override
    public Text getDisplayName() {
        return Text.of(TextColors.LIGHT_PURPLE, "Pigchinko Player");
    }

    @Override
    public Text getEntityName() {
        return Text.of("Pigchinko");
    }

    @Override
    public List<Text> getEntityDescription() {
        return Collections.singletonList(Text.of(TextColors.WHITE, "Spawns a Pigchinko Player riding a Pigchinko Pig"));
    }

    @Override
    public boolean shouldDespawn() {
        return true;
    }

    @Override
    public boolean shouldPickUpLoot() {
        return false;
    }
}
