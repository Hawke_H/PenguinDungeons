package com.minecraftonline.penguindungeons.customentity.human;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityType;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.ai.Goal;
import org.spongepowered.api.entity.ai.GoalTypes;
import org.spongepowered.api.entity.ai.task.AITask;
import org.spongepowered.api.entity.ai.task.builtin.LookIdleAITask;
import org.spongepowered.api.entity.ai.task.builtin.SwimmingAITask;
import org.spongepowered.api.entity.ai.task.builtin.WatchClosestAITask;
import org.spongepowered.api.entity.ai.task.builtin.creature.AttackLivingAITask;
import org.spongepowered.api.entity.ai.task.builtin.creature.WanderAITask;
import org.spongepowered.api.entity.ai.task.builtin.creature.target.FindNearestAttackableTargetAITask;
import org.spongepowered.api.entity.living.Agent;
import org.spongepowered.api.entity.living.Creature;
import org.spongepowered.api.entity.living.Human;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.common.entity.living.human.EntityHuman;

import com.minecraftonline.penguindungeons.ai.AttackRangedBow;
import com.minecraftonline.penguindungeons.util.ResourceKey;

public class AggressiveHuman extends HumanType {
    public AggressiveHuman(ResourceKey key) {
        super(key);
    }

    @Override
    public EntityType getType() {
        return EntityTypes.VILLAGER; // just to make the spawn egg easier to use
    }

    @Override
    public void onLoad(Entity entity)
    {
        if (!(entity instanceof Human)) {
            throw new IllegalArgumentException("Expected a human to be given to AggressiveHuman to load, but got: " + entity);
        }
        Human human = (Human) entity;
        addAITasks(human);
    }

    private void addAITasks(Human human) {
        Agent agent = (Agent) human;

        SwimmingAITask swimTask = SwimmingAITask.builder()
                .swimChance(0.9F)
                .build(agent);

        WanderAITask wanderTask = WanderAITask.builder()
                .speed(0.8D)
                .executionChance(5)
                .build((Creature) agent);

        WatchClosestAITask watchTask = WatchClosestAITask.builder()
                .watch(Player.class)
                .maxDistance(8.0F)
                .build((Creature) agent);

        LookIdleAITask idleTask = LookIdleAITask.builder()
                .build((Creature) agent);

        AttackLivingAITask attackTask = AttackLivingAITask.builder()
                .longMemory()
                .speed(1.0D)
                .build((Creature) agent);

        AITask<?> rangedAttackTask = (AITask<? extends Agent>) new AttackRangedBow.ShootWithBow((EntityHuman) human, 0.5D, 20, 15.0F);

        Optional<Goal<Agent>> normalGoals = agent.getGoal(GoalTypes.NORMAL);
        if (normalGoals.isPresent()) {
            normalGoals.get().addTask(0, swimTask)
                             .addTask(2, rangedAttackTask)
                             .addTask(3, attackTask)
                             .addTask(7, wanderTask)
                             .addTask(8, watchTask)
                             .addTask(8, idleTask);
        }

        FindNearestAttackableTargetAITask findTargetTask = FindNearestAttackableTargetAITask.builder()
                .chance(1)
                .target(Player.class)
                .build((Creature) agent);

        Optional<Goal<Agent>> targetGoals = agent.getGoal(GoalTypes.TARGET);
        if (targetGoals.isPresent()) {
            targetGoals.get().addTask(1, findTargetTask);
        }
    }

    @Override
    public Text getDisplayName() {
        return Text.of(TextColors.RESET, "Aggressive Human");
    }

    @Override
    public Text getEntityName() {
        return Text.of("Human");
    }

    @Override
    public List<Text> getEntityDescription() {
        return Collections.singletonList(Text.of(TextColors.WHITE, "Spawns a Human that walks around and attacks players"));
    }

    @Override
    public boolean shouldDespawn() {
        return true;
    }

    @Override
    public boolean shouldPickUpLoot() {
        return false;
    }
}
