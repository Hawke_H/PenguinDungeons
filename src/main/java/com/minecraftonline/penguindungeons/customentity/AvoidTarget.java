package com.minecraftonline.penguindungeons.customentity;
import net.minecraft.entity.ai.RandomPositionGenerator;

import org.spongepowered.api.entity.living.Creature;

import com.minecraftonline.penguindungeons.ai.DelegatingToMCAI;
import com.minecraftonline.penguindungeons.data.PenguinDungeonAITaskTypes;

import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.pathfinding.Path;
import net.minecraft.pathfinding.PathNavigate;
import net.minecraft.util.math.Vec3d;

public class AvoidTarget extends EntityAIBase {
   /** The entity we are attached to */
   protected EntityCreature entity;
   private final double farSpeed;
   private final double nearSpeed;
   protected EntityLivingBase target;
   private final float avoidDistance;
   /** The PathEntity of our entity */
   private Path path;
   /** The PathNavigate of our entity */
   private final PathNavigate navigation;

   public AvoidTarget(EntityCreature entityIn, float avoidDistanceIn, double farSpeedIn, double nearSpeedIn) {
      this.entity = entityIn;
      this.avoidDistance = avoidDistanceIn * avoidDistanceIn;
      this.farSpeed = farSpeedIn;
      this.nearSpeed = nearSpeedIn;
      this.navigation = entityIn.getNavigator();
      this.setMutexBits(1);
   }

   /**
    * Returns whether the EntityAIBase should begin execution.
    */
   public boolean shouldExecute() {
      this.target = this.entity.getAttackTarget();
      if (target == null) {
         return false;
      } else if (this.entity.getDistanceSq(this.target) <= avoidDistance) {
         Vec3d vec3d = RandomPositionGenerator.findRandomTargetBlockAwayFrom(this.entity, 16, 7, new Vec3d(this.target.posX, this.target.posY, this.target.posZ));
         if (vec3d == null) {
            return false;
         } else if (this.target.getDistanceSq(vec3d.x, vec3d.y, vec3d.z) < this.target.getDistanceSq(this.entity)) {
            return false;
         } else {
            this.path = this.navigation.getPathToXYZ(vec3d.x, vec3d.y, vec3d.z);
            return this.path != null;
         }
      }
      else
      {
          return false;
      }
   }

   /**
    * Returns whether an in-progress EntityAIBase should continue executing
    */
   public boolean shouldContinueExecuting() {
      return !this.navigation.noPath();
   }

   /**
    * Execute a one shot task or start executing a continuous task
    */
   public void startExecuting() {
      this.navigation.setPath(this.path, this.farSpeed);
   }

   /**
    * Reset the task's internal state. Called when this task is interrupted by another one
    */
   public void resetTask() {
      this.target = null;
   }

   /**
    * Keep ticking a continuous task that has already been started
    */
   public void updateTask() {
      if (this.entity.getDistanceSq(this.target) < 49.0D) {
         this.entity.getNavigator().setSpeed(this.nearSpeed);
      } else {
         this.entity.getNavigator().setSpeed(this.farSpeed);
      }

   }

   public static class AvoidTargetTask extends DelegatingToMCAI<Creature> {
       public AvoidTargetTask(Creature creature, float avoidDistanceIn, double farSpeedIn, double nearSpeedIn) {
           super(PenguinDungeonAITaskTypes.AVOID_TARGET, new AvoidTarget((EntityCreature) creature, avoidDistanceIn, farSpeedIn, nearSpeedIn));
       }
   }
}


