package com.minecraftonline.penguindungeons.wand;

import com.flowpowered.math.vector.Vector3d;
import com.flowpowered.math.vector.Vector3i;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.minecraftonline.penguindungeons.PenguinDungeons;
import com.minecraftonline.penguindungeons.container_loot.ContainerListener;
import com.minecraftonline.penguindungeons.data.PenguinDungeonKeys;
import com.minecraftonline.penguindungeons.data.wand.CustomWandData;
import com.minecraftonline.penguindungeons.data.wand.dungeon.CustomWandDungeonData;
import com.minecraftonline.penguindungeons.dungeon.Dungeon;
import com.minecraftonline.penguindungeons.dungeon.WeightedSpawnOption;
import com.minecraftonline.penguindungeons.gui.inventory.ConfirmationPopup;
import com.minecraftonline.penguindungeons.gui.inventory.PDContainerLootEdit;
import com.minecraftonline.penguindungeons.gui.inventory.PDNodeEditInventory;
import com.minecraftonline.penguindungeons.gui.inventory.PDSpawnEntityEditInventory;
import com.minecraftonline.penguindungeons.gui.inventory.PDSpawnEntityEditInventory.SpawnInventoryType;
import com.minecraftonline.penguindungeons.render.RenderedDungeon;
import com.minecraftonline.penguindungeons.render.RenderedNodeKey;
import com.minecraftonline.penguindungeons.util.HeldItemMap;
import com.minecraftonline.penguindungeons.util.weightedtable.WeightedTableReference;

import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.BlockType;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.block.tileentity.carrier.TileEntityCarrier;
import org.spongepowered.api.data.DataQuery;
import org.spongepowered.api.data.DataView;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.DyeColors;
import org.spongepowered.api.data.type.HandType;
import org.spongepowered.api.data.type.HandTypes;
import org.spongepowered.api.data.value.ValueContainer;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.living.Living;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.block.InteractBlockEvent;
import org.spongepowered.api.event.cause.EventContextKeys;
import org.spongepowered.api.event.entity.InteractEntityEvent;
import org.spongepowered.api.event.filter.cause.First;
import org.spongepowered.api.event.game.state.GameStartedServerEvent;
import org.spongepowered.api.event.game.state.GameStoppingServerEvent;
import org.spongepowered.api.event.network.ClientConnectionEvent;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.ItemStackSnapshot;
import org.spongepowered.api.item.inventory.entity.MainPlayerInventory;
import org.spongepowered.api.item.inventory.query.QueryOperationTypes;
import org.spongepowered.api.scheduler.Task;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.util.Direction;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PDWand {

    public static final String BASE_PERMISSION = PenguinDungeons.BASE_PERMISSION + "wand.";

    private static Task TICK_TASK = null;
    private static final Map<String, RenderedDungeon> RENDERED_DUNGEONS = new HashMap<>();
    private static final HeldItemMap<UUID, String> PLAYER_HELD_DUNGEON_NAME = new HeldItemMap<>();
    private static final Multimap<String, UUID> DUNGEON_WAND_PLAYER = HashMultimap.create();
    private static final Map<RenderedNodeKey, PDSpawnEntityEditInventory> OPEN_EDIT_INVENTORIES = new HashMap<>();

    public static ItemStack createFor(final String dungeon) {
        ItemStack itemStack = ItemStack.of(ItemTypes.STICK, 1);
        itemStack.offer(Keys.DISPLAY_NAME, Text.of(TextColors.BLUE, "Penguin Dungeons Wand - ", TextColors.GOLD, dungeon));
        itemStack.offer(Keys.ITEM_LORE, Collections.singletonList(Text.of(TextColors.GREEN, "Used for viewing/editing the " + dungeon + " dungeon")));
        itemStack.offer(new CustomWandData(true));
        itemStack.offer(new CustomWandDungeonData(dungeon));
        return itemStack;
    }

    public static boolean isPDWand(ValueContainer<?> dataHolder) {
        return dataHolder.get(PenguinDungeonKeys.IS_PD_WAND).orElse(false);
    }

    @Listener
    public void onStarted(GameStartedServerEvent event) {
        PDWand.TICK_TASK = Task.builder()
                .name("Penguin Dungeons Dungeon Renderer")
                .execute(PDWand::tick)
                .intervalTicks(10)
                .submit(PenguinDungeons.getInstance());
    }

    @Listener
    public void onInteractEntityEvent(InteractEntityEvent event, @First Player player) {
        final Entity entity = event.getTargetEntity();
        final UUID uuid = entity.getUniqueId();
        final Optional<RenderedDungeon> renderedDungeon = findDungeonForNodeUUID(uuid);
        if (!renderedDungeon.isPresent()) {
            if (entity instanceof Living && !(entity instanceof Player)) {
                onInteractMobEvent(event, player);
            }
            return;
        }
        if (!player.hasPermission(BASE_PERMISSION + "edit")) {
            player.sendMessage(Text.of(TextColors.RED, "You don't have permission to edit!"));
            return;
        }
        final Vector3d spawnOptionPos = renderedDungeon.get().getReferencedLocation(uuid);
        final Dungeon dungeon = renderedDungeon.get().dungeon();
        final RenderedNodeKey nodeKey = RenderedNodeKey.of(dungeon, spawnOptionPos);
        PDSpawnEntityEditInventory pdSpawnOptionEditInventory = PDWand.OPEN_EDIT_INVENTORIES.computeIfAbsent(nodeKey, (k) -> {
            WeightedSpawnOption weightedSpawnOption = renderedDungeon.get().dungeon().spawnLocations().get(spawnOptionPos);
            if (weightedSpawnOption == null) {
                PenguinDungeons.getLogger().error("No SpawnOption found for location: " + spawnOptionPos);
                return null;
            }
            return editWeightedSpawnOptionInventory(uuid, weightedSpawnOption, Text.of("Dungeon Node"));
        });
        if (pdSpawnOptionEditInventory == null) {
            player.sendMessage(Text.of(TextColors.RED, "An error occurred trying to create the GUI for you, check console for details."));
            return;
        }
        pdSpawnOptionEditInventory.showInventoryTo(player);
    }

    public void onInteractMobEvent(InteractEntityEvent event, Player player) {
        Optional<ItemStackSnapshot> wand = event.getContext().get(EventContextKeys.USED_ITEM).filter(PDWand::isPDWand);
        if (!wand.isPresent()) {
            return;
        }

        final Entity entity = event.getTargetEntity();
        Optional<Text> name = entity.get(Keys.DISPLAY_NAME);

        // give spawn egg for clicked mob
        ItemStack itemStack = ItemStack.builder()
                .itemType(ItemTypes.SPAWN_EGG)
                .build();

        if (name.isPresent()) {
            itemStack.offer(Keys.DISPLAY_NAME, Text.of(TextColors.WHITE, "Spawn ", name.get()));
        }

        Optional<DataView> entityTag = entity.toContainer().getView(DataQuery.of("UnsafeData"));
        if (!entityTag.isPresent()) return;
        DataView itemView = itemStack.toContainer();
        itemView.set(DataQuery.of("UnsafeData", "EntityTag"), entityTag.get());
        itemView.set(DataQuery.of("UnsafeData", "EntityTag", "id"), entity.getType().getName());
        // remove location based data
        itemView.remove(DataQuery.of("UnsafeData", "EntityTag", "Dimension"));
        itemView.remove(DataQuery.of("UnsafeData", "EntityTag", "Pos"));
        itemView.remove(DataQuery.of("UnsafeData", "EntityTag", "Rotation"));
        itemView.remove(DataQuery.of("UnsafeData", "EntityTag", "Motion"));
        itemView.remove(DataQuery.of("UnsafeData", "EntityTag", "APX"));
        itemView.remove(DataQuery.of("UnsafeData", "EntityTag", "APY"));
        itemView.remove(DataQuery.of("UnsafeData", "EntityTag", "APZ"));
        // remove unique data
        itemView.remove(DataQuery.of("UnsafeData", "EntityTag", "UUIDLeast"));
        itemView.remove(DataQuery.of("UnsafeData", "EntityTag", "UUIDMost"));

        itemStack.setRawData(itemView);

        player.getInventory().query(QueryOperationTypes.INVENTORY_TYPE.of(MainPlayerInventory.class)).offer(itemStack.copy());
    }

    @Listener
    public void onInteractMobSpawner(InteractBlockEvent.Secondary event, @First Player player)
    {
        Optional<ItemStackSnapshot> wand = event.getContext().get(EventContextKeys.USED_ITEM).filter(PDWand::isPDWand);
        if (!wand.isPresent()) {
            return;
        }

        Location<World> block = event.getTargetBlock().getLocation().get();
        if (block.getBlockType() != BlockTypes.MOB_SPAWNER) return;

        if (!player.hasPermission(BASE_PERMISSION + "edit" + ".mob_spawner")) {
            player.sendMessage(Text.of(TextColors.RED, "You don't have permission to edit!"));
            return;
        }

        Optional<Boolean> isSneaking = player.get(Keys.IS_SNEAKING);

        if (isSneaking.isPresent() && isSneaking.get())
        {
            // shift clicked spawner, give them item copy
            ItemStack itemStack = ItemStack.builder()
                    .itemType(ItemTypes.MOB_SPAWNER)
                    .add(Keys.DISPLAY_NAME, Text.of(TextColors.WHITE, "Custom Monster Spawner"))
                    .build();

            Optional<DataView> blockTag = event.getTargetBlock().toContainer().getView(DataQuery.of("UnsafeData"));
            if (!blockTag.isPresent()) return;
            DataView itemView = itemStack.toContainer().set(DataQuery.of("UnsafeData", "BlockEntityTag"), blockTag);
            itemStack.setRawData(itemView);

            player.getInventory().query(QueryOperationTypes.INVENTORY_TYPE.of(MainPlayerInventory.class)).offer(itemStack.copy());
        }
        else
        {
            PDSpawnEntityEditInventory pdSpawnOptionEditInventory = new PDSpawnEntityEditInventory(WeightedTableReference.ofSpawner(block),
                    Text.of("Monster Spawner"), SpawnInventoryType.MOB_SPAWNER);
            pdSpawnOptionEditInventory.showInventoryTo(player);
        }
    }

    @Listener
    public void onInteractContainer(InteractBlockEvent.Secondary event, @First Player player) {
        Optional<Boolean> isSneaking = player.get(Keys.IS_SNEAKING);
        // ignore shift clicks
        if (isSneaking.isPresent() && isSneaking.get()) return;

        Optional<ItemStackSnapshot> wand = event.getContext().get(EventContextKeys.USED_ITEM).filter(PDWand::isPDWand);
        if (!wand.isPresent()) return;

        Location<World> block = event.getTargetBlock().getLocation().get();
        if (!ContainerListener.containerBlocks.contains(block.getBlockType())) return;

        // cancel the normal event (opening container)
        event.setCancelled(true);

        if (!player.hasPermission(BASE_PERMISSION + "edit" + ".container_loot")) {
            player.sendMessage(Text.of(TextColors.RED, "You don't have permission to edit!"));
            return;
        }

        if (!(block.getTileEntity().isPresent() || block.getTileEntity().get() instanceof TileEntityCarrier)) return;

        Optional<Text> containerName = block.get(Keys.DISPLAY_NAME);

        // check for adjacent chest
        BlockType blockType = block.getBlockType();
        if (blockType == BlockTypes.CHEST || blockType == BlockTypes.TRAPPED_CHEST) {
            for (Direction direction : ContainerListener.HORIZONTAL_DIRECTIONS) {
                Location<World> adjacentBlock = block.getBlockRelative(direction);
                if (adjacentBlock.getBlockType() == blockType) {
                    Optional<Text> adjacentName = adjacentBlock.get(Keys.DISPLAY_NAME);
                    if (direction != Direction.SOUTH && direction != Direction.EAST) {
                        // this adjacent chest is the main chest
                        block = adjacentBlock;
                        // override name with the name of this chest if present
                        if (adjacentName.isPresent()) containerName = adjacentName;
                    } else {
                        // try get name from this adjacent block if first block does not have it
                        if (!containerName.isPresent()) containerName = adjacentName;
                    }
                    break;
                }
            }
        }

        Text menuTitle;
        if (containerName.isPresent()) {
            menuTitle = Text.of("\"", containerName.get(), "\" Loot");
        } else {
            menuTitle = Text.of("Loot Table");
        }

        PDContainerLootEdit containerLootEdit = new PDContainerLootEdit(block, menuTitle);
        containerLootEdit.showInventoryTo(player);
    }

    @Listener
    public void onInteractBlock(InteractBlockEvent.Secondary event, @First Player player) {
        Optional<ItemStackSnapshot> wand = event.getContext().get(EventContextKeys.USED_ITEM).filter(PDWand::isPDWand);
        if (!wand.isPresent()) {
            return;
        }
        if (!player.hasPermission(BASE_PERMISSION + "edit")) {
            player.sendMessage(Text.of(TextColors.RED, "You don't have permission to edit!"));
            return;
        }
        Optional<Dungeon> dungeon = wand.get().get(PenguinDungeonKeys.PD_WAND_DUNGEON).map(PenguinDungeons.getDungeons()::get);
        if (!dungeon.isPresent()) {
            player.sendMessage(Text.of(TextColors.RED, "Dungeon held in wand no longer exists!"));
            return;
        }
        Location<World> block = event.getTargetBlock().getLocation().get();
        // handled by onInteractMobSpawner or onInteractContainer
        if (block.getBlockType() == BlockTypes.MOB_SPAWNER || ContainerListener.containerBlocks.contains(block.getBlockType())) return;
        Location<World> loc = block.getRelative(event.getTargetSide());
        if (!loc.getExtent().getUniqueId().equals(dungeon.get().world())) {
            player.sendMessage(Text.of(TextColors.RED, "This wand relates to a different world - " + dungeon.get().world()));
            return;
        }
        Vector3i blockPos = loc.getBlockPosition();
        if (dungeon.get().spawnLocations().keySet().stream()
                .map(Vector3d::toInt)
                .anyMatch(vec -> vec.equals(blockPos))) {
            return; // Already a thing here.
        }

        ItemStack confirm = ItemStack.builder().itemType(ItemTypes.STAINED_GLASS)
                .add(Keys.DYE_COLOR, DyeColors.GREEN)
                .add(Keys.DISPLAY_NAME, Text.of(TextColors.GREEN, "Create a new node"))
                .add(Keys.ITEM_LORE, Collections.singletonList(Text.of(TextColors.GRAY, "Click to add a new node, which spawns mobs")))
                .build();

        ConfirmationPopup confirmationPopup = new ConfirmationPopup(confirm, () -> {
            Vector3d pos = blockPos.toDouble().add(0.5, 0, 0.5);
            dungeon.get().set(pos, new WeightedSpawnOption());
            RenderedDungeon renderedDungeon = RENDERED_DUNGEONS.get(dungeon.get().name());
            if (renderedDungeon != null) {
                renderedDungeon.addNode(pos);
            }
        }, Text.of(TextColors.DARK_GREEN, "Create new node?"), null);

        player.openInventory(confirmationPopup.create());
    }

    private static PDSpawnEntityEditInventory editWeightedSpawnOptionInventory(UUID nodeUUID, WeightedSpawnOption weightedSpawnOption, Text title) {
        if (weightedSpawnOption.all().size() > 9) {
            throw new IllegalStateException("IDK what to do with more than 8 entries, oops");
        }
        return new PDNodeEditInventory(WeightedTableReference.of(weightedSpawnOption), title, nodeUUID);
    }

    private static Optional<RenderedDungeon> findDungeonForNodeUUID(UUID uuid) {
        return PDWand.RENDERED_DUNGEONS.values().stream()
                .filter(renderedDungeon -> renderedDungeon.ownsNode(uuid))
                .findAny();
    }

    @Listener
    public void onStopping(GameStoppingServerEvent event) {
        PDWand.RENDERED_DUNGEONS.values().forEach(RenderedDungeon::cleanupAll);
        PDWand.RENDERED_DUNGEONS.clear();
        PDWand.DUNGEON_WAND_PLAYER.clear();
        PDWand.PLAYER_HELD_DUNGEON_NAME.clear();
        PDWand.OPEN_EDIT_INVENTORIES.clear();
        if (PDWand.TICK_TASK != null) {
            PDWand.TICK_TASK.cancel();
        }
    }

    @Listener
    public void onDisconnect(ClientConnectionEvent.Disconnect event) {
        PDWand.OPEN_EDIT_INVENTORIES.values().forEach(gui -> gui.onDisconnect(event.getTargetEntity()));
    }

    public static void beginHolding(Player player, ItemStackSnapshot item, HandType handType) {
        if (!player.hasPermission(BASE_PERMISSION + "view")) {
            return;
        }
        String dungeonName = item.get(PenguinDungeonKeys.PD_WAND_DUNGEON).orElseThrow(() -> new IllegalStateException("Penguin Dungeon wand didn't have dungeon data!"));
        final Dungeon dungeon = PenguinDungeons.getDungeons().get(dungeonName);
        if (dungeon == null) {
            player.sendMessage(Text.of(TextColors.RED, "This stick's dungeon, '" + dungeonName + "' does not exist!"));
            return;
        }
        if (PDWand.PLAYER_HELD_DUNGEON_NAME.containsEntry(player.getUniqueId(), dungeonName)) {
            return;
        }
        PDWand.PLAYER_HELD_DUNGEON_NAME.put(player.getUniqueId(), dungeonName, handType);
        PDWand.DUNGEON_WAND_PLAYER.put(dungeonName, player.getUniqueId());

        PDWand.RENDERED_DUNGEONS.computeIfAbsent(dungeonName, k -> new RenderedDungeon(dungeon));
        PDWand.tick();
    }

    public static void stopHolding(Player player, ItemStackSnapshot item, HandType handType) {
        String dungeon = item.get(PenguinDungeonKeys.PD_WAND_DUNGEON).orElseThrow(() -> new IllegalStateException("Penguin Dungeon wand didn't have dungeon data!"));
        if (!PDWand.RENDERED_DUNGEONS.containsKey(dungeon)) {
            return;
        }

        PDWand.DUNGEON_WAND_PLAYER.remove(dungeon, player.getUniqueId());

        PDWand.PLAYER_HELD_DUNGEON_NAME.remove(player.getUniqueId(), handType);

        if (!PDWand.PLAYER_HELD_DUNGEON_NAME.containsEntry(player.getUniqueId(), dungeon)) {
            // This player is no longer holding this.
            PDWand.DUNGEON_WAND_PLAYER.remove(dungeon, player.getUniqueId());
            if (!PDWand.DUNGEON_WAND_PLAYER.containsKey(dungeon)) {
                RenderedDungeon renderedDungeon = PDWand.RENDERED_DUNGEONS.remove(dungeon);
                renderedDungeon.cleanupAll();
            }
            else {
                PDWand.tick();
            }
        }
    }

    public static void refreshHeldAdded(Player player) {
        player.getItemInHand(HandTypes.MAIN_HAND).filter(PDWand::isPDWand)
                .ifPresent(item -> beginHolding(player, item.createSnapshot(), HandTypes.MAIN_HAND));

        player.getItemInHand(HandTypes.OFF_HAND).filter(PDWand::isPDWand)
                .ifPresent(item -> beginHolding(player, item.createSnapshot(), HandTypes.OFF_HAND));
    }

    public static void tick() {
        PDWand.DUNGEON_WAND_PLAYER.asMap().forEach((dungeon, players) -> {
            Set<Vector3i> toBeRenderedChunks = players.stream().map(Sponge.getServer()::getPlayer)
                    .filter(Optional::isPresent)
                    .map(Optional::get)
                    .flatMap(PDWand::getChunksRenderedByPlayer)
                    .collect(Collectors.toSet());
            final RenderedDungeon renderedDungeon = PDWand.RENDERED_DUNGEONS.get(dungeon);
            if (renderedDungeon == null) {
                PenguinDungeons.getLogger().error("Missing rendered dungeon entry in PDWand.");
                return;
            }
            renderedDungeon.renderOnlyChunks(toBeRenderedChunks);
        });
    }

    public static void dungeonDeleted(Dungeon dungeon) {
        RenderedDungeon renderedDungeon = PDWand.RENDERED_DUNGEONS.remove(dungeon.name());
        if (renderedDungeon == null) {
            return;
        }
        renderedDungeon.cleanupAll();
    }

    public static void deleteNode(UUID nodeUUID) {
        findDungeonForNodeUUID(nodeUUID).ifPresent(renderedDungeon -> {
            Vector3d pos = renderedDungeon.getReferencedLocation(nodeUUID);
            renderedDungeon.dungeon().remove(pos);
            renderedDungeon.removeNode(nodeUUID);
        });
    }

    private static final int CHUNKS_RENDER_DISTANCE = 5;

    private static Stream<Vector3i> getChunksRenderedByPlayer(Player player) {
        List<Vector3i> chunks = new ArrayList<>(CHUNKS_RENDER_DISTANCE * 2 * CHUNKS_RENDER_DISTANCE * 2);
        final Vector3i chunkPos = player.getLocation().getChunkPosition();
        for (int x = -CHUNKS_RENDER_DISTANCE; x < CHUNKS_RENDER_DISTANCE; x++) {
            for (int z = -CHUNKS_RENDER_DISTANCE; z < CHUNKS_RENDER_DISTANCE; z++) {
                chunks.add(chunkPos.add(x, 0, z));
            }
        }
        return chunks.stream();
    }
}
