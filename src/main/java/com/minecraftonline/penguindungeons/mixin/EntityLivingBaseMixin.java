package com.minecraftonline.penguindungeons.mixin;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.DamageSource;
import net.minecraft.world.World;

import java.util.Optional;

import org.spongepowered.api.entity.living.Living;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import com.minecraftonline.penguindungeons.customentity.CustomEntityTypes;
import com.minecraftonline.penguindungeons.data.PenguinDungeonKeys;
import com.minecraftonline.penguindungeons.util.ResourceKey;

@Mixin(EntityLivingBase.class)
public abstract class EntityLivingBaseMixin extends Entity {

    public EntityLivingBaseMixin(World worldIn) {
        super(worldIn);
    }

    // prevent custom PD mobs from dismounting at all, especially when in water
    // unless they are dead or it is a player dismounting the mob
    @Inject(method = "dismountRidingEntity", at = @At("HEAD"), cancellable = true)
    public void onDismountRidingEntity(CallbackInfo ci) {
        // don't prevent players from dismounting
        if (this instanceof Player || !this.isEntityAlive()) return;
        Entity ridingEntity = this.getRidingEntity();
        if (ridingEntity == null || !ridingEntity.isEntityAlive()) return;
        // don't dismount riding entity if mob is a custom PD one
        if (((Living) this).get(PenguinDungeonKeys.PD_ENTITY_TYPE).isPresent() ||
           ((org.spongepowered.api.entity.Entity) ridingEntity).get(PenguinDungeonKeys.PD_ENTITY_TYPE).isPresent())
        {
            ci.cancel();
        }
    }

    @Inject(method = "attackEntityFrom", at = @At("HEAD"), cancellable = true)
    public void onAttackEntityFrom(DamageSource source, float amount, CallbackInfoReturnable<Boolean> cir) {
        // redirect damage from mobs that are part of another mob
        Optional<ResourceKey> pdType = ((org.spongepowered.api.entity.Entity) this).get(PenguinDungeonKeys.PD_ENTITY_TYPE);
        if (pdType.isPresent()) {
            if (pdType.get().equals(CustomEntityTypes.ASTRAL_MONARCH_FLIPPEH_HUMAN.getId())) {
                // this custom mob is immune to magic damage
                if (source.isMagicDamage()) cir.setReturnValue(false);
                return;
            } else if (pdType.get().equals(CustomEntityTypes.HEROBRINE_BOSS.getId())) {
                // this custom mob is immune to magic, explosive, fire damage, and fall damage
                if (source.isMagicDamage() || source.isExplosion() || source.isFireDamage() ||
                    source.getDamageType() == DamageSource.FALL.getDamageType()) cir.setReturnValue(false);
            } else if (pdType.get().equals(CustomEntityTypes.LOVE_VEX.getId())) {
                for(net.minecraft.entity.Entity rider : ((EntityLivingBase)(Object)this).getPassengers()) {
                    Optional<ResourceKey> riderPdType = ((org.spongepowered.api.entity.Entity) rider).get(PenguinDungeonKeys.PD_ENTITY_TYPE);
                    if (riderPdType.isPresent() && riderPdType.get().equals(CustomEntityTypes.FLYING_CUPID.getId())) {
                        if (rider.isEntityEqual(source.getTrueSource())) {
                            // don't accept attacks from rider
                            cir.setReturnValue(false);
                            return;
                        }
                        cir.setReturnValue(rider.attackEntityFrom(source, amount));
                        return;
                    }
                    break;
                }
            }
        }
    }

    @Inject(method = "addPotionEffect", at = @At("HEAD"), cancellable = true)
    public void onAddPotionEffect(PotionEffect potioneffectIn, CallbackInfo ci) {
        // redirect potion effects from mobs that are part of another mob
        Optional<ResourceKey> pdType = ((org.spongepowered.api.entity.Entity) this).get(PenguinDungeonKeys.PD_ENTITY_TYPE);
        if (pdType.isPresent()) {
            if (pdType.get().equals(CustomEntityTypes.ASTRAL_MONARCH_FLIPPEH_HUMAN.getId()) ||
                pdType.get().equals(CustomEntityTypes.HEROBRINE_BOSS.getId()) ||
                pdType.get().equals(CustomEntityTypes.BUTTERFLY.getId())) {
                // these custom mobs are immune to potion effects
                ci.cancel();
                return;
            } else if (pdType.get().equals(CustomEntityTypes.LOVE_VEX.getId())) {
                for(net.minecraft.entity.Entity rider : ((EntityLivingBase)(Object)this).getPassengers()) {
                    if (!(rider instanceof EntityLivingBase)) return;
                    Optional<ResourceKey> riderPdType = ((org.spongepowered.api.entity.Entity) rider).get(PenguinDungeonKeys.PD_ENTITY_TYPE);
                    if (riderPdType.isPresent() && riderPdType.get().equals(CustomEntityTypes.FLYING_CUPID.getId())) {
                        ((EntityLivingBase)rider).addPotionEffect(potioneffectIn);
                        ci.cancel();
                        return;
                    }
                    break;
                }
            }
        }
    }
}
