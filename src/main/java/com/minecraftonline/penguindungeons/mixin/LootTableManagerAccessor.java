package com.minecraftonline.penguindungeons.mixin;

import com.google.gson.Gson;
import net.minecraft.world.storage.loot.LootTableManager;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;

@Mixin(LootTableManager.class)
public interface LootTableManagerAccessor {

    @Accessor("GSON_INSTANCE")
    public static Gson getGsonInstance() {
        throw new AssertionError();
    }
}
