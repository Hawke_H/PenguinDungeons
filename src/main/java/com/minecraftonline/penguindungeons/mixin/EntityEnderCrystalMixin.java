package com.minecraftonline.penguindungeons.mixin;

import net.minecraft.entity.Entity;
import net.minecraft.entity.item.EntityEnderCrystal;
import net.minecraft.util.DamageSource;

import java.util.Optional;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import com.minecraftonline.penguindungeons.customentity.CustomEntityTypes;
import com.minecraftonline.penguindungeons.data.PenguinDungeonKeys;
import com.minecraftonline.penguindungeons.util.ResourceKey;

@Mixin(EntityEnderCrystal.class)
public abstract class EntityEnderCrystalMixin {

    @Inject(method = "attackEntityFrom", at = @At("HEAD"), cancellable = true)
    public void onAttackEntityFrom(DamageSource source, float amount, CallbackInfoReturnable<Boolean> cir) {
        Entity vehicle = ((Entity)(Object)this).getRidingEntity();
        if (vehicle == null) return;
        Optional<ResourceKey> vehiclePdType = ((org.spongepowered.api.entity.Entity) vehicle).get(PenguinDungeonKeys.PD_ENTITY_TYPE);
        if (vehiclePdType.isPresent() && vehiclePdType.get().equals(CustomEntityTypes.CHAOS_GHAST.getId())) {
            if (vehicle.equals(source.getTrueSource())) {
                // don't accept attacks from vehicle
                cir.setReturnValue(false);
            }
            cir.setReturnValue(vehicle.attackEntityFrom(source, amount));
            return;
        }
    }

    @Inject(method = "onUpdate", at = @At(value = "INVOKE", target = "Lnet/minecraft/world/World;getBlockState(Lnet/minecraft/util/math/BlockPos;)Lnet/minecraft/block/state/IBlockState;"), cancellable = true)
    public void onUpdateFirePlace(CallbackInfo ci) {
        // don't attempt to place fire if this crystal is riding another entity
        if (((Entity)(Object)this).isRiding()) ci.cancel();
    }
}
