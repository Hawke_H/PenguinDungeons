package com.minecraftonline.penguindungeons.mixin;

import java.util.ArrayList;
import java.util.List;

import org.spongepowered.api.data.DataContainer;
import org.spongepowered.api.data.DataQuery;
import org.spongepowered.api.data.DataView;
import org.spongepowered.api.data.Queries;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.ModifyArg;
import org.spongepowered.common.data.persistence.NbtTranslator;
import org.spongepowered.common.util.Constants;

import com.minecraftonline.penguindungeons.data.PenguinDungeonKeys;
import com.minecraftonline.penguindungeons.data.spawning.PDSpawnData;

import net.minecraft.entity.passive.EntityShoulderRiding;
import net.minecraft.entity.passive.EntityTameable;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;

@Mixin(EntityShoulderRiding.class)
public abstract class EntityShoulderRidingMixin extends EntityTameable {

    public EntityShoulderRidingMixin(World worldIn) {
        super(worldIn);
    }

    @ModifyArg(method = "setEntityOnShoulder", at = @At(value = "INVOKE", target = "Lnet/minecraft/entity/player/EntityPlayer;addShoulderEntity(Lnet/minecraft/nbt/NBTTagCompound;)Z"), index = 0)
    private NBTTagCompound onSetEntityOnShoulder(NBTTagCompound nbttagcompound) {
        if (!((Entity)this).get(PenguinDungeonKeys.PD_ENTITY_TYPE).isPresent()) return nbttagcompound;

        DataContainer data = NbtTranslator.getInstance().translate(nbttagcompound);
        List<DataView> manipulators = data.getViewList(DataQuery.of(Constants.Forge.FORGE_DATA, Constants.Sponge.SPONGE_DATA,
                Constants.Sponge.CUSTOM_MANIPULATOR_TAG_LIST)).orElse(new ArrayList<DataView>());
        // add spawn data to this mob so that when it is recreated later
        // so it does not get reset by PD
        DataView pdSpawn = DataContainer.createNew();
        pdSpawn.set(Queries.CONTENT_VERSION, 2);
        pdSpawn.set(DataQuery.of(Constants.Sponge.MANIPULATOR_ID), PenguinDungeonKeys.PD_SPAWN.getId());
        // note this is set to false, not true
        // which tells us this entity needs PD to reload it later
        pdSpawn.set(Constants.Sponge.INTERNAL_DATA, new PDSpawnData(false).toContainer());
        manipulators.add(pdSpawn);
        data.set(DataQuery.of(Constants.Forge.FORGE_DATA, Constants.Sponge.SPONGE_DATA,
                   Constants.Sponge.CUSTOM_MANIPULATOR_TAG_LIST), manipulators);
        return NbtTranslator.getInstance().translate(data);
    }

}
