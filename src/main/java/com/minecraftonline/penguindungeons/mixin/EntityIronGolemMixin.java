package com.minecraftonline.penguindungeons.mixin;

import net.minecraft.entity.Entity;
import net.minecraft.entity.monster.EntityGolem;
import net.minecraft.entity.monster.EntityIronGolem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.projectile.EntityArrow;
import net.minecraft.entity.projectile.EntityFireball;
import net.minecraft.entity.projectile.EntityShulkerBullet;
import net.minecraft.init.SoundEvents;
import net.minecraft.util.DamageSource;
import net.minecraft.util.SoundEvent;
import net.minecraft.world.World;

import java.util.Optional;

import org.spongepowered.api.entity.living.golem.IronGolem;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import com.minecraftonline.penguindungeons.customentity.CustomEntityTypes;
import com.minecraftonline.penguindungeons.data.PenguinDungeonKeys;
import com.minecraftonline.penguindungeons.util.ResourceKey;

@Mixin(EntityIronGolem.class)
public abstract class EntityIronGolemMixin extends EntityGolem {

    private Optional<ResourceKey> getPenguinDungeonKey()
    {
        return ((IronGolem) this).get(PenguinDungeonKeys.PD_ENTITY_TYPE);
    }

    private boolean isFrostGolem()
    {
        return isFrostGolem(getPenguinDungeonKey());
    }

    private boolean isFrostGolem(Optional<ResourceKey> id)
    {
        return id.isPresent() && id.get().equals(CustomEntityTypes.FROST_GOLEM.getId());
    }

    private boolean isEggGolem()
    {
        return isEggGolem(getPenguinDungeonKey());
    }

    private boolean isEggGolem(Optional<ResourceKey> id)
    {
        return id.isPresent() && id.get().equals(CustomEntityTypes.EGG_GOLEM.getId());
    }

    public EntityIronGolemMixin(World worldIn) {
        super(worldIn);
    }

    @Override
    public SoundEvent getAmbientSound() {
        return isEggGolem() ? SoundEvents.ENTITY_RABBIT_AMBIENT : null;
    }

    @Inject(method = "getHurtSound", at = @At("HEAD"), cancellable = true)
    private void onGetHurtSound(CallbackInfoReturnable<SoundEvent> cir) {
        if (isEggGolem()) {
            cir.setReturnValue(SoundEvents.ENTITY_RABBIT_HURT);
        }
    }

    @Inject(method = "getDeathSound", at = @At("HEAD"), cancellable = true)
    private void onGetDeathSound(CallbackInfoReturnable<SoundEvent> cir) {
        if (isEggGolem()) {
            cir.setReturnValue(SoundEvents.ENTITY_RABBIT_DEATH);
        }
    }

    @Override
    protected float getSoundPitch() {
        if (isEggGolem()) {
            return (this.rand.nextFloat() - this.rand.nextFloat()) * 0.2F + 0.2F;
        }
        return super.getSoundPitch();
    }

    @Override
    public boolean attackEntityFrom(DamageSource source, float amount) {
        if (this.getHealth() < (this.getMaxHealth() / 2))
        {
            // Frost Golems don't take damage from most projectiles or explosions when under half health
            if (this.isFrostGolem())
            {
                if (source.isExplosion()) return false;
                Entity entity = source.getImmediateSource();
                if (entity instanceof EntityArrow || entity instanceof EntityShulkerBullet || entity instanceof EntityFireball) {
                    return false;
                }
            }
        }

        boolean result = super.attackEntityFrom(source, amount);

        return result;
    }

    @Inject(method = "collideWithEntity", at = @At("HEAD"), cancellable = true)
    private void onCollideWithEntity(Entity entityIn, CallbackInfo ci) {
        if (getPenguinDungeonKey().isPresent())
        {
            // don't attack regular hostile mobs that bump in to custom iron golem
            ci.cancel();
        }
    }

    @Override
    protected int getExperiencePoints(EntityPlayer player) {
        if (getPenguinDungeonKey().isPresent())
        {
            this.experienceValue = 50; // same as Wither boss
        }

        return super.getExperiencePoints(player);
    }
}
