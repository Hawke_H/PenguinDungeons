package com.minecraftonline.penguindungeons.mixin;

import net.minecraft.entity.EnumCreatureAttribute;
import net.minecraft.entity.monster.EntityGiantZombie;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import org.spongepowered.api.entity.living.monster.Giant;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import com.minecraftonline.penguindungeons.data.PenguinDungeonKeys;

@Mixin(EntityGiantZombie.class)
public abstract class EntityGiantMixin extends EntityMob {

    public EntityGiantMixin(World worldIn) {
        super(worldIn);
    }

    @Inject(method = "getBlockPathWeight", at = @At("HEAD"), cancellable = true)
    public void onGetBlockPathWeight(BlockPos pos, CallbackInfoReturnable<Float> cir) {
        cir.setReturnValue(super.getBlockPathWeight(pos));
    }

    @Override
    public EnumCreatureAttribute getCreatureAttribute() {
        // giants are undead
        return EnumCreatureAttribute.UNDEAD;
    }

    @Override
    protected int getExperiencePoints(EntityPlayer player) {
        if (((Giant) this).get(PenguinDungeonKeys.PD_ENTITY_TYPE).isPresent())
        {
            this.experienceValue = 25;
        }

        return super.getExperiencePoints(player);
    }
}
