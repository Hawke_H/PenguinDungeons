package com.minecraftonline.penguindungeons.mixin;

import java.util.Random;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import com.minecraftonline.penguindungeons.customentity.dragon.CrystalType;
import com.minecraftonline.penguindungeons.customentity.dragon.WorldGenSpikesAccessor;

import net.minecraft.entity.item.EntityEnderCrystal;
import net.minecraft.init.Blocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.World;
import net.minecraft.world.gen.feature.WorldGenSpikes;
import net.minecraft.world.gen.feature.WorldGenerator;

@Mixin(WorldGenSpikes.class)
public abstract class WorldGenSpikesMixin extends WorldGenerator implements WorldGenSpikesAccessor {

    @Shadow public boolean crystalInvulnerable;
    @Shadow public WorldGenSpikes.EndSpike spike;
    @Shadow public BlockPos beamTarget;
    protected CrystalType type = CrystalType.NONE;

    public void setCrystalType(CrystalType type) {
        this.type = type;
    }

    @Inject(method = "generate", at = @At("HEAD"), cancellable = true)
    public void generate(World worldIn, Random rand, BlockPos position, CallbackInfoReturnable<Boolean> cir) {
        if (this.spike == null) {
           throw new IllegalStateException("Decoration requires priming with a spike");
        } else {
           int i = this.spike.getRadius();

           for(BlockPos.MutableBlockPos blockpos$mutableblockpos : BlockPos.getAllInBoxMutable(new BlockPos(position.getX() - i, 0, position.getZ() - i), new BlockPos(position.getX() + i, this.spike.getHeight() + 10, position.getZ() + i))) {
              if (blockpos$mutableblockpos.distanceSq((double)position.getX(), (double)blockpos$mutableblockpos.getY(), (double)position.getZ()) <= (double)(i * i + 1) && blockpos$mutableblockpos.getY() < this.spike.getHeight()) {
                 if (blockpos$mutableblockpos.getY() == this.spike.getHeight() - 1) {
                     // top pillar block material
                     this.setBlockAndNotifyAdequately(worldIn, blockpos$mutableblockpos, this.type.getTopMaterial());
                 } else if (blockpos$mutableblockpos.getY() >= this.spike.getHeight() - this.type.getMiddleSize()) {
                     this.setBlockAndNotifyAdequately(worldIn, blockpos$mutableblockpos, this.type.getMiddleMaterial());
                 } else {
                     this.setBlockAndNotifyAdequately(worldIn, blockpos$mutableblockpos, this.type.getBaseMaterial());
                 }
              } else if (blockpos$mutableblockpos.getY() > 65) {
                 this.setBlockAndNotifyAdequately(worldIn, blockpos$mutableblockpos, Blocks.AIR.getDefaultState());
              }
           }

           if (this.spike.isGuarded()) {
              for(int j = -2; j <= 2; ++j) {
                 for(int k = -2; k <= 2; ++k) {
                    if (MathHelper.abs(j) == 2 || MathHelper.abs(k) == 2) {
                       this.setBlockAndNotifyAdequately(worldIn, new BlockPos(position.getX() + j, this.spike.getHeight(), position.getZ() + k), this.type.getCageMaterial());
                       this.setBlockAndNotifyAdequately(worldIn, new BlockPos(position.getX() + j, this.spike.getHeight() + 1, position.getZ() + k), this.type.getCageMaterial());
                       this.setBlockAndNotifyAdequately(worldIn, new BlockPos(position.getX() + j, this.spike.getHeight() + 2, position.getZ() + k), this.type.getCageMaterial());
                    }

                    this.setBlockAndNotifyAdequately(worldIn, new BlockPos(position.getX() + j, this.spike.getHeight() + 3, position.getZ() + k), this.type.getCageMaterial());
                 }
              }
           }

           EntityEnderCrystal entityendercrystal = new EntityEnderCrystal(worldIn);
           if (this.type.getName() != null) entityendercrystal.setCustomNameTag(this.type.getName());
           entityendercrystal.setBeamTarget(this.beamTarget);
           entityendercrystal.setEntityInvulnerable(this.crystalInvulnerable);
           entityendercrystal.setLocationAndAngles((double)((float)position.getX() + 0.5F), (double)(this.spike.getHeight() + 1), (double)((float)position.getZ() + 0.5F), rand.nextFloat() * 360.0F, 0.0F);
           worldIn.spawnEntity(entityendercrystal);
           this.setBlockAndNotifyAdequately(worldIn, new BlockPos(position.getX(), this.spike.getHeight(), position.getZ()), this.type.getCrystalMaterial());
           cir.setReturnValue(true);
        }
     }

}
