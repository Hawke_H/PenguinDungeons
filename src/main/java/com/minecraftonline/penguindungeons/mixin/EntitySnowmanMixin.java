package com.minecraftonline.penguindungeons.mixin;

import net.minecraft.entity.monster.EntityGolem;
import net.minecraft.entity.monster.EntitySnowman;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;

import org.spongepowered.api.entity.living.golem.SnowGolem;
import org.spongepowered.asm.mixin.Mixin;

import com.minecraftonline.penguindungeons.data.PenguinDungeonKeys;

@Mixin(EntitySnowman.class)
public abstract class EntitySnowmanMixin extends EntityGolem {

    public EntitySnowmanMixin(World worldIn) {
        super(worldIn);
    }

    @Override
    protected int getExperiencePoints(EntityPlayer player) {
        if (((SnowGolem) this).get(PenguinDungeonKeys.PD_ENTITY_TYPE).isPresent())
        {
            this.experienceValue = 10; // same as Blaze
        }

        return super.getExperiencePoints(player);
    }
}
