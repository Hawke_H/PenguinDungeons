package com.minecraftonline.penguindungeons.mixin;

import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.passive.EntityAnimal;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import java.util.Optional;

import org.spongepowered.api.entity.living.Living;
import org.spongepowered.api.entity.living.animal.Animal;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import com.minecraftonline.penguindungeons.data.PenguinDungeonKeys;
import com.minecraftonline.penguindungeons.util.ResourceKey;

@Mixin(EntityAnimal.class)
public abstract class EntityAnimalMixin extends EntityLiving {

    public EntityAnimalMixin(World worldIn) {
        super(worldIn);
    }

    @Inject(method = "getCanSpawnHere", at = @At("HEAD"), cancellable = true)
    private void onGetCanSpawnHere(CallbackInfoReturnable<Boolean> cir) {
        Animal animal = (Animal) this;
        final Optional<ResourceKey> id = animal.get(PenguinDungeonKeys.PD_ENTITY_TYPE);
        if (id.isPresent())
        {
            // bypass lighting and block spawn restriction
            IBlockState iblockstate = this.world.getBlockState((new BlockPos(this)).down());
            cir.setReturnValue(iblockstate.canEntitySpawn(this));
        }
    }

    @Inject(method = "canDespawn", at = @At("HEAD"), cancellable = true)
    protected void onCanDespawn(CallbackInfoReturnable<Boolean> cir) {
        if (((Living)(Object)this).get(PenguinDungeonKeys.PD_ENTITY_TYPE).isPresent())
        {
            cir.setReturnValue(true);
        }
    }

    @Inject(method = "getExperiencePoints", at = @At("HEAD"), cancellable = true)
    protected void onGetExperiencePoints(EntityPlayer player, CallbackInfoReturnable<Integer> cir) {
        if (((Living)(Object)this).get(PenguinDungeonKeys.PD_ENTITY_TYPE).isPresent())
        {
            cir.setReturnValue(5 + this.world.rand.nextInt(3));
        }
    }
}
