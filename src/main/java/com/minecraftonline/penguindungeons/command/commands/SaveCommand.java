package com.minecraftonline.penguindungeons.command.commands;

import com.minecraftonline.penguindungeons.PenguinDungeons;
import com.minecraftonline.penguindungeons.command.AbstractCommand;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

public class SaveCommand extends AbstractCommand {

    public SaveCommand() {
        super(PenguinDungeons.BASE_PERMISSION + "save");

        setExecutor((src, args) -> {
            if (!PenguinDungeons.getInstance().save()) {
                throw new CommandException(Text.of(TextColors.RED, "Failed to save config - See console for details."));
            }
            src.sendMessage(Text.of(TextColors.GREEN, "Saved config."));
            return CommandResult.success();
        });
    }
}
