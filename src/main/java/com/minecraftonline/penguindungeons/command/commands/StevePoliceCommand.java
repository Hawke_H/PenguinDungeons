package com.minecraftonline.penguindungeons.command.commands;

import java.util.Optional;

import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.entity.living.Human;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import com.minecraftonline.penguindungeons.PenguinDungeons;
import com.minecraftonline.penguindungeons.command.AbstractCommand;
import com.minecraftonline.penguindungeons.customentity.CustomEntityTypes;
import com.minecraftonline.penguindungeons.customentity.human.StevePolice.PoliceType;

public class StevePoliceCommand extends AbstractCommand {

    StevePoliceCommand() {
        super(PenguinDungeons.BASE_PERMISSION + "stevepolice");

        addArguments(GenericArguments.playerOrSource(Text.of("player")), GenericArguments.optional(GenericArguments.enumValue(Text.of("police type"), PoliceType.class)));

        setExecutor((src, args) -> {
            Player player = args.requireOne("player");
            Optional<Object> type = args.getOne("police type");
            src.sendMessage(Text.of(TextColors.GRAY, "Spawning police officer targeting " + player.getName()));
            Human officer;
            if (type.isPresent() && type.get() instanceof PoliceType) {
                officer = CustomEntityTypes.STEVE_POLICE.makeHumanWithTarget(player.getWorld(), player.getPosition(), player, (PoliceType) type.get());
            } else {
                officer = CustomEntityTypes.STEVE_POLICE.makeHumanWithTarget(player.getWorld(), player.getPosition(), player);
            }
            player.getWorld().spawnEntity(officer);
            CustomEntityTypes.STEVE_POLICE.onLoad(officer);
            return CommandResult.empty();
        });
    }
}
