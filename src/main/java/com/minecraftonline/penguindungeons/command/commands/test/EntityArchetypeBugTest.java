package com.minecraftonline.penguindungeons.command.commands.test;

import com.minecraftonline.penguindungeons.command.AbstractCommand;
import com.minecraftonline.penguindungeons.data.PenguinDungeonKeys;
import com.minecraftonline.penguindungeons.data.customentity.PDEntityTypeData;
import com.minecraftonline.penguindungeons.util.ResourceKey;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.DyeColors;
import org.spongepowered.api.data.type.HorseColors;
import org.spongepowered.api.entity.EntityArchetype;
import org.spongepowered.api.entity.EntityTypes;

public class EntityArchetypeBugTest extends AbstractCommand {

    public EntityArchetypeBugTest() {
        setExecutor((src, args) -> {
            System.out.println("Test 1 - Builder.");
            EntityArchetype e1 = EntityArchetype.builder()
                    .type(EntityTypes.SHULKER)
                    .set(Keys.DYE_COLOR, DyeColors.GREEN)
                    .build();
            System.out.println("Test 1 Result: " + e1.getEntityData());

            System.out.println("Test 2 - of then offer");
            EntityArchetype e2 = EntityArchetype.of(EntityTypes.SHULKER);
            e2.offer(Keys.DYE_COLOR, DyeColors.GREEN);
            System.out.println("Test 2 Result: " + e2.getEntityData());

            System.out.println("Test 3 - Builder with custom data key");
            EntityArchetype e3 = EntityArchetype.builder()
                    .type(EntityTypes.SHULKER)
                    .set(PenguinDungeonKeys.PD_ENTITY_TYPE, ResourceKey.pd("test_type"))
                    .build();
            System.out.println("Test 3 Result: " + e3.getEntityData());

            System.out.println("Test 4 - of then custom data");
            EntityArchetype e4 = EntityArchetype.of(EntityTypes.SHULKER);
            e4.offer(PenguinDungeonKeys.PD_ENTITY_TYPE, ResourceKey.pd("test_type"));
            System.out.println("Test 4 Result: " + e4.getEntityData());

            System.out.println("Test 5 - Custom data with data manipulators");
            EntityArchetype e5 = EntityArchetype.of(EntityTypes.SHULKER);
            e5.offer(new PDEntityTypeData(ResourceKey.pd("test_type")));
            System.out.println("Test 5 Result: " + e5.getEntityData());

            System.out.println("Test 6 - Builder with a different entity type and key");
            EntityArchetype e6 = EntityArchetype.builder()
                    .type(EntityTypes.HORSE)
                    .set(Keys.HORSE_COLOR, HorseColors.CHESTNUT)
                    .build();
            System.out.println("Test 6 Result: " + e6.getEntityData());

            return CommandResult.success();
        });
    }
}
